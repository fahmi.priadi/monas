<!doctype html>
<html>
<head>
<title>jQuery BlockUI Plugin</title>
<link rel="stylesheet" href="<?=base_url()?>public/lib/blockUI/jquery-ui.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>public/lib/blockUI/jq.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>public/lib/blockUI/block.css?v3">
<script src="<?=base_url()?>public/lib/blockUI/jquery.min.js"></script>
<script src="<?=base_url()?>public/lib/blockUI/jquery-ui.min.js"></script>
<!-- <script src="jquery.blockUI.js"></script> -->
<script src="<?=base_url()?>public/lib/blockUI/jquery.blockUI.js"></script>
<script src="<?=base_url()?>public/lib/blockUI/chili-1.7.pack.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#demo7').click(function() {
        $.blockUI({ message: 'Processing..' });

        //setTimeout($.unblockUI, 2000);
    });
});
</script>
<p><input name="" type="button" id="demo7" value="submit"></p>
<p>&nbsp;</p>
