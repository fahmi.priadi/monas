<script src="<? echo base_url()?>public/datepicker/jquery-1.4.2.js"></script>
		<style type="text/css" title="currentStyle">
			@import "http://10.3.0.212/test/amr_view/public/datatables/css/demo_table.css";
			@import "http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/css/TableTools.css";
		</style>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/js/ZeroClipboard.js"></script>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/js/TableTools.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				$('#example').dataTable( {
					 "sPaginationType": "full_numbers",
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/swf/copy_cvs_xls_pdf.swf",
						"aButtons": ["xls"]
					}
				} );
			} );
		</script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				$('#example2').dataTable( {
					 "sPaginationType": "full_numbers",
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/swf/copy_cvs_xls_pdf.swf",
						"aButtons": ["xls"]
					}
				} );
			} );
		</script>
<script language="Javascript" SRC="http://10.3.0.212/test/amr_view/public/Charts/FusionCharts.js"></script>
<h1><a href="#"><?php echo $title; ?></a></h1>
<br/>

<table width="100%">
  <tr >
    <td width="11%"><div align="left">IDPEL</div></td>
    <td width="1%"><strong>:</strong></td>
    <td width="37%"><strong><?=$plg->NOPEL?></strong></td>
    <td width="11%"><div align="left">TARIF</div></td>
    <td width="1%"><strong>:</strong></td>
    <td width="39%"><strong><?=$plg->TARIF?></strong></td>
  </tr>
  <tr>
    <td><div align="left">IDMETER</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=$plg->IDMETER?></strong></td>
    <td><div align="left">DAYA</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=$plg->DAYATPS?></strong></td>
  </tr>
  <tr>
    <td><div align="left">NAMA</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=$plg->NAMA?></strong></td>
    <td><div align="left">KDJENISMETER</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=$plg->KDJENISMETER?></strong></td>
  </tr>
  <tr>
    <td><div align="left">ALAMAT</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=$plg->ALAMAT?></strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<p style="clear:both; margin-top:4em; font-weight: bold;">Tabel Data Tidak Normal</p>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
 <thead>
  <tr>
    <th>KDJENISMETER</th>
    <th>TGLJAM</th>
    <th>VA</th>
    <th>VB</th>
    <th>VC</th>
    <th>IA</th>
    <th>IB</th>
    <th>IC</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach($result as $plg){  ?>
  <tr >
    <td align="center"><?=$plg['KDJENISMETER']?></td>
    <td align="center"><?=$plg['TGLJAM']?></td>
    <td align="center"><?=$plg['VA']?></td>
    <td align="center"><?=$plg['VB']?></td>
    <td align="center"><?=$plg['VC']?></td>
    <td align="center"><?=$plg['IA']?></td>
    <td align="center"><?=$plg['IB']?></td>
	<td align="center"><?=$plg['IC']?></td>
  </tr>
  <?php } //end foreach ?> 
 </tbody>
</table>

<p>
<?php
 echo $graph_hist_voltase;
?>	
</p>
<p style="clear:both; margin-top:4em; font-weight: bold;">Tabel Data  (All)</p>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example2" width="100%">
 <thead>
  <tr>
    <th>KDJENISMETER</th>
    <th>TGLJAM</th>
    <th>VA</th>
    <th>VB</th>
    <th>VC</th>
    <th>IA</th>
    <th>IB</th>
    <th>IC</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach($result_all as $plg){  ?>
   <tr <? if($plg['VA']==0 or $plg['VB']==0 or $plg['VC']==0) echo "class='gradeX'";?>>
    <td align="center"><?=$plg['KDJENISMETER']?></td>
    <td align="center"><?=$plg['TGLJAM']?></td>
    <td align="center"><?=$plg['VA']?></td>
    <td align="center"><?=$plg['VB']?></td>
    <td align="center"><?=$plg['VC']?></td>
    <td align="center"><?=$plg['IA']?></td>
    <td align="center"><?=$plg['IB']?></td>
	<td align="center"><?=$plg['IC']?></td>
  </tr>
  <?php } //end foreach ?> 
 </tbody>
</table>
