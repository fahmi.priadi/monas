<?php
class Rekap_model extends Model
{
	function Rekap_model()
	{
		parent::Model();
		$this->load->library('session');
	}
	
	function get_data_plg_idpel($idpel, $jenis_meter, $unitup)
	{
		$sql = "SELECT T.IDMETER, T.NOPEL, T.NAMA, T.ALAMAT, T.DAYATPS, 
						T.GOLONGAN AS TARIF, T.KDJENISMETER 
				FROM METER T WHERE T.NOPEL='".$idpel."' 
				AND TRIM(T.KDJENISMETER)='".$jenis_meter."' AND T.UNITUP='".$unitup."'" ;
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->row();
	}
	
	function get_rekap_perbulan($blth)
	{
		$sql = "SELECT * FROM JML_PLG_PERBULAN_GAB
				WHERE BLTH=".$blth;
				
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_rekap_perbulan2($blth)
	{
		$sql = "SELECT * FROM plg_kdunit_perbulan_gab
				WHERE BLTH=".$blth;
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_jml_plg()
	{
		$sql = "SELECT * FROM JML_PLG_AMR_GAB  ORDER BY BLTH DESC";
				
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_jml_plg_new($sort)
	{
		if($sort == 'ASC') $urut = " ORDER BY BLTH ASC";
		else $urut = "";
		$sql = "SELECT * FROM (
				SELECT * FROM JML_PLG_AMR_GAB ORDER BY BLTH DESC) WHERE ROWNUM < 13 ".$urut;
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_jml_kwh_pantau()
	{
		$sql = "SELECT * FROM JML_PLG_AMR_GAB8  ORDER BY BLTH DESC";		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_jml_kwh_pantau_detail($blth)
	{
		$sql = "SELECT * FROM JML_PLG_PERBULAN_GAB8
				WHERE BLTH='".$blth."'";
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}

	function get_rekap_kwh_pantau_perbulan($blth)
	{
		$sql = "SELECT * FROM JML_PLG_PERBULAN_GAB8
				WHERE BLTH=".$blth;
				
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}

	
	function get_rinci_rekap_perbulan($blth, $unitup, $awal, $akhir)
	{
		$sql = "SELECT *
				  FROM (SELECT ROWNUM AS NOMOR, a.*, b.jml_penarikan as JML_LP
						  FROM rekap_penarikan_meter a
						  left join rekap_penarikan b
							on a.idmeter = b.idmeter
						   and a.kdjenismeter = b.kdjenismeter
						   and a.unitup = b.unitup
						   and a.blth = b.blth
						 WHERE a.BLTH = $blth
						   AND a.UNITUP = '$unitup')
				 WHERE NOMOR BETWEEN ".$awal." AND ".$akhir;
				
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_rinci_rekap_perbulan_no($blth, $unitup, $awal, $akhir)
	{
		$sql = "SELECT *
				  FROM (SELECT ROWNUM AS NOMOR, T.*
						  FROM METERALL T
						 WHERE BLTH = $blth
						   AND UNITUP = '$unitup'
						   AND IDMETER NOT IN (SELECT IDMETER
						                         FROM REKAP_PENARIKAN_METER 
						                        WHERE BLTH = $blth
						                          AND UNITUP = '$unitup')
						)
				 WHERE NOMOR BETWEEN ".$awal." AND ".$akhir;
				
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_rinci_rekap_kwh_pantau_perbulan($blth, $unitup, $awal, $akhir)
	{
		$sql = "SELECT *
				  FROM (SELECT ROWNUM AS NOMOR, T.*
				          FROM REKAP_PENARIKAN_METER8 T
				         WHERE BLTH = ".$blth."
				           AND UNITUP = '".$unitup."')
				 WHERE NOMOR BETWEEN ".$awal." AND ".$akhir;
				
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_rinci_rekap_plg_kwh_pantau_perbulan($blth, $unitup, $awal, $akhir)
	{
		$sql = "SELECT * FROM (
					SELECT ROWNUM AS NOMOR, T.* FROM
					METERALL T WHERE BLTH = '".$blth."' AND UNITUP = '".$unitup."' AND KDUNIT = '54888')
				WHERE NOMOR BETWEEN ".$awal." AND ".$akhir;
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_max_blth()
	{
		$sql = "SELECT max(BLTH) BLTH FROM V_BLTH";
		$query	= $this->db->query($sql);
		return $query->row()->BLTH;
	}
	
	function get_blth()
	{
		$sql = "SELECT BLTH FROM V_BLTH ORDER BY BLTH DESC" ;
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_blth_kwh_pantau()
	{
		$sql = "SELECT DISTINCT(BLTH) AS BLTH FROM JML_PLG_PERBULAN_GAB8 ORDER BY BLTH DESC" ;
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}

	
	function get_nama_aream($kdarea)
	{
		$sql = "SELECT NAMA_AREAM FROM AREA WHERE KDAREAM='".$kdarea."'";
		
		$query	= $this->db->query($sql);
		return $query->row()->NAMA_AREAM;
	}
	
	function get_nama_area($kdarea)
	{
		$sql = "SELECT NAMA_AREA FROM AREA WHERE KDAREA=".$kdarea;
		
		$query	= $this->db->query($sql);
		return $query->row()->NAMA_AREA;
	}
	
	function get_area_detail_download($blth,$unitup,$opsi,$awal,$akhir)
	{
		$tabel = "meter".substr($blth,-2).substr($blth,2,2);
		if($opsi == '1') {
			$sql = "SELECT * FROM (
					SELECT ROWNUM AS rownumber, a.*, b.jml_penarikan as JML_LP
					  FROM (select * from meterall where blth = '$blth') a
					  left join rekap_penarikan b
						on a.idmeter = b.idmeter
					   and a.kdjenismeter = b.kdjenismeter
					   and a.unitup = b.unitup
					   and a.blth = b.blth
					 WHERE a.kdunit = '$unitup'
					   AND a.idmeter IN
						   (SELECT b.idmeter FROM rekap_penarikan b WHERE b.blth = '$blth'))
					WHERE rownumber BETWEEN ".$awal." AND ".$akhir;
		} else if($opsi == '2') {
			$sql = "SELECT * FROM (
					SELECT ROWNUM AS rownumber,a.*, '0' AS JML_LP FROM (select * from meterall where blth = '$blth') a
					WHERE a.kdunit='".$unitup."' AND a.idmeter NOT IN (SELECT b.idmeter FROM rekap_penarikan b 
					WHERE b.blth='".$blth."'  ))
					WHERE rownumber BETWEEN ".$awal." AND ".$akhir;
		}
		//echo $sql;
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_blth_rekap_login()
	{
		$sql = "SELECT DISTINCT(bln_thn) AS bln_thn FROM v_rekap_area_per_jabatan WHERE bln_thn IS NOT NULL";
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_rekap_login($blth)
	{
		$sql = "select a.kdarea,case when a.nama_area=' .Semua Area.' then 'KANTOR DISTRIBUSI' else a.nama_area end nama_area,nvl(b.manajer,0) manajer,nvl(b.asman,0) asman,nvl(b.spv,0) spv
  from v_area a,
       (select kdarea,
               nvl(sum(decode(kdjab, 1, jml_login, 0)), 0) as MANAJER,
               nvl(SUM(DECODE(kdjab, 2, jml_login, 0)), 0) as ASMAN,
               nvl(SUM(DECODE(kdjab, 3, jml_login, 0)), 0) as SPV
          from v_rekap_area_per_jabatan where bln_thn='".$blth."' 
         GROUP BY kdarea) b
 where a.KDAREA  = b.kdarea (+)
";
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_tgl_download($unitup, $blth)
	{
		$sql = "select unitup,tg_hari tg_download,count(*) jml_plg 
				from (select b.unitup,a.tg_hari from rekap_hari a, meter b where a.idmeter=b.idmeter and b.kdunit<>'54888' and substr(a.tg_hari,1,6) = '".$blth."') 
				where unitup = '".$unitup."' group by unitup,tg_hari order by tg_hari";
		//echo $sql;
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_tgl_download2($kdunit, $blth)
	{
		$sql = "select kdunit,tg_hari tg_download,count(*) jml_plg 
				from (select b.kdunit,a.tg_hari from rekap_hari a, meter b where a.idmeter=b.idmeter and b.kdunit<>'54888' and substr(a.tg_hari,1,6) = '".$blth."') 
				where kdunit = '".$kdunit."' group by kdunit,tg_hari order by tg_hari";
				
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_rekap_plg_all($blth, $batas_awal, $batas_akhir)
	{
		$sql = "select * from 
				(select rownum as rownumber, t.* from v_meter_all t where blth=$blth order by nopel)
				where rownumber between $batas_awal and $batas_akhir";
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_jml_plg_all($blth)
	{
		$sql = "select count(1) as JML from meterall where blth = ".$blth;
		
		$query	= $this->db->query($sql);
		return $query->row()->JML;
	
	}
	
}
?>