<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>public/css/table_default.css" />

<p>
<table width="100%"  class="table table-bordered mb30">
 
  <tr>
    <th><div align="center">IDPEL</div></th>
    <th><div align="center">NOMETER</div></th>
    <th><div align="center">NAMA</div></th>
    <th><div align="center">ALAMAT</div></th>
    <th><div align="center">TARIF</div></th>
    <th><div align="center">DAYA (VA)</div></th>
  </tr>
  
  <?php 
  $i=0;
  foreach($result as $plg){ if($i%2==0)
	$row="row-a";
  else $row="row-b";
  ?>
  <tr class="<?=$row?>">
    <td align="center"><?=anchor('pelanggan/search_detail/'.trim($plg['NOPEL']).'/'.trim($plg['IDMETER']),$plg['NOPEL'])?></td>
    <td align="center"><?=$plg['IDMETER']//anchor('pelanggan/detil_pelanggan/'.$plg['IDPEL'],$plg['IDPEL'])?></td>
    <td><?=$plg['NAMA']?></td>
    <td><?=$plg['ALAMAT']?></td>
    <td align="center"><?=$plg['GOLONGAN']?></td>
	<td align="right"><?php echo number_format($plg['DAYATPS'],0,'','.');?></td>
  </tr>
  <?php $i++;} //end foreach ?> 
  
</table>
<?
if($jml<1) echo 'Data Pelanggan Tidak Ditemukan';
?>
</p>

