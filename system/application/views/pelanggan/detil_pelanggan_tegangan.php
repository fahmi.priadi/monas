<script language="Javascript" src="<?=base_url()?>public/lib/Charts/FusionCharts.js"></script>
<table width="100%">
  <tr >
    <td width="11%"><div align="left">IDPEL</div></td>
    <td width="1%"><strong>:</strong></td>
    <td width="37%"><strong>
    <?=$plg->NOPEL?></strong></td>
    <td width="11%"><div align="left">TARIF</div></td>
    <td width="1%"><strong>:</strong></td>
    <td width="39%"><strong><?=$plg->TARIF?></strong></td>
  </tr>
  <tr>
    <td><div align="left">IDMETER</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=$plg->IDMETER?></strong></td>
    <td><div align="left">DAYA</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=$plg->DAYATPS?></strong></td>
  </tr>
  <tr>
    <td><div align="left">NAMA</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=$plg->NAMA?></strong></td>
    <td><div align="left">KDJENISMETER</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=$plg->KDJENISMETER?></strong></td>
  </tr>
  <tr>
    <td><div align="left">ALAMAT</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=$plg->ALAMAT?></strong></td>
    <td><div align="left">UNITUP</div></td>
    <td><strong>:</strong></td>
    <td><strong><?=strtoupper($plg->NAMA_AREA)?></strong></td>
  </tr>
</table>
<br />
<?=anchor_popup('pelanggan/catatan/'.$idpel,'<span class="glyphicon glyphicon-pencil"></span> Catatan hasil pemeriksaan',array())?>
<p><h5 class="subtitle mb5">Tabel Data Tidak Normal&nbsp;<? if(1500>65000) echo "Jumlah Data Melebihi Kapasitas Ms.Excel<br>Silakan Persempit Kriteria Pencarian"; else echo anchor('pelanggan/excel_tegangan/'.$idpel.'/'.$tgl_awal.'/'.$tgl_akhir.'/'.$teg_min.'/'.$teg_normal.'/'.$unitup.'/'.$jenis_meter.'/'.$idmeter,'<i class="fa fa-download"></i>&nbsp;Excel')?></h5></p>
<div class="table-responsive">
<table class="table" id="table2">
 <thead>
  <tr>
    <th>IDMETER</th>
    <th>TGLJAM</th>
    <th>VR</th>
    <th>VS</th>
    <th>VT</th>
    <th>IR</th>
    <th>IS</th>
    <th>IT</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach($result as $plg){  ?>
  <tr >
    <td align="center"><?=$plg['IDMETER']?></td>
    <td align="center"><?=$plg['TGLJAM']?></td>
    <td align="center"><?=$plg['VA']?></td>
    <td align="center"><?=$plg['VB']?></td>
    <td align="center"><?=$plg['VC']?></td>
    <td align="center"><?=$plg['IA']?></td>
    <td align="center"><?=$plg['IB']?></td>
	<td align="center"><?=$plg['IC']?></td>
  </tr>
  <?php } //end foreach ?> 
 </tbody>
</table>
<p>&nbsp;</p>
</div>
<p>
<?php
 echo $graph_hist_voltase;
?>	
</p>
<p>
<?php
 echo $graph_hist_ampere;
?>	
</p>
<p><h5 class="subtitle mb5">Tabel Data  (All)&nbsp;<? if(1500>65000) echo "Jumlah Data Melebihi Kapasitas Ms.Excel<br>Silakan Persempit Kriteria Pencarian"; else echo anchor('pelanggan/excel_tegangan_all/'.$idpel.'/'.$tgl_awal.'/'.$tgl_akhir.'/'.$teg_min.'/'.$teg_normal.'/'.$unitup.'/'.$idmeter.'/'.$jenis_meter,'<i class="fa fa-download"></i>&nbsp;Excel')?></h5></p>
<div class="table-responsive">
<table class="table" id="table3">
 <thead>
  <tr>
    <th>IDMETER</th>
    <th>TGLJAM</th>
    <th>VR</th>
    <th>VS</th>
    <th>VT</th>
    <th>IR</th>
    <th>IS</th>
    <th>IT</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach($result_all as $plg){  ?>
   <tr <? if($plg['VA']==0 or $plg['VB']==0 or $plg['VC']==0) echo "class='gradeX'";?>>
    <td align="center"><?=$plg['IDMETER']?></td>
    <td align="center"><?=$plg['TGLJAM']?></td>
    <td align="center"><?=$plg['VA']?></td>
    <td align="center"><?=$plg['VB']?></td>
    <td align="center"><?=$plg['VC']?></td>
    <td align="center"><?=$plg['IA']?></td>
    <td align="center"><?=$plg['IB']?></td>
	<td align="center"><?=$plg['IC']?></td>
  </tr>
  <?php } //end foreach ?> 
 </tbody>
</table>
<p>&nbsp;</p>
</div>
<p>
<?php
 echo $graph_hist_voltase_all;
?>	
</p>
<p>
<?php
 echo $graph_hist_ampere_all;
?>	
</p>