<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>public/css/table_default.css" />

<p>
<table width="100%"  class="table table-bordered mb30">
 
  <tr>
    <th><div align="center">BLTH</div></th>
    <th><div align="center">IDPEL</div></th>
    <th><div align="center">NAMA</div></th>
    <th><div align="center">ALAMAT</div></th>
    <th><div align="center">TARIF</div></th>
    <th><div align="center">DAYA (VA)</div></th>
	<th><div align="center">JUMLAH LP</div></th>
  </tr>
  
  <?php 
  $i=0;
  foreach($result as $plg){ if($i%2==0)
	$row="row-a";
  else $row="row-b";
  ?>
  <tr class="<?=$row?>">
    <td align="center"><?=$plg['BLTH']?></td>
    <td align="center"><?=$plg['NOPEL']//anchor('pelanggan/detil_pelanggan/'.$plg['IDPEL'],$plg['IDPEL'])?></td>
    <td><?=$plg['NAMA']?></td>
    <td><?=$plg['ALAMAT']?></td>
    <td align="center"><?=$plg['GOLONGAN']?></td>
	<td align="right"><?php echo number_format($plg['DAYATPS'],0,'','.');?></td>
	<td align="right"><?=$plg['JML_LP']?></td>
  </tr>
  <?php $i++;} //end foreach ?> 
  <tr>
    <td colspan="7" align="center">Jumlah data: <strong><?=number_format($jml_data,0,'','.')?></strong> record</td>
  </tr>
  <tr>
    <td colspan="7" align="center"><?=$paging?></td>
  </tr>
  <tr>
    <td colspan="7" align="center"><? if($jml_data>65000) echo "Jumlah Data Melebihi Kapasitas Ms.Excel<br>Silakan Persempit Kriteria Pencarian"; else echo anchor('rekap/excel_rinci_area/'.$plg['BLTH'].'/'.$plg['UNITUP'].'/'.$jml_data,'<img src="'.base_url().'public/images/Excel-icon.png" width="12" height="9"/>&nbsp;Export to Excel')?></td>
  </tr>
</table>
</p>

