<script type="text/javascript">
	$(function() {
		$(".sph_tanggal").datepicker({
			maxDate: "+0D"
		});
	});
	function numberOnly(angka){
		var charCode = (angka.which) ? angka.which : event.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
		else return true;
	}
	$(document).ready(function(){
		$("#jenis").chained("#kategori");
	});
</script>
<div class="contentpanel">  
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title"><?=$subtitle?></h4>
          <p>Fitur kWh plus minus digunakan untuk mencari selisih kwh jual dan kwh beli pada pelanggan. </p>
        </div>
        <div class="panel-body panel-body-nopadding">
          
            <?=form_open('pelanggan/kwh_pm', array('class'=>'form-horizontal form-bordered'))?>
            <div class="form-group">
              <label class="col-sm-3 control-label">UNITUP</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="unitup" >
                  <? foreach($area as $data) { ?>
                  <option value="<?=$data['KDAREA']?>"><?=$data['NAMA_AREA']?></option>
                   <? } //end foreach ?>
                </select>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-3 control-label">Tahun / Bulan</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="blth" >
                  <?php
					foreach($tahun as $r){
						?><option value="<?php echo $r['BLTH'];?>"><?php echo substr_replace($r['BLTH'],'/',4,0);?>&nbsp;&nbsp;</option>
						<?php
					}
					?>
                </select>
              </div>
            </div>
            
           <!--<div class="form-group">
              <label class="col-sm-3 control-label">Jenis Meter</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="jenismeter">
                    <option value="10">Tidak Termasuk Meter WDT</option>
                    <option value="11" selected="Selected">Termasuk Meter WDT</option>
                </select>
              </div>
            </div>-->
            
            <div class="form-group">
              <label class="col-sm-3 control-label">Persentase</label>
              <div class="col-sm-6">
              <input type="text" name="persen" id="persen" size="5" value="10" class="form-control" onkeypress="return numberOnly(event);" >
                <span class="help-block">Persen (%)</span>
              </div>
            </div>
            
           <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-6 col-sm-offset-3">
				  <button id="blok" class="btn btn-primary" type="submit">Submit</button>&nbsp;
				</div>
			 </div>
		  </div><!-- panel-footer -->
            
          </form>
          
        </div><!-- panel-body -->
        
      </div>
      </div>
</div>