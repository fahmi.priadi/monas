<link rel="stylesheet" href="<? echo base_url()?>public/datepicker/themes/base/jquery.ui.all.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/datatables/css/demo_table.css" />

<script src="<? echo base_url()?>public/datepicker/jquery-1.4.2.js"></script>
<script src="<? echo base_url()?>public/datepicker/ui/jquery.ui.core.js"></script>
<script src="<? echo base_url()?>public/datepicker/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" language="javascript" src="<? echo base_url()?>public/datepicker/ui/jquery.ui.datepicker.js"></script>		

<script type="text/javascript" language="javascript" src="<?=base_url()?>public/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#example').dataTable( {
        "sPaginationType": "full_numbers"
   		} );
	} );
</script>
<script>
	$(function() {
		$( "#datepicker" ).datepicker();
		
		$( "#datepicker2" ).datepicker();
		
	});
</script>

<h1><a href="#"><?php echo $title; ?></a></h1>
<table width="486" border="0" cellpadding="15" cellspacing="1" id="result">
  <tr>
  <td width="454" >
		  <?=form_open('pelanggan/tegangan', array('name'=>'form1'))?>
        <table width="100%" border="0">
  <tr>
    <td width="40%">Tanggal</td>
    <td width="60%">
      <input type="text" name="tgl1" id="datepicker" size="10"/>&nbsp;s.d&nbsp;<input type="text" name="tgl2" id="datepicker2" size="10"/>
   </td>
  </tr>
  <tr>
    <td>Tegangan</td>
    <td><select name="penanda">
      <option value="0">KURANG</option>
      <option value="1">LEBIH</option>
    </select>
      &nbsp;dari&nbsp;
      <input type="text" name="voltase" size="10"/>&nbsp;Volt</td>
  </tr>
  <tr>
    <td>Minimum Sejumlah</td>
    <td><input type="text" name="jumlah" size="10"/>&nbsp;Kali</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="Submit" value="Submit" /></td>
  </tr>
</table>
 </form>
</td></tr></table>
<br />
<hr>
<br />
<?php
	if($tanda){	
	$i=0;
?>
<br/>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
 <thead>
  <tr>
    <th>KDJENISMETER</th>
    <th>TGLJAM</th>
    <th>VA</th>
    <th>VB</th>
    <th>VC</th>
    <th>IA</th>
    <th>IB</th>
    <th>IC</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach($result as $plg){  ?>
  <tr >
    <td align="center"><?=$plg['KDJENISMETER']?></td>
    <td align="center"><?=$plg['TGLJAM']?></td>
    <td align="center"><?=$plg['VA']?></td>
    <td align="center"><?=$plg['VB']?></td>
    <td align="center"><?=$plg['VC']?></td>
    <td align="center"><?=$plg['IA']?></td>
    <td align="center"><?=$plg['IB']?></td>
	<td align="center"><?=$plg['IC']?></td>
  </tr>
  <?php } //end foreach ?> 
 </tbody>
</table>
<?php	
	}							
?>

