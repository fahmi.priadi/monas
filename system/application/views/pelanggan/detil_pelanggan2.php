<link rel="stylesheet" href="<?php echo base_url();?>/public/tabs/themes/base/jquery.ui.all.css">
    <script src="<?php echo base_url();?>/public/Charts/FusionCharts.js"></script>
	<script src="<?php echo base_url();?>/public/tabs/jquery-1.6.2.js"></script>
	<script src="<?php echo base_url();?>/public/tabs/ui/jquery.ui.core.js"></script>
	<script src="<?php echo base_url();?>/public/tabs/ui/jquery.ui.widget.js"></script>
	<script src="<?php echo base_url();?>/public/tabs/ui/jquery.ui.tabs.js"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>/public/tabs/demos.css">
	<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
	</script>
<h1><a href="#"><?php echo $title; ?></a>&nbsp;&nbsp;<?=anchor('pelanggan/excel_sorek/'.$idpel,'<img src="'.base_url().'public/images/Excel-icon.png" width="16" height="12"/>');?> </h1>
<br/>

<table width="100%">
 <?php foreach($plg as $row) { ?>
  <tr class="row-b">
    <td><div align="right">UNITUP</div></td>
    <td><strong><?=$row['UNITUP']?>&nbsp;-&nbsp;<?=$row['NAMA_AREA']?></strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="row-a">
    <td width="15%"><div align="right">IDPEL</div></td>
    <td width="35%"><strong><?=$row['IDPEL']?></strong></td>
    <td width="15%"><div align="right">TARIF</div></td>
    <td width="35%"><strong><?=$row['TARIF']?></strong></td>
  </tr>
  <tr class="row-b">
    <td><div align="right">NAMA</div></td>
    <td><strong><?=$row['NAMA']?></strong></td>
    <td><div align="right">DAYA</div></td>
    <td><strong><?=$row['DAYA']?></strong></td>
  </tr>
  <tr class="row-a">
    <td><div align="right">ALAMAT</div></td>
    <td><strong><?=$row['ALAMAT']?></strong></td>
    <td><div align="right">KDDK</div></td>
    <td><strong><?=$row['RBM']?></strong></td>
  </tr>
 <? } ?>
</table>
	

<div class="demo">

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Info Rekening</a></li>
		<li><a href="#tabs-2">History Rekening</a></li>
		<li><a href="#tabs-3">History kWh</a></li>
		<li><a href="#tabs-4">History Stand</a></li>
	</ul>
	<div id="tabs-1">
		<p><? $this->load->view('pelanggan/inforek'); ?></p>
	</div>
	<div id="tabs-2">
		<p><? $this->load->view('pelanggan/hist_rek'); ?></p>
	</div>
	<div id="tabs-3">
		<p><? $this->load->view('pelanggan/hist_kwh'); ?></p>
	</div>
	<div id="tabs-4">
		<p><? $this->load->view('pelanggan/hist_stand'); ?></p>
	</div>
</div>

</div><!-- End demo -->



<div class="demo-description">
<p>Click tabs to swap between content.</p>
</div><!-- End demo-description -->

