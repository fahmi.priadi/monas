<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themepixels.com/demo/webpage/bracket/tables.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 06:35:54 GMT -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?=base_url()?>public/images/favicon.png" type="image/png">

  <title>Monas - Monitoring dan Analisa Sistem AMR</title>

  <link href="<?=base_url()?>public/css/style.default.css" rel="stylesheet">
  <link href="<?=base_url()?>public/css/jquery.datatables.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <? $this->load->view('left_menu');?>
  
  <div class="mainpanel">
    
    <div class="headerbar">
      
      <a class="menutoggle"><i class="fa fa-bars"></i></a>
    
      <div class="header-right">
        <ul class="headermenu">
           <? $this->load->view('user_menu'); ?>
        </ul>
      </div><!-- header-right -->
      
    </div><!-- headerbar -->
        
    <div class="pageheader">
      <h2><i class="fa fa-table"></i> <?=$title?></h2>
    </div>
    
    <div class="contentpanel"><!-- row -->
      <!-- row -->
      <!-- row -->
<div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><?=$subtitle?></h3>
          <p><?php $this->load->view($isi);?></p>
        </div>
      </div><!-- panel -->
    </div>
    <!-- contentpanel -->
    <p>&nbsp;</p>
  </div><!-- mainpanel -->
  <!-- rightpanel -->
</section>


<script src="<?=base_url()?>public/js/jquery-1.10.2.min.js"></script>
<script src="<?=base_url()?>public/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>public/js/modernizr.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.sparkline.min.js"></script>
<script src="<?=base_url()?>public/js/toggles.min.js"></script>
<script src="<?=base_url()?>public/js/retina.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.cookies.js"></script>

<script src="<?=base_url()?>public/js/jquery.datatables.min.js"></script>
<script src="<?=base_url()?>public/js/chosen.jquery.min.js"></script>

<script src="<?=base_url()?>public/js/custom.js"></script>
<script>
  jQuery(document).ready(function() {
    
    jQuery('#table1').dataTable();
    
    jQuery('#table2').dataTable({
      "sPaginationType": "full_numbers"
    });
    
    // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
    
    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });
        
        return false;
    });
    
    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });
  
  
  });
</script>

</body>

<!-- Mirrored from themepixels.com/demo/webpage/bracket/tables.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 06:35:54 GMT -->
</html>
