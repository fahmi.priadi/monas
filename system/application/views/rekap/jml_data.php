<script src="<?php echo base_url();?>public/lib/Charts/FusionCharts.js"></script>
<!-- <link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>public/css/table_default.css" /> -->

<table width="100%" align="center">
  <tr>
  	<td ><? echo $graph; ?><br /></td>   
  </tr>
  	<tr><td align="center">
    <table width="50%" class="table table-bordered mb30">
    <thead>
     <tr >
     	<th><div align="center">BLTH</div></th>
        <th><div align="center">JML PLG</div></th>
		<th><div align="center">JML METER</div></th>
        <th colspan="2"><div align="center">BERHASIL DOWNLOAD</div></th>
        <th><div align="center">BLM BERHASIL</div></th>
     </tr>
    </thead> 
  
  <?php 
  $i=0;
  $tot_plg = 0;
  foreach($result as $plg){
  $tot_plg += $plg['JML_PLG'];
  
  ?>
  <tr >
    <td align="center"><?=$plg['BLTH']?></td>
	<td align="center"><?=$plg['JML_PLG_ALL_ID']?></td>
	<td align="center"><?=anchor('rekap/plg_all/new/'.$plg['BLTH'].'/'.$plg['JML_PLG_ALL'], number_format($plg['JML_PLG_ALL'],0,'','.')) ?></td>
    <td align="center"><?=anchor('rekap/jml_plg/new/'.$plg['BLTH'], number_format($plg['JML_PLG'],0,'','.'))?></td>
	<td align="center"><?=number_format(($plg['JML_PLG']/$plg['JML_PLG_ALL'])*100,2,',','.')?>&nbsp;%</td>
	<td align="center"><?=$plg['SELISIH']?></td>
  </tr>
  
  <?php $i++;} //end foreach ?> 
  
</table></td>
     </tr>
	 <tr align="center"><td><?=anchor('rekap/excel_rekap_jml_plg/','<img src="'.base_url().'public/images/Excel-icon.png" width="12" height="9"/>&nbsp;Export to Excel')?></td></tr>
</table>


</p>

