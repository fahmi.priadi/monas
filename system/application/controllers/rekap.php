<?php

class Rekap extends Controller {

	function Rekap()
	{
		parent::Controller();	
		$this->load->model('rekap_model','model');
		$this->load->library('session');
	}
	
	function index()
	{
		redirect('rekap/jml_data/new');
	}
	
	function plg_all()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/index.php/rekap/plg_all/';
		$config['per_page'] = '20';		
		$batas_awal=0;
		
		if($this->uri->segment(3)=="new") 
		{		
			//get parameter data
			$blth		= $this->uri->segment(4);
			$total_rows = $this->model->get_jml_plg_all($blth);//$this->uri->segment(5);
								
			$arrdata = array(
						   'blth'		=> $blth,
						   'total_rows' => $total_rows
		               );
			//set new session
			$this->session->set_userdata($arrdata);
		} else if($this->uri->segment(3)!="" )
		{
			$batas_awal = $this->uri->segment(3);
		}//end of else
		 
		$config['total_rows'] = $this->session->userdata('total_rows');
		$this->pagination->initialize($config);
		$data['paging'] = $this->pagination->create_links();
		$data['jml_data'] = $this->session->userdata('total_rows');
		$data['awal']	= $batas_awal+1;
		$data['akhir']	= $batas_awal+$config['per_page'];
		
		$this->WriteToLog("REKAP", "PLG ALL");
		
		//query data
		$data['result'] = $this->model->get_rekap_plg_all($this->session->userdata('blth'),
																$data['awal'], 
																$data['akhir']);
		$data['blth']   = $this->session->userdata('blth');
		$data['title']  = "REKAP JUMLAH PELANGGAN";
		$data['subtitle']  = "Rinci Data Pelanggan";
		$data['isi']	= "rekap/plg_all";
		$this->load->view('template', $data);
	
	}
	
	function jml_data()
	{		
		//page title
		$data['title']  = "REKAP JUMLAH PELANGGAN";
		$data['subtitle']  = "";
		//load library fusion chart
		$this->load->library('fusioncharts') ;
        $this->swfCharts  = base_url().'public/lib/Charts/MSColumn2D.swf' ;
		
		//get data from DB
		$data['result'] = $this->model->get_jml_plg_new('');
		
		//grafik plg
		$jml_xml = $this->jml_data_xml($this->model->get_jml_plg_new('ASC'));
		$data['graph'] = $this->fusioncharts->renderChart($this->swfCharts,'',$jml_xml,"amr", 700, 400, false, false) ;
		
		$this->WriteToLog("REKAP", "JML DATA");
		
		$data['isi']	= "rekap/jml_data";
		$this->load->view('template', $data);
	}
	
	function jml_plg()
	{
		//page title
		$data['title']  = "REKAP JUMLAH PELANGGAN AREA PERBULAN";
		$data['subtitle']  = "";
		
		//get blth
		if($this->uri->segment(3)=="new")
		{
			if($this->uri->segment(4)!="")
				$data['blth'] = $this->uri->segment(4);
			else $data['blth'] = date('Ym'); //date NOW
		}
		else
		{
			$data['blth'] = $_POST['blth'];
		}
		
		//load library fusion chart
		$this->load->library('fusioncharts') ;
        $this->swfCharts  = base_url().'public/lib/Charts/MSColumn2D.swf' ;
		
		//WRITE TO LOG
		$this->WriteToLog("REKAP","JML PLG");
		
		//get data from DB
		$data['result'] = $this->model->get_rekap_perbulan($data['blth']);
		
		//get rekep per bulan per area
		$data['result2'] = $this->model->get_rekap_perbulan2($data['blth']);
		
		//grafik 
		$jml_xml = $this->jml_plg_xml($data['result']);
		$data['graph'] = $this->fusioncharts->renderChart($this->swfCharts,'',$jml_xml,"fakm", 750, 400, false, false) ;
		$data['list_blth']   = $this->model->get_blth();
		$data['isi']	= "rekap/jml_plg";
		$this->load->view('template', $data);
	}
	
	function rinci_area()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/index.php/rekap/rinci_area/';
		$config['per_page'] = '20';		
		$batas_awal=0;
		
		if($this->uri->segment(3)=="new") 
		{		
			//get parameter data
			$blth		= $this->uri->segment(4);
			$unitup		= $this->uri->segment(5);
			$total_rows = $this->uri->segment(6);
								
			$newdata = array(
						   'unitup' 	=> $unitup,
						   'blth'		=> $blth,
						   'total_rows' => $total_rows
		               );
			//set new session
			$this->session->set_userdata($newdata);
			
		} else if($this->uri->segment(3)!="new" )
		{
			$batas_awal = $this->uri->segment(3);
				
		}//end of else
		
		//pagination
		$config['total_rows'] = $this->session->userdata('total_rows');
		$this->pagination->initialize($config);
		$data['paging'] = $this->pagination->create_links();
				
		//jml data
		$data['jml_data'] = $this->session->userdata('total_rows');
			
		//query data
		$data['result'] = $this->model->get_rinci_rekap_perbulan($this->session->userdata('blth'),
																$this->session->userdata('unitup'),
																$batas_awal+1, 
																$batas_awal+$config['per_page']);
		//WRITE TO LOG
		$this->WriteToLog("REKAP","RINCI AREA");
		
		$data['title']  = "Rinci Data Area ".$this->model->get_nama_aream($this->session->userdata('unitup'));
		$data['subtitle'] = "";
		$data['isi']	= "rekap/rinci_area";
		$this->load->view('template', $data);
	}
	
	function rinci_area_no()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/index.php/rekap/rinci_area_no/';
		$config['per_page'] = '20';		
		$batas_awal=0;
		
		if($this->uri->segment(3)=="new") 
		{		
			//get parameter data
			$blth		= $this->uri->segment(4);
			$unitup		= $this->uri->segment(5);
			$total_rows = $this->uri->segment(6);
								
			$newdata = array(
						   'unitup' 	=> $unitup,
						   'blth'		=> $blth,
						   'total_rows' => $total_rows
		               );
			//set new session
			$this->session->set_userdata($newdata);
			
		} else if($this->uri->segment(3)!="new" )
		{
			$batas_awal = $this->uri->segment(3);
				
		}//end of else
		
		//pagination
		$config['total_rows'] = $this->session->userdata('total_rows');
		$this->pagination->initialize($config);
		$data['paging'] = $this->pagination->create_links();
				
		//jml data
		$data['jml_data'] = $this->session->userdata('total_rows');
			
		//query data
		$data['result'] = $this->model->get_rinci_rekap_perbulan_no($this->session->userdata('blth'),
																$this->session->userdata('unitup'),
																$batas_awal+1, 
																$batas_awal+$config['per_page']);
		//WRITE TO LOG
		$this->WriteToLog("REKAP","RINCI AREA");
		
		$data['title']  = "Rinci Data Area ".$this->model->get_nama_aream($this->session->userdata('unitup'));
		$data['subtitle'] = "";
		$data['isi']	= "rekap/rinci_area_no";
		$this->load->view('template', $data);
	}
	
	function area_detail_download()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/index.php/rekap/area_detail_download/';
		$config['per_page'] = '20';
		$batas_awal=0;
		
		if($this->uri->segment(3)=="new") 
		{
			$var		= explode('-',$this->uri->segment(4));
			$blth		= $var[0];
			$unitup		= $var[1];
			$total_rows	= $var[2];
			$opsi		= $var[3];
			
			$newdata = array(
						   '_unitup' => $unitup,
						   '_blth' => $blth,
						   '_opsi' => $opsi,
						   '_total_rows' => $total_rows
		               );
			//set new session
			$this->session->set_userdata($newdata);
			
		} else {
			$batas_awal = $this->uri->segment(3);
			
		}
		
		//echo $this->session->userdata('_total_rows');
		//pagination
		$config['total_rows'] = $this->session->userdata('_total_rows');
		$this->pagination->initialize($config);
		$data['paging'] = $this->pagination->create_links();
		$data['jml_data'] = $this->session->userdata('_total_rows');
		$data['blth'] = $this->session->userdata('_blth');
		$data['opsi'] = $this->session->userdata('_opsi');
		$data['unitup'] = $this->session->userdata('_unitup');
		$data['result'] = $this->model->get_area_detail_download($this->session->userdata('_blth'),
																$this->session->userdata('_unitup'),
																$this->session->userdata('_opsi'),
																$batas_awal+1, 
																$batas_awal+$config['per_page']);
		//WRITE TO LOG
		$this->WriteToLog("REKAP","AREA DETAIL");
		
		$data['title']  = "Detail Rayon ".$this->model->get_nama_area($this->session->userdata('_unitup'));
		$data['subtitle'] = "";
		$data['isi']	= "rekap/area_detail";
		$this->load->view('template', $data);
	}
	
	function tgl_download(){
		//page title
		$data['title']  = "TANGGAL DOWNLOAD";
		$data['subtitle']  = "";
		$unitup = $this->uri->segment(3);
		$kode = $this->uri->segment(4);
		$blth = $this->uri->segment(5);
		
		//WRITE TO LOG
		//$this->WriteToLog("REKAP-FAKM",$data['blth']);
		
		//get data from DB
		$data['unitup'] = $unitup;
		$data['kode'] = $kode;
		$data['blth'] = $blth;
		if($kode == 1) $data['result'] = $this->model->get_tgl_download($unitup, $blth);
		elseif($kode == 2) $data['result'] = $this->model->get_tgl_download2($unitup, $blth);
		
		$data['isi']	= "rekap/tgl_download";
		$this->load->view('template', $data);
	}
	
	
	//function to build XML chart rekening
	function get_jml_xml($row_query)
	{
		$xml = "<chart palette='4' caption='Diagram Jumlah Pelanggan'>";
		foreach($row_query as $row_rek){
			$xml .= "<set label='".$row_rek['NAMA_AREAM']."' value='".$row_rek['JML_PLG']."'/>";
		}
		$xml .= "</chart>";
		
		return $xml;
	}
	
	function jml_plg_xml($data)
	{
		$cat = "";
		$sr1 = "";
		$sr2 = "";
		
		foreach($data as $row)
		{
			$cat .= "<category label='".$row['NAMA_AREAM']."'/>";
			$sr1 .= "<set value='".$row['JML_PLG_ALL']."'/>";
			$sr2 .= "<set value='".$row['JML_PLG']."'/>";
		}
		
		$xml = "<chart caption='Grafik Keberhasilan Download Load Profile AMR' xAxisName='Area' yAxisName='Jumlah' yAxisMinValue='5000' shownames='1' showvalues='0' decimals='0' >";
		$xml .= "<categories>$cat</categories>";
		$xml .= "<dataset seriesName='JML METER' color='AFD8F8' showValues='0'>$sr1</dataset>";
		$xml .= "<dataset seriesName='BERHASIL' color='F6BD0F' showValues='0'>$sr2</dataset></chart>";
		//echo $xml;
		return $xml;
	}
	
	function jml_data_xml($data)
	{
		$cat = "";
		$sr1 = "";
		$sr2 = "";
		
		foreach($data as $row)
		{
			$cat .= "<category label='".$row['BLTH']."'/>";
			$sr1 .= "<set value='".$row['JML_PLG_ALL']."'/>";
			$sr2 .= "<set value='".$row['JML_PLG']."'/>";
		}
		
		$xml = "<chart caption='Grafik Keberhasilan Download Load Profile AMR' xAxisName='Bulan' yAxisName='Jumlah' yAxisMinValue='5000' shownames='1' showvalues='0' decimals='0' >";
		$xml .= "<categories>$cat</categories>";
		$xml .= "<dataset seriesName='JML METER' color='AFD8F8' showValues='0'>$sr1</dataset>";
		$xml .= "<dataset seriesName='BERHASIL' color='F6BD0F' showValues='0'>$sr2</dataset></chart>";
		//echo $xml;
		return $xml;
		
	}
	
	
	//function to get excel data
	function excel_plg_all()
	{
		$this->load->plugin('to_excel');
		$blth = $this->uri->segment(3);
		$jml_data = $this->uri->segment(4);
		to_excel($this->model->get_rekap_plg_all($blth, 1, $jml_data), 'PLGALL_'.$blth);	
	}
	
	function excel_rekap_jml_plg()
	{
		$this->load->plugin('to_excel');
		to_excel($this->model->get_jml_plg(), 'REKAP_JML_PLG');
	}
	
	function excel_rekap_jml_data()
	{
		$this->load->plugin('to_excel');
		$blth = $this->uri->segment(3);
		to_excel($this->model->get_rekap_perbulan($blth), 'REKAP_JML_DATA_'.$blth);
	}
	
	function excel_rekap_jml_data2()
	{
		$this->load->plugin('to_excel');
		$blth = $this->uri->segment(3);
		to_excel($this->model->get_rekap_perbulan2($blth), 'REKAP_JML_DATA_'.$blth);
	}
	
	function excel_rekap_jml_kwh_pantau()
	{
		$this->load->plugin('to_excel');
		to_excel($this->model->get_jml_kwh_pantau(), 'REKAP_JML_KWH_PANTAU');
	}
	
	function excel_rekap_jml_kwh_pantau_detail()
	{
		$this->load->plugin('to_excel');
		$blth = $this->uri->segment(3);
		to_excel($this->model->get_jml_kwh_pantau_detail($blth), 'REKAP_JML_KWH_PANTAU_DETAIL');
		
	}

	
	function excel_rinci_area()
	{
		$this->load->plugin('to_excel');
		$blth = $this->uri->segment(3);
		$kdarea = $this->uri->segment(4);
		$jml_data = $this->uri->segment(5);
		to_excel($this->model->get_rinci_rekap_perbulan($blth, $kdarea, 1, $jml_data), 'REKAP_AREA_'.$kdarea.'_'.$blth);
	}
	
	function excel_rinci_area_no()
	{
		$this->load->plugin('to_excel');
		$blth = $this->uri->segment(3);
		$kdarea = $this->uri->segment(4);
		$jml_data = $this->uri->segment(5);
		to_excel($this->model->get_rinci_rekap_perbulan_no($blth, $kdarea, 1, $jml_data), 'REKAP_AREA_'.$kdarea.'_'.$blth);
	}
	
	function excel_rinci_area_kwh_pantau()
	{
		$this->load->plugin('to_excel');
		$blth = $this->uri->segment(3);
		$kdarea = $this->uri->segment(4);
		$jml_data = $this->uri->segment(5);
		to_excel($this->model->get_rinci_rekap_kwh_pantau_perbulan($blth, $kdarea, 1, $jml_data), 'REKAP_KWH_PANTAU_AREA_'.$kdarea.'_'.$blth);
	}
	
	function excel_rinci_plg_kwh_pantau()
	{
		$this->load->plugin('to_excel');
		$blth = $this->uri->segment(3);
		$kdarea = $this->uri->segment(4);
		$jml_data = $this->uri->segment(5);
		to_excel($this->model->get_rinci_rekap_plg_kwh_pantau_perbulan($blth, $kdarea, 1, $jml_data), 'REKAP_KWH_PANTAU_AREA_'.$kdarea.'_'.$blth);
	}
	
	function excel_area_detail_download()
	{
		$this->load->plugin('to_excel');
		$var		= explode('-',$this->uri->segment(3));
		$blth		= $var[0];
		$unitup		= $var[1];
		$jml_data	= $var[2];
		$opsi		= $var[3];
		to_excel($this->model->get_area_detail_download($blth,$unitup,$opsi,1,$jml_data),'REKAP_AREA_' . $unitup);
	}
	
	function excel_tgl_download()
	{
		$this->load->plugin('to_excel');
		$unitup = $this->uri->segment(3);
		$blth = $this->uri->segment(4);
		to_excel($this->model->get_tgl_download($unitup, $blth), 'TGL_DOWNLOAD_'.$unitup);
	}
	
	function excel_tgl_download2()
	{
		$this->load->plugin('to_excel');
		$unitup = $this->uri->segment(3);
		$blth = $this->uri->segment(4);
		to_excel($this->model->get_tgl_download2($unitup, $blth), 'TGL_DOWNLOAD_'.$unitup);
	}
	
	function sip_logs()
	{
		$data['title']  = "SIP LOGS";
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/index.php/rekap/sip_logs/';
		$config['per_page'] = '50';		
		$batas_awal=0;
		
		if($this->uri->segment(3)=="new"){
		
			$total_rows = $this->model->get_log_count();
					
			//SET ARRAY FOR INPUT VARIABLE
			$newdata = array(
						   'total_rows' => $total_rows
						);
					
			//SET SESSION FOR ARRAY
			$this->session->set_userdata($newdata);
					
		}else if($this->uri->segment(3)!="" )
		{
			$batas_awal = $this->uri->segment(3);
		}//end of else
			
		//pagination
		$config['total_rows'] = $this->session->userdata('total_rows');
		$this->pagination->initialize($config);
		$data['paging'] = $this->pagination->create_links();
		$data['jml_data'] = $this->session->userdata('total_rows');
		
		//get data from DB
		$data['result'] = $this->model->get_log_data($batas_awal+1,	$batas_awal+$config['per_page']);
		
		$data['isi']	= "rekap/sip_logs";
		$this->load->view('tampilan', $data);
	
	}
	
	function log_apl()
	{
		$data['title']  = "REKAP LOG PER APL";

		//load library fusion chart
		$this->load->library('fusioncharts') ;
        $this->swfCharts  = base_url().'public/Charts/Bar2D.swf' ;
		
		//get data from DB
		$data['result'] = $this->model->get_log_apl();
		
		//grafik log
		$log_xml = $this->get_log_apl_xml($data['result']);
		$data['graph_log'] = $this->fusioncharts->renderChart($this->swfCharts,'',$log_xml,"log", 600, 700, false, false) ;
		
		$data['isi']	= "rekap/log_apl";
		$this->load->view('tampilan', $data);
	
	}
	
	function log_rinci_apl()
	{
		$data['title']  = "REKAP LOG RINCI APL/IP Segment";
		
		$ip = $this->uri->segment(3);
		
		//get data from DB
		$data['result'] = $this->model->get_log_rinci_apl($ip);
		$data['ip']		= $ip;
		$data['isi']	= "rekap/log_rinci_apl";
		$this->load->view('tampilan', $data);
	
	}
	
	function log_area()
	{
		$data['title']  = "REKAP LOG PER AREA";

		//load library fusion chart
		$this->load->library('fusioncharts') ;
        $this->swfCharts  = base_url().'public/Charts/Bar2D.swf' ;
		
		//get data from DB
		$data['result'] = $this->model->get_log_area();
		
		//grafik log
		$log_xml = $this->get_log_area_xml($data['result']);
		$data['graph_log'] = $this->fusioncharts->renderChart($this->swfCharts,'',$log_xml,"log_area", 500, 600, false, false) ;
		
		$data['isi']	= "rekap/log_area";
		$this->load->view('tampilan', $data);
	
	}
	
	function rekap_login()
	{
		if($_POST) $data['blth_current'] = $_POST['blth'];
		else $data['blth_current'] = date('mY');
		
		$data['blth']	= $this->model->get_blth_rekap_login();
		$data['result'] = $this->model->get_rekap_login($data['blth_current']);
		$data['title']	= "REKAP LOGIN PER AREA";
		$data['isi']	= "rekap/rekap_login";
		$this->load->view('tampilan2', $data);
	}
	
	function excel_rekap_login()
	{
		$this->load->plugin('to_excel');
		$blth = $this->uri->segment(3);
		to_excel($this->model->get_rekap_login($blth), 'REKAP_LOGIN');
	}
	
	function WriteToLog($page, $var)
	{
		//ASSIGN VARIABLES TO USER INFO
		if (getenv("HTTP_CLIENT_IP"))
			$ip = getenv("HTTP_CLIENT_IP");
		else if(getenv("HTTP_X_FORWARDED_FOR"))
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		else if(getenv("REMOTE_ADDR"))
			$ip = getenv("REMOTE_ADDR");
		else
			$ip = "UNKNOWN";
		
		
		//CALL OUR LOG FUNCTION
		$this->db->insert('AMR_LOGS', array(
											'IP_KOM' => $ip,
											'PAGE' => $page,
											'LOG_TEXT' => $var,
											'USN' => $this->session->userdata('usn') ));	
	}
	
}

/* End of file Rekap.php */
/* Location: ./system/application/controllers/Rekap.php */