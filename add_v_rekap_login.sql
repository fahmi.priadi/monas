drop view v_user_cnt;

drop view v_rekap_login;

create or replace view v_rekap_user_area as
select s."BLTH",s."USN",s."JML", d.kdjab, d.kdaream, d.nama_aream
  from (select to_char(tanggal, 'YYYYMM') as blth, usn, count(1) as jml
          from amr_logs a
         where page = 'LOGIN'
         group by to_char(tanggal, 'YYYYMM'), usn) s
  left join (select t.usn, t.kdjab, a.kdaream, a.nama_aream
               from amr_user t
               left join (select distinct kdaream, nama_aream from area) a
                 on t.kdarea = a.kdaream) d
    on s.usn = d.usn;

create or replace view v_rekap_area_per_jabatan as
select a.blth, a.kdaream, b.nama_aream,
nvl(sum(decode(a.kdjab,1,a.jml,0)),0) as MANAJER,
nvl(SUM(DECODE(a.kdjab,2,a.jml,0)),0) as ASMAN,
nvl(SUM(DECODE(a.kdjab,3,a.jml,0)),0) as SPV,
nvl(SUM(DECODE(a.kdjab,4,a.jml,0)),0) as COMMON_USER
 from
v_rekap_user_area a, (select distinct kdaream, nama_aream from area) b
where a.kdaream=b.kdaream
GROUP BY a.blth,a.kdaream, b.nama_aream
order by a.blth;