<? if($this->session->userdata('nama')!='') {?>
		  <li>
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <img src="<?=base_url()?>public/images/photos/loggeduser.png" alt="" />
                <?=$this->session->userdata('nama')?>
              <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                <li><?=anchor('user/user_profile','<i class="glyphicon glyphicon-user"></i> My Profile')?></li>
				<? if($this->session->userdata('level')== 1) { ?>
                <li><?=anchor('user/user_all','<i class="glyphicon glyphicon-cog"></i> Account Settings')?></li> <? } ?>
                <li><a href="<?=base_url()?>public/monas_v1.2re_user_guide.pdf" target="_blank"><i class="glyphicon glyphicon-question-sign"></i> Manual Book</a></li>
				<li><?=anchor('user/rekap_login','<i class="glyphicon glyphicon-list-alt"></i> Rekap Login')?></li>
                 <li><?=anchor('auth/logout','<i class="glyphicon glyphicon-log-out"></i> Log Out')?></li>
              </ul>
            </div>
          </li> <? }else { ?>
			<li>
            <div >
              <button type="button" class="btn btn-default dropdown-toggle" >
                <a href="<?=base_url()?>index.php/auth/login"><span class="glyphicon glyphicon-log-in"></span> <strong>LOGIN</strong></a>
               
              </button>              
            </div>
          </li> <? } ?>