
<br/><label style="font-size:14px;color:#343434;">Pelanggan yang anda maksud memiliki lebih dari 1 ID meter, pilih ID meter yang diinginkan.</label><br/><br/>
<table width="100%" class="table table-bordered mb30">
	<thead>
		<tr>
			<th><div align="center">IDPEL</div></th>
			<th><div align="center">IDMETER</div></th>
			<th><div align="center">KDJENISMETER</div></th>
			<th><div align="center">NAMA</div></th>
			<th><div align="center">TARIF</div></th>
			<th><div align="center">DAYA</div></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($result as $r) { ?>
		<tr>
			<td align="center"><a href="<?php echo site_url('pelanggan/byidpel/nopel').'/'.trim($r['NOPEL']).'/'.trim($r['IDMETER']).'/'.$r['KDJENISMETER'];?>"><?php echo $r['NOPEL'];?></a></td>
			<td align="right"><?php echo $r['IDMETER'];?></td>
			<td align="center"><?php echo $r['KDJENISMETER'];?></td>
			<td><?php echo $r['NAMA'];?></td>
			<td align="center"><?php echo $r['GOLONGAN'];?></td>
			<td align="right"><?php echo $r['DAYATPS'];?></td>
		</tr>
	<?php } ?>
	</tbody>
</table><br/><br/><br/>