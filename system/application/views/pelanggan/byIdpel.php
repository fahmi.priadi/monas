		
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				$('#submitid').submit(function(){
					var idpelg = $.trim($('#idpelg').val());
					var expresi = new RegExp(/^\d+$/);
					if(!expresi.test(idpelg)){
					//	alert('Inputan Hanya Angka');
					//	return false;
					}
				});
			});

		</script>
<script language="Javascript" src="<?=base_url()?>public/lib/Charts/FusionCharts.js"></script>

<hr>

<?php
if($resultId!=null){
	$idpel = trim($resultId->NOPEL);
	$nama = trim($resultId->NAMA);
	$nama_area = $resultId->NAMA_AREA;
	//$idmeter = $resultId->IDMETER;
	//$kdjenismeter = $resultId->KDJENISMETER;
	$alamat = $resultId->ALAMAT;
	$tarif = $resultId->GOLONGAN;
	$daya = $resultId->DAYATPS;
	$unitup = $resultId->KDUNIT;
} else { $nama=null; $nama_area=null; $idmeter=null; $kdjenismeter=null; $alamat=null; $tarif=null; $daya=null; $unitup=null; }
?>

<?php if($result_all != null) { ?>
<h3 class="subtitle mb5">Data Pelanggan</h3>
<table width="100%">
	<tr >
		
		<td width="11%"><div align="left">IDPEL</div></td>
		<td width="1%"><strong>:</strong></td>
		<td width="37%"><input  type="text" id="idpelg" style="border:none;font-size:12px;" value="<?php echo $idpel;?>" maxlength="12" >
		
		</td>
		<td width="11%"><div align="left">TARIF</div></td>
		<td width="1%"><strong>:</strong></td>
		<td width="39%"><strong><input type="text" id="tarif" style="border:none;font-size:12px;" value="<?php echo $tarif;?>" size="40" Readonly /></strong></td>
	</tr>
	<tr>
		<td><div align="left">IDMETER</div></td>
		<td><strong>:</strong></td>
		<td><strong><input type="text" id="idmeter" style="border:none;font-size:12px;" value="<?php echo $idmeter;?>" size="40" Readonly /></strong></td>
		<td><div align="left">DAYA</div></td>
		<td><strong>:</strong></td>
		<td><strong><input type="text" id="daya" style="border:none;font-size:12px;" value="<?php echo $daya;?>" size="40" Readonly /></strong></td>
	</tr>
	<tr>
		<td><div align="left">NAMA</div></td>
		<td><strong>:</strong></td>
		<td><strong><input type="text" id="nama" style="border:none;font-size:12px;" value="<?php echo $nama;?>" size="40" Readonly /></strong></td>
		<td><div align="left">KDJENISMETER</div></td>
		<td><strong>:</strong></td>
		<td><strong><input type="text" id="kdjenismeter" style="border:none;font-size:12px;" value="<?php echo $kdjenismeter;?>" size="40" Readonly /></strong></td>
	</tr>
	<tr>
		<td><div align="left">ALAMAT</div></td>
		<td><strong>:</strong></td>
		<td><strong><input type="text" id="alamat" style="border:none;font-size:12px;" value="<?php echo $alamat;?>" size="44" Readonly /></strong></td>
		<td><div align="left">RAYON</div></td>
		<td><strong>:</strong></td>
		<td><strong><input type="text" id="unitup" style="border:none;font-size:12px;" value="<?php echo strtoupper($nama_area);?>" size="40" Readonly /></strong></td>
	</tr>
</table>


<p>&nbsp;</p>
<?php if($catatan) {?>
<h3 class="subtitle mb5">Catatan Hasil Pemeriksaan</h3>
<table class="table table-bordered mb30">
 <thead>
  <tr>
    <th width="25">NO</th>
    <th width="150">TANGGAL</th>
    <th>CATATAN</th>
  </tr>
  </thead>
  <tbody>
  <?php
	$no = 1;
	foreach($catatan as $r){
		?>
		  <tr>
		    <td><?php echo $no;?></td>
		    <td align="center"><?php echo $r['TGL'];?></td>
		    <td><?php echo $r['CATATAN'];?></td>
		  </tr>
		<?php
		$no++;
	}
  ?>
  </tbody>
</table>

<?php } ?>

<h3 class="subtitle mb5">Load Profile Pelanggan&nbsp;<? echo anchor('pelanggan/excel_byIdpel/'.$idpel.'/'.$idmeter.'/'.$kdjenismeter,'<i class="fa fa-download"></i>&nbsp;Excel')?></h3>

<?php if(($result_all == null) && ($idpel != null)) { echo '<div style="clear:both;font-weight: bold;"><span style="color:#fe3401;">MAAF, DATA LOADPROFILE PELANGGAN TIDAK DITEMUKAN</span></div>'; } ?>

<div class="table-responsive">
          <table class="table" id="table2">
              <thead>
                 <tr>
                    <th>IDMETER</th>
                    <th>TGLJAM</th>
                    <th>VR</th>
                    <th>VS</th>
                    <th>VT</th>
                    <th>IR</th>
                    <th>IS</th>
                    <th>IT</th>
                 </tr>
              </thead>
              <tbody>
                 <? if($result_all != null) {
					foreach($result_all as $r){
					?>
						<tr>
							<td><?php echo $r['IDMETER']?></td>
							<td><?php echo $r['TGLJAM']?></td>
							<td><?php echo $r['VA']?></td>
							<td><?php echo $r['VB']?></td>
							<td><?php echo $r['VC']?></td>
							<td><?php echo $r['IA']?></td>
							<td><?php echo $r['IB']?></td>
							<td><?php echo $r['IC']?></td>
						</tr>
					<?php
					}
					}
				  ?>
              </tbody>
           </table>
           <p>&nbsp;</p>
          </div>
<p>&nbsp;</p>
<p><br/>
	<?php
		echo $graph_hist_voltase;
	?>
</p>
<p>
	<?php
		echo $graph_hist_ampere;
	?>
</p>
<? } ?>
