<?php
class User_model extends Model
{
	function User_model()
	{
		parent::Model();
	}
	
	function get_user_profile($usn)
	{
		$sql = "SELECT * FROM AMR_USER WHERE USN ='$usn'";
		
		$query	= $this->db->query($sql);
		return $query->row();
	}
	
	function get_user_all()
	{
		$sql = "SELECT T.USN, T.NAMA, T.EMAIL, T.KDJAB, T.KDAREA, A.NAMA_AREAM AS NAMA_AREA 
				FROM AMR_USER T, (SELECT DISTINCT KDAREAM, NAMA_AREAM FROM AREA) A
				WHERE trim(T.KDAREA)=trim(A.KDAREAM)
				ORDER BY T.NAMA ASC";
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_data_area()
	{
		$sql = "SELECT DISTINCT KDAREAM AS KDAREA,NAMA_AREAM AS NAMA_AREA 
				FROM AREA 
				ORDER BY KDAREAM";
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function cek_user($usn)
	{	
		$sql = "SELECT COUNT(1) AS JML FROM AMR_USER WHERE UPPER(USN) = '".strtoupper($usn)."'";
		
		$query	= $this->db->query($sql);
		if($query->row()->JML > 0)
			return false;
		else return true;
	}
	
	function insert_user()
	{
		$sql = "INSERT INTO AMR_USER (USN, PSWD, NAMA, EMAIL, KDAREA, KDJAB, LEVEL_USER, ID_USER_CREATE)
				VALUES ('".strtoupper($this->input->post('usn'))."',
						'".md5($this->input->post('pass1'))."',
						'".strtoupper($this->input->post('nama'))."',
						'".strtoupper($this->input->post('email'))."',
						'".$this->input->post('kdarea')."',
						".$this->input->post('kdjab').",
						".$this->input->post('level_user').",
						'".$this->session->userdata('usn')."')";
		//echo $sql;
		if($this->db->query($sql))
			return true;
		else return false;
	}
	
	function update_user()
	{
		$pass = "";
		if($this->input->post('pass1')!="")
			$pass = "PSWD = '".md5($this->input->post('pass1'))."',";
		$sql = "UPDATE AMR_USER SET
					NAMA = '".$this->input->post('nama')."',$pass
					EMAIL = '".$this->input->post('email')."',
					KDAREA = '".$this->input->post('kdarea')."',
					KDJAB = ".$this->input->post('kdjab').",
					LEVEL_USER = ".$this->input->post('level_user').",
					ID_USER_CREATE = '".$this->session->userdata('usn')."'
				WHERE USN = '".$this->input->post('usn')."'";
		//echo $sql;
		if($this->db->query($sql))
			return true;
		else return false;
	}
	
	function delete_user($usn)
	{
		$sql = "DELETE AMR_USER WHERE USN = '$usn'";
		
		if($this->db->query($sql))
			return true;
		else return false;
	}
	
	function rekap_login($blth)
	{
		$sql = "SELECT * FROM V_REKAP_AREA_PER_JABATAN WHERE BLTH = '$blth'";
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_blth_login()
	{
		$sql = "SELECT DISTINCT BLTH FROM V_REKAP_AREA_PER_JABATAN 
				ORDER BY BLTH DESC";
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function detil_rekap($kdjab, $blth)
	{
		$sql = "SELECT T.TANGGAL, T.IP_KOM, T.USN
				  FROM AMR_LOGS T, AMR_USER U
				 WHERE T.PAGE = 'LOGIN'
				   AND TO_CHAR(TANGGAL, 'YYYYMM') = '$blth'
				   AND T.USN = U.USN
				   AND U.KDJAB = $kdjab
				 ORDER BY TANGGAL";
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	
	}
	
}

?>