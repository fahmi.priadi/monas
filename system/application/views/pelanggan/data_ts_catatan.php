<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" type="image/ico" href="http://10.3.0.10/amr_view/public/login_files/icon.png" />
<title>Data Tagihan Susulan</title>
<style type="text/css">
#printarea{padding:10px 0;width:1000px; margin:auto;}
table.head td { padding:0 3px;}
textarea {font-family: Verdana, Calibri;}
hr.style_ts { margin:20px 10px; height:1px; background-color:#dedede; border:none; }
.printhead{text-align:center; font-size:20px; font-family:Calibri; font-weight:bold; border-bottom:1px solid #121212; margin:0 0 10px 0;}

table.topkonten td { padding:2px 0 }
table.konten td { font-family: Verdana, Calibri; padding:3px 0 }
@media print{
	#non-printable { display: none; }
	#printarea { display: block; }
}	
</style>
<script>
function printpage(){
	window.print()
}
</script>
</head>
<body>

<div id="printarea">
<div class="printhead">
	<table cellpadding="0" cellspacing="0" border="0" class="head" style="padding:0; font-family:Calibri; font-size:16px">
		<tr >
			<td width="10%" rowspan="2"><img src="<?=base_url()?>public/images/logo_pln.gif" width="36" height="48" /></td>
			<td width="90%"><div align="left">PT PLN (Persero)</div></td>
		</tr>
		<tr>
			<td style="vertical-align:top"><div align="left"><?=$this->config->item('nama_dist')?></div></td>
		</tr>
	</table>
	<br />
  <?php echo $title;?>
</div>

<?php
	foreach($result as $r){
		$idpel = $r['IDPEL'];
		$nama = strtoupper($r['NAMA']);
		$alamat = $r['ALAMAT'];
		$tarif = $r['TARIF'];
		$daya = $r['DAYA'];
		$kategori = $r['KATEGORI'];
		$jenis = $r['JENIS'];
		$deskripsi = $r['DESKRIPSI'];
		$ts_daya = $r['TS_DAYA'];
		$ts_kwh = $r['TS_KWH'];
		$ts_rupiah = $r['TS_RUPIAH'];
		$sph_tanggal = $r['SPH_TANGGAL'];
		$sph_nomor = $r['SPH_NOMOR'];
		$sph_rupiah = $r['SPH_RUPIAH'];
		$tgl = $r['TGL'];
		$catatan = $r['CATATAN'];
	}
?>

<table cellpadding="0" cellspacing="0" class="konten" style="font-size:12px; margin:0 10px;">
<tr>
	<td>IDPEL</td><td>: <?php echo $idpel;?></td>
	<td width="60%"></td>
	<td>TARIF</td><td>: <?php echo $tarif;?></td>
</tr>
<tr>
	<td>NAMA</td><td>: <?php echo $nama;?></td>
	<td></td>
	<td>DAYA</td><td>: <?php echo $daya;?> VA</td>
</tr>
<tr>
	<td>ALAMAT</td><td colspan="4">: <?php echo $alamat;?></td>
</tr>
</table>
</table>

<hr class="style_ts" />

<table cellpadding="0" cellspacing="0" class="konten" style="font-size:12px; margin:0 10px; width:100%;">
<tr>
	<td width="150"><b>Kategori Temuan</b></td>
	<td>:&nbsp;<i><?php echo $kategori;?></i></td>
</tr>
<tr>
	<td><b>Jenis</b></td>
	<td>:&nbsp;<i><?php echo $jenis;?></i></td>
</tr>
<tr>
	<td colspan="2"><b>Deskripsi Kelainan / Pelanggaran :</b></td>
</tr>
<tr>
	<td colspan="2" style="padding:0 10px;"><i><?php echo $deskripsi;?></i><br/><br/></td>
</tr>
<tr>
	<td colspan="2"><b>Data Tagihan Susulan</b></td>
</tr>
<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;Jumlah kWh</td>
	<td>:&nbsp;<i><?php echo $ts_kwh;?></i></td>
</tr>
<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Daya</td>
	<td>:&nbsp;<i><?php echo $ts_daya;?></i></td>
</tr>
<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Rupiah</td>
	<td>:&nbsp;<i><?php echo number_format($ts_rupiah,0,',','.');?></i></td>
</tr>
<tr>
	<td colspan="2"><b>Data Surat Pengakuan Hutang (SPH)</b></td>
</tr>
<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;Tanggal</td>
	<td>:&nbsp;<i><?php echo $tgl;?></i></td>
</tr>
<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;Nomor SPH</td>
	<td>:&nbsp;<i><?php echo $sph_nomor;?></i></td>
</tr>
<tr>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;Rupiah SPH</td>
	<td>:&nbsp;<i><?php echo number_format($sph_rupiah,0,',','.');?></i></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><b>Catatan Hasil Pemeriksaan</b></td>
</tr>
<tr>
	<td colspan="2" style="padding:0 10px;"><i><?php echo $catatan;?></i><br/><br/></td>
</tr>
<tr>
	<td colspan="2"></td>
</tr>

</table>


</div>

<div id="non-printable">
<br>
<center><input type="button" value="Print Laporan" onclick="printpage();"></center>
</div>