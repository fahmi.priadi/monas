<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themepixels.com/demo/webpage/bracket/general-forms.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 06:34:46 GMT -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?=base_url()?>public/images/favicon.png" type="image/png">

   <title>Monas - Monitoring dan Analisa Sistem AMR</title>

  <link rel="stylesheet" href="<?=base_url()?>public/css/style.default.css" />
  <link rel="stylesheet" href="<?=base_url()?>public/css/bootstrap-fileupload.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>public/css/bootstrap-timepicker.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>public/css/jquery.tagsinput.css" />
  <link rel="stylesheet" href="<?=base_url()?>public/css/colorpicker.css" />
  <link rel="stylesheet" href="<?=base_url()?>public/css/dropzone.css" />
  

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
  
</head>

<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <? $this->load->view('left_menu');?>
  
  <div class="mainpanel">
    
    <div class="headerbar">
      
      <a class="menutoggle"><i class="fa fa-bars"></i></a>
      
      <div class="header-right">
        <ul class="headermenu">
          <? $this->load->view('user_menu'); ?>
          </li>
        </ul>
      </div><!-- header-right -->
      
    </div><!-- headerbar -->
    
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <?=$title?> </h2>      
    </div>
     <? $this->load->view($isi);?>
      
   
  </div><!-- mainpanel -->
  <!-- rightpanel -->
</section>



<script src="<?=base_url()?>public/js/jquery-1.10.2.min.js"></script>
<script src="<?=base_url()?>public/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=base_url()?>public/js/jquery-ui-1.10.3.min.js"></script>
<script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>public/js/modernizr.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.sparkline.min.js"></script>
<script src="<?=base_url()?>public/js/toggles.min.js"></script>
<script src="<?=base_url()?>public/js/retina.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.cookies.js"></script>

<script src="<?=base_url()?>public/js/jquery.autogrow-textarea.js"></script>
<script src="<?=base_url()?>public/js/bootstrap-fileupload.min.js"></script>
<script src="<?=base_url()?>public/js/bootstrap-timepicker.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.maskedinput.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.tagsinput.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>public/js/chosen.jquery.min.js"></script>
<script src="<?=base_url()?>public/js/dropzone.min.js"></script>
<script src="<?=base_url()?>public/js/colorpicker.js"></script>

<script src="<?=base_url()?>public/js/custom.js"></script>
<script src="<?=base_url()?>public/lib/blockUI/jquery.blockUI.js"></script>
<script>
$(document).ready(function() {
    $('#blok').click(function() {
        $.blockUI({ message: 'Processing..' });
    });
});
</script>
<script>
jQuery(document).ready(function(){
    
  // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
  
  // Tags Input
  jQuery('#tags').tagsInput({width:'auto'});
   
  // Textarea Autogrow
  jQuery('#autoResizeTA').autogrow();
  
  // Color Picker
  if(jQuery('#colorpicker').length > 0) {
	 jQuery('#colorSelector').ColorPicker({
			onShow: function (colpkr) {
				jQuery(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				jQuery(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				jQuery('#colorSelector span').css('backgroundColor', '#' + hex);
				jQuery('#colorpicker').val('#'+hex);
			}
	 });
  }
  
  // Color Picker Flat Mode
	jQuery('#colorpickerholder').ColorPicker({
		flat: true,
		onChange: function (hsb, hex, rgb) {
			jQuery('#colorpicker3').val('#'+hex);
		}
	});
   
  // Date Picker
  jQuery('#datepicker').datepicker();
  jQuery('#datepicker2').datepicker();
	jQuery('#datepicker3').datepicker({
			dateFormat: 'dd-mm-yy',
			maxDate: "+0D"
		});
  
  jQuery('#datepicker-inline').datepicker();
  
  jQuery('#datepicker-multiple').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });
  
  // Spinner
  var spinner = jQuery('#spinner').spinner();
  spinner.spinner('value', 0);
  
  // Input Masks
  jQuery("#date").mask("99/99/9999");
  jQuery("#phone").mask("(999) 999-9999");
  jQuery("#ssn").mask("999-99-9999");
  
  // Time Picker
  jQuery('#timepicker').timepicker({defaultTIme: false});
  jQuery('#timepicker2').timepicker({showMeridian: false});
  jQuery('#timepicker3').timepicker({minuteStep: 15});

  
});
</script>


</body>

<!-- Mirrored from themepixels.com/demo/webpage/bracket/general-forms.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 06:35:03 GMT -->
</html>
