<div class="contentpanel">  
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title"><?=$subtitle?></h4>
          <p>Fitur analisa arus digunakan untuk mencari pelanggan yang arusnya tidak sesuai dengan standar TMP. Fitur ini akan mencari data load profile pelanggan yang salah satu/dua arus phasa-nya kurang dari Batas Arus Minimum dan dua/satu arus phasa lainnya lebih dari Batas Arus Normal dalam rentang waktu sesuai periode yang dipilih.</p>
        </div>
        <div class="panel-body panel-body-nopadding">
          
            <?=form_open('pelanggan/tegangan_tr', array('class'=>'form-horizontal form-bordered'))?>
            <div class="form-group">
              <label class="col-sm-3 control-label">UNITUP</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="unitup" >
                  <? foreach($area as $data) { ?>
                  <option value="<?=$data['KDAREA']?>"><?=$data['NAMA_AREA']?></option>
                   <? } //end foreach ?>
                </select>
              </div>
            </div>
            
            <div class="form-group">
				  <label class="col-sm-3 control-label" for="disabledinput">Periode</label>
				  <div class="col-sm-6">
					 <div class="input-group">
                <input type="text" name="tgl1" class="form-control" id="datepicker"  value="<?=$tgl_01?>">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div>
				  </div>
				</div>
            
            <div class="form-group">
				  <label class="col-sm-3 control-label" for="readonlyinput">s.d</label>
				  <div class="col-sm-6">
					 <div class="input-group">
                <input type="text" name="tgl2" class="form-control" placeholder="mm/dd/yyyy" id="datepicker2" value="<?=$tgl_02?>">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div>
				  </div>
				</div>
            
            <div class="form-group">
              <label class="col-sm-3 control-label">Batas Tegangan Minimum (V)</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="teg_min" >
                  <option value="190" selected="selected">190</option>
                  <option value="180">180</option>
                  <option value="170">170</option>
                  <option value="160">160</option>
                  <option value="150">150</option>
                  <option value="52">52</option>
                  <option value="50">50</option>
                  <option value="48">48</option>
                  <option value="45">45</option>
                  <option value="40">40</option>
                  <option value="20">30</option>
                  <option value="10">10</option>
                </select>
                <span class="help-block">Batas bawah tegangan dalam loadprofile yang dicari</span>
              </div>
            </div>
            
           <div class="form-group">
              <label class="col-sm-3 control-label">Batas Tegangan Normal (V)</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="teg_normal" >
                  <option value="210">210</option>
                  <option value="205">205</option> 
                  <option value="200">200</option>
                  <option value="195" selected="selected">195</option>
                  <option value="190">190</option>
                  <option value="52">52</option>
                  <option value="55">55</option>
                  <option value="57">57</option>
                  <option value="60">60</option>
                  <option value="62">62</option>
               </select>
                <span class="help-block">Batas atas tegangan dalam loadprofile yang dicari</span>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-3 control-label">Minimum Sejumlah</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="jumlah">
                  <option value="100">100 x</option>
				  <option value="300">300 x</option>
                  <option value="500" selected="selected">500 x</option>
                  <option value="1000">1000 x</option>
                  <option value="2000">2000 x</option>
                </select>
                <span class="help-block">Jumlah kejadian minimum</span>
              </div>
            </div>
            
           <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-6 col-sm-offset-3">
				  <button id="blok" class="btn btn-primary" type="submit">Submit</button>&nbsp;
				</div>
			 </div>
		  </div><!-- panel-footer -->
            
          </form>
          
        </div><!-- panel-body -->
        
      </div>
      </div>
</div>