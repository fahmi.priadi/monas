<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themepixels.com/demo/webpage/bracket/signin.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 06:36:30 GMT -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?=base_url()?>public/images/favicon.png" type="image/png">

  <title>Monas - Monitoring dan Analisa Sistem AMR</title>

  <link href="<?=base_url()?>public/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="signin">

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
    <div class="signinpanel">
        
        <div class="row">
            
            <div class="col-md-7">
                
                <div class="signin-info">
                    <div class="logopanel">
                        <h1><span>[</span> <?=$this->config->item('version')?> <span>]</span></h1>
                    </div><!-- logopanel -->
                
                    <div class="mb20"></div>
                
                    <h5><strong>Monitoring dan Analisa Sistem AMR</strong></h5>
                    <ul>
                        
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> Monitoring Keberhasilan Download Load Profile</li>
						<li><i class="fa fa-arrow-circle-o-right mr5"></i> Mencari Target Operasi P2TL AMR</li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> Catatan Hasil Pemeriksaan Lapangan</li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> Catatan Tagihan Susulan Pelanggan</li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> and many more...</li>
                    </ul>
                    <div class="mb20"></div>
                    <strong>&nbsp;</strong>
                </div><!-- signin0-info -->
            
            </div><!-- col-sm-7 -->
            
            <div class="col-md-5">
                
                <?=form_open('auth/set_sess', array('name'=>'form1'))?>
                    <h4 class="nomargin">Sign In</h4>
                    <p class="mt5 mb20">Login to access your account.</p>
                
                    <input type="text" name="var_username" class="form-control uname" placeholder="Username" />
                    <input type="password" name="var_password" class="form-control pword" placeholder="Password" />
                    <div style="color:#FF0000"><small><?=$err?></small></div>
                    <button class="btn btn-success btn-block">Sign In</button>
                    
                </form>
            </div><!-- col-sm-5 -->
            
        </div><!-- row -->
        
        <div class="signup-footer">
            <div class="pull-left">
                PT PLN (Persero) Distribusi Jakarta Raya & Tangerang &copy; 2014. All Rights Reserved. 
            </div>
            <div class="pull-right">
                Created By: <a href="http://www.pln.co.id/disjaya" target="_blank">Disjaya</a>
            </div>
        </div>
        
    </div><!-- signin -->
  
</section>


<script src="<?=base_url()?>public/js/jquery-1.10.2.min.js"></script>
<script src="<?=base_url()?>public/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>public/js/modernizr.min.js"></script>
<script src="<?=base_url()?>public/js/retina.min.js"></script>

<script src="<?=base_url()?>public/js/custom.js"></script>

</body>

<!-- Mirrored from themepixels.com/demo/webpage/bracket/signin.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 06:36:30 GMT -->
</html>
