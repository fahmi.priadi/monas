<?php
	
class Pelanggan extends Controller
{
	function Pelanggan()
	{
		parent::Controller();
		$this->load->model('pelanggan_model','model');
		$this->load->library('session');
	}
	
	function index()
	{
		redirect('pelanggan/arus');
	}
	
	function general()
	{
		$this->load->view('general-forms');
	}
	
	function tables()
	{
		$this->load->view('tables');
	}
	
	function arus_form()
	{
		$data['title']  = "Pengukuran TM - Arus";
		$data['subtitle']  = "Pengukuran TM - Arus";
		$data['isi']	= "pelanggan/arus_form";
		$data['tanda']  = false;
		$data['tgl_01'] = $this->firstday_2lastmonth();
		$data['tgl_02']	= $this->lastday_lastmonth();
		$data['area'] = $this->model->get_data_area();
		$this->load->view('forms', $data);
	}
	
	function arus()
	{		
		$data['title']  = "Pengukuran TM - Arus";
		$data['subtitle']  = "";
		$data['isi']	= "pelanggan/arus";
		
		if($_POST)
		{
			$data['tanda'] = true;
			
			$unitup		= $_POST['unitup'];
			$tgl_awal	= $this->extract_tgl($_POST['tgl1']);
			$tgl_akhir	= $this->extract_tgl($_POST['tgl2']);
			$arus_min	= $_POST['arus_min'];
			$arus_normal	= $_POST['arus_normal'];
			$jumlah		= $_POST['jumlah'];
			
			$list_kdarea = $this->model->get_data_area();
			$data['result'] 	= $this->model->get_data_arus_amr($tgl_awal, $tgl_akhir, $jumlah, $arus_min, $arus_normal, $unitup, $list_kdarea);	
			$data['tgl_awal']  	= $tgl_awal;
			$data['tgl_akhir'] 	= $tgl_akhir;
			$data['current_area'] = $this->input->post('unitup');
			$data['tgl_01'] 	= $_POST['tgl1'];
			$data['tgl_02']		= $_POST['tgl2'];
			$data['tgl1'] 	   	= $this->extract_tgl_display($_POST['tgl1']);
			$data['tgl2'] 	   	= $this->extract_tgl_display($_POST['tgl2']);
			$data['arus_min']   = $arus_min;
			$data['arus_normal']   = $arus_normal;
			$data['jumlah']    	= $jumlah;
			$data['nama_aream'] = $this->model->get_nama_aream($unitup);
			//WRITE TO LOG
			$this->WriteToLog("ARUS", $unitup.$tgl_awal.$tgl_akhir.$arus_min.$arus_normal.$jumlah);
			
		}
		else $this->WriteToLog("ARUS", "OPEN HALAMAN ARUS");
		
		$this->load->view('template_tables', $data);
		
	}
	
	
	function arus_2_form()
	{
		$data['title']  = "Pengukuran Langsung - Arus";
		$data['subtitle']  = "Pengukuran Langsung - Arus";
		$data['isi']	= "pelanggan/arus_2_form";
		$data['tanda']  = false;
		$data['tgl_01'] = $this->firstday_2lastmonth();
		$data['tgl_02']	= $this->lastday_lastmonth();
		$data['area'] = $this->model->get_data_area();
		$this->load->view('forms', $data);
	}
	
	function arus_2()
	{
		$data['title']  = "Pengukuran Langsung - Arus";
		$data['subtitle']  = "";
		$data['isi']	= "pelanggan/arus_2";	
		
		if($_POST)
		{
			$data['tanda'] = true;
			
			$unitup		= $_POST['unitup'];
			$tgl_awal	= $this->extract_tgl($_POST['tgl1']);
			$tgl_akhir	= $this->extract_tgl($_POST['tgl2']);
			$arus_min	= $_POST['arus_min'];
			$arus_normal	= $_POST['arus_normal'];
			$jumlah		= $_POST['jumlah'];
			
			
			$list_kdarea = $this->model->get_data_area();
			$data['result'] 	= $this->model->get_data_arus_amr($tgl_awal, $tgl_akhir, $jumlah, $arus_min, $arus_normal, $unitup, $list_kdarea);	
			$data['tgl_awal']  	= $tgl_awal;
			$data['tgl_akhir'] 	= $tgl_akhir;
			$data['current_area'] = $this->input->post('unitup');
			$data['tgl_01'] 	= $_POST['tgl1'];
			$data['tgl_02']		= $_POST['tgl2'];
			$data['tgl1'] 	   	= $this->extract_tgl_display($_POST['tgl1']);
			$data['tgl2'] 	   	= $this->extract_tgl_display($_POST['tgl2']);
			$data['arus_min']   = $arus_min;
			$data['arus_normal']   = $arus_normal;
			$data['jumlah']    	= $jumlah;
			$data['nama_aream'] = $this->model->get_nama_aream($unitup);
			//WRITE TO LOG
			$this->WriteToLog("ARUS", $unitup.$tgl_awal.$tgl_akhir.$arus_min.$arus_normal.$jumlah);
			
		}
		else $this->WriteToLog("ARUS", "OPEN HALAMAN ARUS");
		
		$this->load->view('template', $data);
		
	}
	
	function arus_idpel()
	{
		$data['title']  = "Detail Pelanggan AMR";
		$data['subtitle']  = "Data Pelanggan";
		$data['isi']	= "pelanggan/detil_pelanggan_arus";
		
		$data['tanda'] = true;
		
		//load library fusion chart
		$this->load->library('fusioncharts') ;
	    $this->swfCharts  = base_url().'public/lib/Charts/ZoomLine.swf' ;
		
		$data['idpel']		= $this->uri->segment(3);
		$data['tgl_awal']	= $this->uri->segment(4);
		$data['tgl_akhir']	= $this->uri->segment(5);
		$data['arus_min']	= $this->uri->segment(6);
		$data['arus_normal']= $this->uri->segment(7);
		$data['jenis_meter']= $this->uri->segment(8);
		$data['unitup']		= $this->uri->segment(9);
		$data['idmeter']	= $this->uri->segment(10);
		$data['kdunit']		= $this->uri->segment(11);
			
		$data['result'] = $this->model->get_data_arus_amr_idpel($data['idpel'], $data['tgl_awal'], $data['tgl_akhir'], $data['arus_min'], $data['arus_normal'], $data['unitup'],$data['idmeter'],$data['jenis_meter']);
	
		$data['result_all'] = $this->model->get_data_plg_by_idpel($data['idpel'], $data['unitup'], $data['idmeter'], $data['jenis_meter']);
		
		$data['plg']	= $this->model->get_data_plg_idpel($data['idpel'], $data['jenis_meter'], $data['kdunit']);
		
		//grafik TEGANGAN
	/*	$v_xml = $this->voltase_xml($data['result_all']);	*/
		$v_xml = $this->voltase_xml($data['result']);
		$data['graph_hist_voltase'] = $this->fusioncharts->renderChart($this->swfCharts,'',$v_xml,"hist_volt", 700, 500, false, true) ;
		
		//grafik ARUS
	/*	$i_xml = $this->ampere_xml($data['result_all']);	*/
		$i_xml = $this->ampere_xml($data['result']);
		$data['graph_hist_ampere'] = $this->fusioncharts->renderChart($this->swfCharts,'',$i_xml,"hist_amp", 700, 500, false, true) ;
		
		//grafik TEGANGAN ALL
		$v_xml_all = $this->voltase_xml($data['result_all']);
		$data['graph_hist_voltase_all'] = $this->fusioncharts->renderChart($this->swfCharts,'',$v_xml_all,"hist_volt2", 700, 500, false, true) ;
		
		//grafik ARUS ALL
		$i_xml_all = $this->ampere_xml($data['result_all']);
		$data['graph_hist_ampere_all'] = $this->fusioncharts->renderChart($this->swfCharts,'',$i_xml_all,"hist_amp2", 700, 500, false, true) ;
		
		$this->load->view('template', $data);
	}
		
	function tegangan_form()
	{
		$data['title']  = "Pengukuran TM - Tegangan";
		$data['subtitle']  = "Pengukuran TM - Tegangan";
		$data['isi']	= "pelanggan/tegangan_form";
		$data['tanda']  = false;
		$data['tgl_01'] = $this->firstday_2lastmonth();
		$data['tgl_02']	= $this->lastday_lastmonth();
		$data['area'] = $this->model->get_data_area();
		$this->load->view('forms', $data);
	}
	
	function tegangan()
	{
		$data['title']  = "Pengukuran TM - Tegangan";
		$data['subtitle']  = "";
		$data['isi']	= "pelanggan/tegangan";
		
		if($_POST)
		{
			$data['tanda'] = true;
			
			$unitup		= $_POST['unitup'];
			$tgl_awal	= $this->extract_tgl($_POST['tgl1']);
			$tgl_akhir	= $this->extract_tgl($_POST['tgl2']);
			$teg_min	= $_POST['teg_min'];
			$teg_normal	= $_POST['teg_normal'];
			$jumlah		= $_POST['jumlah'];
			
			$list_kdarea = $this->model->get_data_area();
			$data['result'] 	= $this->model->get_data_tegangan_amr_tm($tgl_awal, $tgl_akhir, $jumlah, $teg_min, $teg_normal, $unitup, $list_kdarea);
			$data['current_area'] = $this->input->post('unitup');
			$data['tgl_awal']  	= $tgl_awal;
			$data['tgl_akhir'] 	= $tgl_akhir;
			$data['tgl1'] 	   	= $this->extract_tgl_display($_POST['tgl1']);
			$data['tgl2'] 	   	= $this->extract_tgl_display($_POST['tgl2']);
			$data['tgl_01'] 	= $_POST['tgl1'];
			$data['tgl_02']		= $_POST['tgl2'];
			$data['teg_min']   	= $teg_min;
			$data['teg_normal'] = $teg_normal;
			$data['jumlah']    	= $jumlah;
			$data['nama_aream'] = $this->model->get_nama_aream($unitup);
		}
		
		
		$this->load->view('template', $data);
	}
	
	function tegangan_tr_form()
	{
		$data['title']  = "Pengukuran TR - Tegangan";
		$data['subtitle']  = "Pengukuran TR - Tegangan";
		$data['isi']	= "pelanggan/tegangan_tr_form";
		$data['tanda']  = false;
		$data['tgl_01'] = $this->firstday_2lastmonth();
		$data['tgl_02']	= $this->lastday_lastmonth();
		$data['area'] = $this->model->get_data_area();
		$this->load->view('forms', $data);
	}
	
	function tegangan_tr()
	{
		$data['title']  = "Pengukuran TR - Tegangan";
		$data['subtitle']  = "";
		$data['isi']	= "pelanggan/tegangan_tr";
		
		if($_POST)
		{
			$data['tanda'] = true;
			
			$unitup		= $_POST['unitup'];
			$tgl_awal	= $this->extract_tgl($_POST['tgl1']);
			$tgl_akhir	= $this->extract_tgl($_POST['tgl2']);
			$teg_min	= $_POST['teg_min'];
			$teg_normal	= $_POST['teg_normal'];
			$jumlah		= $_POST['jumlah'];
			
			$list_kdarea = $this->model->get_data_area();
			$data['result'] 	= $this->model->get_data_tegangan_amr($tgl_awal, $tgl_akhir, $jumlah, $teg_min, $teg_normal, $unitup, $list_kdarea);
			$data['current_area'] = $this->input->post('unitup');
			$data['tgl_awal']  	= $tgl_awal;
			$data['tgl_akhir'] 	= $tgl_akhir;
			$data['tgl1'] 	   	= $this->extract_tgl_display($_POST['tgl1']);
			$data['tgl2'] 	   	= $this->extract_tgl_display($_POST['tgl2']);
			$data['tgl_01'] 	= $_POST['tgl1'];
			$data['tgl_02']		= $_POST['tgl2'];
			$data['teg_min']   	= $teg_min;
			$data['teg_normal'] = $teg_normal;
			$data['jumlah']    	= $jumlah;
			$data['nama_aream'] = $this->model->get_nama_aream($unitup);
		}
		
		
		$this->load->view('template', $data);
	}
	
	function tegangan_idpel()
	{
		$data['title']  = "Detail Pelanggan AMR";
		$data['subtitle']  = "Data Pelanggan";
		$data['isi']	= "pelanggan/detil_pelanggan_tegangan";
		
		$data['tanda'] = true;
		
		//load library fusion chart
		$this->load->library('fusioncharts') ;
        $this->swfCharts  = base_url().'public/lib/Charts/ZoomLine.swf' ;
		
		$data['idpel']		= $this->uri->segment(3);
		$data['tgl_awal']	= $this->uri->segment(4);
		$data['tgl_akhir']	= $this->uri->segment(5);
		$data['teg_min']	= $this->uri->segment(6);
		$data['teg_normal'] = $this->uri->segment(7);
		$data['jenis_meter']= trim($this->uri->segment(8));
		$data['unitup']		= $this->uri->segment(9);
		$data['idmeter']	= $this->uri->segment(10);
		$data['kdunit']	= $this->uri->segment(11);
		
		$data['plg']	= $this->model->get_data_plg_idpel($data['idpel'], $data['jenis_meter'], $data['kdunit']);
		
		$data['result'] = $this->model->get_data_tegangan_amr_idpel($data['idpel'], $data['tgl_awal'], $data['tgl_akhir'], $data['teg_min'], $data['teg_normal'], $data['unitup'],$data['idmeter'],$data['jenis_meter']);
	
		$data['result_all'] = $this->model->get_data_plg_by_idpel($data['idpel'], $data['unitup'], $data['idmeter'], $data['jenis_meter']);
		
		
		//grafik TEGANGAN
		$v_xml = $this->voltase_xml($data['result']);
		$data['graph_hist_voltase'] = $this->fusioncharts->renderChart($this->swfCharts,'',$v_xml,"hist_volt", 700, 500, false, true) ;
		
		//grafik ARUS
		$i_xml = $this->ampere_xml($data['result']);
		$data['graph_hist_ampere'] = $this->fusioncharts->renderChart($this->swfCharts,'',$i_xml,"hist_amp", 700, 500, false, true) ;
		
		//grafik TEGANGAN ALL
		$v_xml_all = $this->voltase_xml($data['result_all']);
		$data['graph_hist_voltase_all'] = $this->fusioncharts->renderChart($this->swfCharts,'',$v_xml_all,"hist_volt2", 700, 500, false, true) ;
		
		//grafik ARUS ALL
		$i_xml_all = $this->ampere_xml($data['result_all']);
		$data['graph_hist_ampere_all'] = $this->fusioncharts->renderChart($this->swfCharts,'',$i_xml_all,"hist_amp2", 700, 500, false, true) ;
		
		$this->load->view('template', $data);
	}
	
	function pelanggan_tt(){
		$data['title']	= "Data Pelanggan";
		$data['subtitle']	= "Pelanggan Tegangan Tinggi";
		$data['isi']	= "pelanggan/pelanggan_tt";
		$data['tanda']	= false;
		$data['result'] = $this->model->get_pelanggan_tt();
		$this->load->view('template', $data);
	}
	
	function kwh_pm_form()
	{
		$data['title']  = "KWH Plus - Minus";
		$data['subtitle']  = "KWH Plus - Minus";
		$data['isi']	= "pelanggan/kwh_pm_form";
		$data['area'] = $this->model->get_data_area();
		$data['tahun'] = $this->model->get_periode_kwh_meter();
		$this->load->view('forms', $data);
	}
	
	function kwh_pm(){
		$data['title']  = "KWH Plus - Minus";
		$data['subtitle']  = "";
		$data['isi']	= "pelanggan/kwh_pm";
		
		if($_POST)
		{
			$data['tanda'] = true;
			$data['periode'] = $this->input->post('blth');
			$data['persen'] = $this->input->post('persen');
			$data['current_area'] = $this->input->post('unitup');
			$data['result']	= $this->model->get_data_kwh_meter($this->input->post('blth'),$this->input->post('persen'),'11',$this->input->post('unitup'));
			
		}
		$this->load->view('template', $data);
	}
	
	//function to build XML for graph	
	function voltase_xml($row_query)
	{
		
		$cat = "";
		$va = "";
		$vb = "";
		$vc = "";
		
		foreach($row_query as $row_amr)
		{
			$cat .= "<category label='".$row_amr['TGLJAM']."'/>";
			$va .= "<set value='".$row_amr['VA']."'/>";
			$vb .= "<set value='".$row_amr['VB']."'/>";
			$vc .= "<set value='".$row_amr['VC']."'/>";
			
		}
		
		$xml  = "<chart caption='Grafik Tegangan Load Profile AMR' xAxisName='Tanggal Jam' yAxisName='Tegangan' >";
		$xml .= "<categories>";
		$xml .= $cat;
		//$xml .= "<category label='8/6/2006'/><category label='8/7/2006'/><category label='8/8/2006'/><category label='8/9/2006'/><category label='8/10/2006'/><category label='8/11/2006'/><category label='8/12/2006'/>";
		$xml .= "</categories>";
		$xml .= "<dataset seriesName='Vr' color='FF0000' anchorBorderColor='FF0000' anchorBgColor='FF0000'>";
		//$xml .= "<set value='1699'/><set value='1511'/><set value='1904'/><set value='1957'/><set value='1296'/>";
		$xml .= $va;
		$xml .= "</dataset>";
		$xml .= "<dataset seriesName='Vs' color='FFFF00' anchorBorderColor='FFFF00' anchorBgColor='FFFF00'>";
		//$xml .= "<set value='2042'/><set value='3210'/><set value='2994'/><set value='3115'/><set value='2844'/><set value='3576'/><set value='1862'/>";
		$xml .= $vb;
		$xml .= "</dataset>";
		$xml .= "<dataset seriesName='Vt' color='000000' anchorBorderColor='000000' anchorBgColor='000000'>";
		//$xml .= "<set value='541'/><set value='781'/><set value='920'/><set value='754'/><set value='840'/><set value='893'/><set value='451'/>";
		$xml .= $vc;
		$xml .= "</dataset>";
		
		$xml .= "</chart>";
		return $xml;
	}
	
	function ampere_xml($row_query)
	{
		
		$cat = "";
		$ia = "";
		$ib = "";
		$ic = "";
		foreach($row_query as $row_amr){
			$cat .= "<category label='".$row_amr['TGLJAM']."'/>";
			$ia .= "<set value='".$row_amr['IA']."'/>";
			$ib .= "<set value='".$row_amr['IB']."'/>";
			$ic .= "<set value='".$row_amr['IC']."'/>";
		}
		
		$xml  = "<chart caption='Grafik ARUS Load Profile AMR' xAxisName='Tanggal Jam' yAxisName='Arus' >";
		$xml .= "<categories>";
		$xml .= $cat;
		//$xml .= "<category label='8/6/2006'/><category label='8/7/2006'/><category label='8/8/2006'/><category label='8/9/2006'/><category label='8/10/2006'/><category label='8/11/2006'/><category label='8/12/2006'/>";
		$xml .= "</categories>";
		$xml .= "<dataset seriesName='Ir' color='FF0000' anchorBorderColor='FF0000' anchorBgColor='FF0000'>";
		//$xml .= "<set value='1699'/><set value='1511'/><set value='1904'/><set value='1957'/><set value='1296'/>";
		$xml .= $ia;
		$xml .= "</dataset>";
		$xml .= "<dataset seriesName='Is' color='FFFF00' anchorBorderColor='FFFF00' anchorBgColor='FFFF00'>";
		//$xml .= "<set value='2042'/><set value='3210'/><set value='2994'/><set value='3115'/><set value='2844'/><set value='3576'/><set value='1862'/>";
		$xml .= $ib;
		$xml .= "</dataset>";
		$xml .= "<dataset seriesName='It' color='000000' anchorBorderColor='000000' anchorBgColor='000000'>";
		//$xml .= "<set value='541'/><set value='781'/><set value='920'/><set value='754'/><set value='840'/><set value='893'/><set value='451'/>";
		$xml .= $ic;
		$xml .= "</dataset>";
		$xml .= "</chart>";
		return $xml;
	}
	
	//miscellanous function
	function extract_tgl($tgl)
	{
		$tgl_arr = explode("/", $tgl);
		
		return $tgl_arr[2].$tgl_arr[0].$tgl_arr[1];
	}
	
	function extract_tgl_display($tgl)
	{
		$tgl_arr = explode("/", $tgl);
		
		return $tgl_arr[1]."-".$tgl_arr[0]."-".$tgl_arr[2];
	}
	
	function firstday_2lastmonth()
	{
		$date = date('Y-m');//"1998-08";
		
		$fday = strtotime ( '-2 month' , strtotime ( $date ) ) ;
		$fday = date ( 'm/Y' , $fday );

		$day_arr = explode('/', $fday);
		$new_fday = $day_arr[0].'/01/'.$day_arr[1];
		return $new_fday;
	}
	
	function lastday_lastmonth()
	{
		$date = date('Y-m');//"1998-08";
		
		$bulan = strtotime ( '-1 month' , strtotime ( $date ) ) ;
		$bulan = date ( 'm/Y' , $bulan );
		$last_day = date("j", $this->lastDateOfMonth($bulan));

		$tgl = $bulan.'/'.$last_day;
		$tgl_arr = explode('/', $tgl);
		$new_tgl = $tgl_arr[0].'/'.$tgl_arr[2].'/'.$tgl_arr[1];
		return $new_tgl;
	}
	
	function lastDateOfMonth($Month, $Year=-1) 
	{
		error_reporting(0);
	    if ($Year < 0) $Year = 0+date("Y");
	    $aMonth         = mktime(0, 0, 0, $Month, 1, $Year);
	    $NumOfDay       = 0+date("t", $aMonth);
	    $LastDayOfMonth = mktime(0, 0, 0, $Month, $NumOfDay, $Year);
	    return $LastDayOfMonth;
	}
	
	function search_id()
	{
		$data['title']  = "Pelanggan AMR ";
		$data['subtitle']  = "Pencarian Data Pelanggan";
		$data['isi']	= "pelanggan/search_id";
		
		$this->load->view('template', $data);
	}
	
	
	function search_result()
	{	
		if($_POST)
		{
			$id = trim($this->input->post('idpelg'));
			$idm = trim($this->input->post('idmeter'));
			$nama = strtoupper($this->input->post('nama'));
			$data['jml'] = $this->model->get_jml_idmeter($id, $idm, $nama);
			$data['result'] = $this->model->get_data_identitas($id,$idm,$nama,true);
			$data['title']  = "Pelanggan AMR ";
			$data['subtitle']  = "Pencarian Data Pelanggan";
			$data['isi']	= "pelanggan/search_result";
			
			$this->load->view('template', $data);
			
		}			
	}
	
	function search_detail()
	{
		$id = trim($this->uri->segment(3));
		$idm= trim($this->uri->segment(4));
		
		$data['resultId'] = $this->model->get_pelanggan_idm($id,$idm);
		$data['catatan'] = $this->model->get_catatan_pelanggan($id);
		//print_r($data['resultId']);
		//load library fusion chart
		$this->load->library('fusioncharts') ;
	    $this->swfCharts  = base_url().'public/lib/Charts/ZoomLine.swf' ;
		
		$data['result_all'] = $this->model->get_data_plg_by_idpel($id, $data['resultId']->UNITUP, $idm, $data['resultId']->KDJENISMETER);
		//grafik TEGANGAN
		$v_xml = $this->voltase_xml($data['result_all']);
		$data['graph_hist_voltase'] = $this->fusioncharts->renderChart($this->swfCharts,'',$v_xml,"hist_volt", 700, 500, false, true) ;
		
		//grafik ARUS
		$i_xml = $this->ampere_xml($data['result_all']);
		$data['graph_hist_ampere'] = $this->fusioncharts->renderChart($this->swfCharts,'',$i_xml,"hist_amp", 700, 500, false, true) ;
		
		$data['title']  = "Detail Pelanggan AMR ";
		$data['subtitle']  = "";
		$data['isi']	= "pelanggan/search_detail";
		$this->load->view('template', $data);
		
	}
	
	function catatan()
	{
		$idpel = $this->uri->segment(3);
		$data['idpel']  = $idpel;
		$data['plg'] = $this->model->get_pelanggan($idpel);
		$data['result'] = $this->model->get_catatan_pelanggan($idpel);
		$data['title']  = "Detail Catatan Pelanggan AMR";
		$data['subtitle']  = "";
		$data['isi']	= "pelanggan/catatan";
		$this->load->view('template_popup', $data);
	}
	
	function delete_catatan()
	{
		$idpel = $this->uri->segment(3);
		$tgl = $this->uri->segment(4);
		$cari = array('-', '_');
		$ganti  = array(' ', '/');
		$tanggal = str_replace($cari, $ganti, $tgl);
		
		$this->model->delete_catatan($idpel,$tanggal);
		redirect('pelanggan/catatan/'.$idpel);
	}
	
	function save_catatan()
	{
		$idpel = $this->uri->segment(3);
		$tgl = date('d/m/Y');
		if($this->input->post('tgl') != '') $tgl = $this->input->post('tgl');
		$waktu = $tgl." ".date('H:i:s');
		
		if($this->input->post('catatan') != null) $this->model->save_catatan($idpel,$waktu);
		redirect('pelanggan/catatan/'.$idpel);
	}
	
	function save_catatan_async()
	{
		$idpel = $this->uri->segment(3);
		$tgl = $this->uri->segment(4);
		$cari = array('-', '_');
		$ganti  = array(' ', '/');
		$waktu = str_replace($cari, $ganti, $tgl);
		$this->model->update_catatan($idpel, $waktu);
	}
	
	function data_ts()
	{
		$data['result'] = $this->model->get_data_ts();
		$data['title']  = "REKAP TAGIHAN SUSULAN";
		$data['subtitle']  = "";
		$data['isi']	= "pelanggan/data_ts";
		$this->load->view('template', $data);
	}
	
	function data_ts_input()
	{
		$idpel = $this->uri->segment(3);
		$data['idpel']  = $idpel;
		$data['plg'] = $this->model->get_pelanggan($idpel);
		$data['title']  = "Data Tagihan Susulan";
		$data['subtitle']  = "Input Data Tagihan Susulan";
		$data['isi']	= "pelanggan/data_ts_input";
		$this->load->view('template_popup', $data);
	}
	
	function data_ts_input_sph()
	{
		if($_POST){
			$this->model->data_ts_input_sph();
			redirect('pelanggan/data_ts');
		}
		
		$id = $this->uri->segment(3);
		$data['result'] = $this->model->get_data_ts_by_Id($id);
		$data['title']  = "Input SPH Data Tagihan Susulan";
		$data['subtitle']  = "Input SPH Data Tagihan Susulan";
		$data['isi']	= "pelanggan/data_ts_input_sph";
		$this->load->view('forms', $data);
	}
	
	function data_ts_save()
	{
		$idpel = $this->input->post('current_idpel');
		$this->model->data_ts_save();
		redirect('pelanggan/catatan/'.$idpel);
	}
	
	function data_ts_edit()
	{
		if($_POST){
			
			$this->model->data_ts_edit();
			redirect('pelanggan/data_ts');
		}
		
		$id = $this->uri->segment(3);
		$data['result'] = $this->model->get_data_ts_by_Id($id);
		
		$data['title']  = "Tagihan Susulan";
		$data['subtitle']  = "Edit Data Tagihan Susulan";
		$data['isi']	= "pelanggan/data_ts_edit";
		$this->load->view('forms', $data);
	}
	
	function data_ts_delete()
	{
		$id = $this->uri->segment(3);
		$this->model->data_ts_delete($id);
		redirect('pelanggan/data_ts/');
	}
	
	function ts_catatans() {
		$idpel = $this->uri->segment(3);
		$data['result']	= $this->model->get_ts_catatan($idpel);
		$data['title'] = "LAPORAN DATA TAGIHAN SUSULAN";
		$data['page'] = "LAPORAN DATA TAGIHAN SUSULAN";
		$this->load->view('pelanggan/data_ts_catatan',$data);
		
	}
	
	function stand_sama() {
		$data['title'] = "Data Stand Sama";
		$data['isi']	= "pelanggan/stand_sama";
		$data['area'] = $this->model->get_data_area();
		$data['current_area'] = null;
		$data['tanda']  = false;
		$data['cekkvarh']=null;
		if($_POST)
		{
			$data['tanda'] = true;
			$data['current_area'] = $this->input->post('unitup');
			$data['cekkvarh']=$this->input->post('kvarh');
			$data['result']	= $this->model->get_stand_sama($data['current_area'],$data['cekkvarh']);
		}
		$this->load->view('tampilan2', $data);
	}
	
	function stand_sama_arus() {
		$data['title'] = "Data Stand Sama Arus";
		$data['isi']	= "pelanggan/stand_sama_arus";
		$data['area'] = $this->model->get_data_area();
		$data['tahun'] = $this->model->get_periode_kwh_meter();
		$data['current_area'] = null;
		$data['arus_min'] = "0.2";
		$data['tanda']  = false;
		$data['tgl_01'] = $this->firstday_2lastmonth();
		$data['tgl_02']	= $this->lastday_lastmonth();
		$data['cekkvarh']=null;
		if($_POST)
		{
			$data['tanda'] = true;
			$data['current_area'] = $this->input->post('unitup');
			$data['arus_min'] = $this->input->post('arus_min');
			$data['blth'] = $this->input->post('blth');
			$data['cekkvarh']=$this->input->post('kvarh');
			$list_kdarea = $this->model->get_data_area();
			$data['result']	= $this->model->get_stand_sama_arus($data['current_area'], $data['blth'], $data['arus_min'], $list_kdarea,$data['cekkvarh']);
		}
		$this->load->view('tampilan2', $data);
	}
	
	function idmeter_ganda($id, $idm, $nama)
	{
		$data['title']	= "IDMeter Ganda Pelanggan";
		$data['subtitle']	= "IDMeter Ganda Pelanggan";
		$data['isi']	= "pelanggan/idmeter_ganda";
		$data['result'] = $this->model->get_data_identitas($id,$idm,$nama,true);
		$this->load->view('template', $data);
	}
	
	function lpkurang()
	{
		$data['title']  = "LP Kurang";
		$data['isi']	= "pelanggan/lp_kurang";
		$data['tanda']  = false;
		$data['tgl_01'] = $this->firstday_2lastmonth();
		$data['tgl_02']	= $this->lastday_lastmonth();
		$data['bulan_list'] = $this->model->get_periode_kwh_meter();
		$data['area'] = $this->model->get_data_area();
		$data['current_area'] = null;
		$data['bulan'] = null;
		
		if($_POST)
		{
			$data['tanda'] = true;
			$data['current_area'] = $this->input->post('unitup');
			$data['operasi'] = $this->input->post('operasi');
			$data['bulan'] = $this->input->post('bulan');
			$year = substr($this->input->post('bulan'),2,2);
			$month = substr($this->input->post('bulan'),-2);
			$table = $month.$year;
			$data['result'] = $this->model->get_data_lp_kurang($this->input->post('bulan'), $this->input->post('unitup'), $this->input->post('operasi'), $table);
		}
		$this->load->view('tampilan2', $data);
		
	}
	
	function interval_lp()
	{
		$data['title']  = "Interval LP";
		$data['isi']	= "pelanggan/interval_lp";
		$data['tanda']  = false;
		$data['interval'] = $this->model->get_interval();
		$data['area'] = $this->model->get_data_area();
		$data['current_area'] = null;
		$data['current_interval'] = null;
		
		if($_POST)
		{
			$data['tanda'] = true;
			$data['current_area'] = $this->input->post('unitup');
			$data['current_interval'] = $this->input->post('interval');
			$data['result'] = $this->model->get_interval_detail($this->input->post('unitup'), $this->input->post('interval'));
		}
		$this->load->view('tampilan2', $data);
	
	}
	
	function excel_byIdpel()
	{
		$this->load->plugin('to_excel');
		$idpel  	= $this->uri->segment(3);
		$idmeter  	= $this->uri->segment(4);
		$kdjenismeter  	= $this->uri->segment(5);
		$unitup  	= $this->uri->segment(6);
		
		to_excel($this->model->get_data_plg_by_idpel($idpel, $unitup, $idmeter, $kdjenismeter), 'DETAIL_PELANGGAN_'.$idpel);
	}
	
	//function to build excel report
	function excel_arus()
	{
		$this->load->plugin('to_excel');
		$idpel  	= $this->uri->segment(3);
		$tgl_awal	= $this->uri->segment(4);
		$tgl_akhir	= $this->uri->segment(5);
		$arus_min	= $this->uri->segment(6);
		$arus_normal= $this->uri->segment(7);
		$unitup		= $this->uri->segment(8);
		$jenis_meter = $this->uri->segment(9);
		$idmeter = $this->uri->segment(10);
		
		to_excel($this->model->get_data_arus_amr_idpel_excel($idpel, $tgl_awal, $tgl_akhir, $arus_min, $arus_normal, $unitup, $idmeter, $jenis_meter), 'ARUS_ABNORMAL_'.$idpel);
	}
	
	function excel_arus_all()
	{
		$this->load->plugin('to_excel');
		$idpel  	= $this->uri->segment(3);
		$tgl_awal	= $this->uri->segment(4);
		$tgl_akhir	= $this->uri->segment(5);
		$arus_min	= $this->uri->segment(6);
		$arus_normal= $this->uri->segment(7);
		$unitup		= $this->uri->segment(8);
		$jenis_meter = $this->uri->segment(9);
		$idmeter = $this->uri->segment(10);
	//	$this->model->get_data_amr_idpel_all($data['idpel'], $data['tgl_awal'], $data['tgl_akhir'], $data['unitup'],$data['idmeter'],$data['jenis_meter']);
		to_excel($this->model->get_data_amr_idpel_all_excel($idpel, $tgl_awal, $tgl_akhir, $unitup, $idmeter, $jenis_meter), 'ARUS_ALL_'.$idpel);
	}
	
	function excel_tegangan()
	{
		$this->load->plugin('to_excel');
		$idpel  	= $this->uri->segment(3);
		$tgl_awal	= $this->uri->segment(4);
		$tgl_akhir	= $this->uri->segment(5);
		$teg_min	= $this->uri->segment(6);
		$teg_normal = $this->uri->segment(7);
		$unitup		= $this->uri->segment(8);
		$jenis_meter = $this->uri->segment(9);
		$idmeter 	= $this->uri->segment(10);
		
		to_excel($this->model->get_data_tegangan_amr_idpel_excel($idpel, $tgl_awal, $tgl_akhir, $teg_min, $teg_normal, $unitup, $idmeter, $jenis_meter), 'TEGANGAN_ABNORMAL_'.$idpel);
	}
	
	function excel_tegangan_all()
	{
		$this->load->plugin('to_excel');
		$idpel  	= $this->uri->segment(3);
		$tgl_awal	= $this->uri->segment(4);
		$tgl_akhir	= $this->uri->segment(5);
		$teg_min	= $this->uri->segment(6);
		$teg_normal = $this->uri->segment(7);
		$unitup		= $this->uri->segment(8);
		$idmeter		= $this->uri->segment(9);
		$jenis_meter		= $this->uri->segment(10);
		
		to_excel($this->model->get_data_amr_idpel_all_excel($idpel, $tgl_awal, $tgl_akhir, $unitup, $idmeter, $jenis_meter ), 'TEGANGAN_ALL_'.$idpel);
	}
	
	function excel_pelanggan_tt() {
		$this->load->plugin('to_excel');
		to_excel($this->model->get_pelanggan_tt(), 'PELANGGAN TEGANGAN TINGGI');
	}
	
	function excel_data_ts() {
		$this->load->plugin('to_excel');
		to_excel($this->model->get_data_ts(), 'DATA_TAGIHAN_SUSULAN');
	}
	
	function excel_stand()
	{
		$this->load->plugin('to_excel');
		$idpel = $this->uri->segment(3);
		to_excel($this->model->get_stand_by_idpel($idpel), 'DATA_STAND_'.$idpel);
	}
	
	function excel_an_arus()
	{
		$this->load->plugin('to_excel');
		$tgl_awal  		= $this->uri->segment(3);
		$tgl_akhir		= $this->uri->segment(4);
		$jumlah			= $this->uri->segment(5);
		$arus_min		= $this->uri->segment(6);
		$arus_normal 	= $this->uri->segment(7);
		$unitup			= $this->uri->segment(8);
		
		$list_kdarea = $this->model->get_data_area();
		to_excel($this->model->get_data_arus_amr($tgl_awal, $tgl_akhir, $jumlah, $arus_min, $arus_normal, 
												 $unitup, $list_kdarea), 'ANALISA_ARUS');
	
	}
	
	function excel_an_teg_tm()
	{
		$this->load->plugin('to_excel');
		$tgl_awal  		= $this->uri->segment(3);
		$tgl_akhir		= $this->uri->segment(4);
		$jumlah			= $this->uri->segment(5);
		$arus_min		= $this->uri->segment(6);
		$arus_normal 	= $this->uri->segment(7);
		$unitup			= $this->uri->segment(8);
		
		$list_kdarea = $this->model->get_data_area();
		to_excel($this->model->get_data_arus_amr($tgl_awal, $tgl_akhir, $jumlah, $arus_min, $arus_normal, 
												 $unitup, $list_kdarea), 'ANALISA_ARUS');
	
	}
	
	function excel_an_teg_tr()
	{
		$this->load->plugin('to_excel');
		$tgl_awal  		= $this->uri->segment(3);
		$tgl_akhir		= $this->uri->segment(4);
		$jumlah			= $this->uri->segment(5);
		$arus_min		= $this->uri->segment(6);
		$arus_normal 	= $this->uri->segment(7);
		$unitup			= $this->uri->segment(8);
		
		$list_kdarea = $this->model->get_data_area();
		to_excel($this->model->get_data_arus_amr($tgl_awal, $tgl_akhir, $jumlah, $arus_min, $arus_normal, 
												 $unitup, $list_kdarea), 'ANALISA_ARUS');
	
	}
	
	function excel_an_kwh_pm()
	{
		$this->load->plugin('to_excel');
		$blth  		= $this->uri->segment(3);
		$persen		= $this->uri->segment(4);
		$unitup		= $this->uri->segment(5);
		
		$list_kdarea = $this->model->get_data_area();
		to_excel($this->model->get_data_kwh_meter($blth,$persen,'11',$unitup), 'ANALISA_KWH_PLUS_MINUS');
	
	}
	
	function WriteToLog($page, $var)
	{
		//ASSIGN VARIABLES TO USER INFO
		if (getenv("HTTP_CLIENT_IP"))
			$ip = getenv("HTTP_CLIENT_IP");
		else if(getenv("HTTP_X_FORWARDED_FOR"))
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		else if(getenv("REMOTE_ADDR"))
			$ip = getenv("REMOTE_ADDR");
		else
			$ip = "UNKNOWN";
		
		
		//CALL OUR LOG FUNCTION
		$this->db->insert('AMR_LOGS', array(
											'IP_KOM' => $ip,
											'PAGE' => $page,
											'LOG_TEXT' => $var,
											'USN' => $this->session->userdata('usn') ));	
	}

}	

?>