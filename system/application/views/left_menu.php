  <div class="leftpanel">
    
    <div class="logopanel">
        <h1><span>[</span> <?=$this->config->item('version')?> <span>]</span></h1>
    </div><!-- logopanel -->
    
    
    <div class="leftpanelinner">    
      <h5 class="sidebartitle">Navigation</h5>
	   <? if($this->session->userdata('usn')!='') {?>
      <ul class="nav nav-pills nav-stacked nav-bracket">
        <li class="active"><?=anchor('rekap/jml_data/new','<i class="fa fa-home"></i> <span>Home</span>');?></li>        
        <li class="nav-parent"><a href="#"><i class="fa fa-th-list"></i> <span>Analisa Data AMR</span></a>
          <ul class="children">
            <li><?=anchor('pelanggan/arus_form','<i class="fa fa-caret-right"></i> Arus');?></li>
            <li><?=anchor('pelanggan/arus_2_form','<i class="fa fa-caret-right"></i> Arus-Ukur Langsung');?></li>
            <li><?=anchor('pelanggan/tegangan_form','<i class="fa fa-caret-right"></i> Tegangan');?></li>
            <li><?=anchor('pelanggan/tegangan_tr_form','<i class="fa fa-caret-right"></i> Tegangan-Ukur TR');?></li>
            <li><?=anchor('pelanggan/kwh_pm_form','<i class="fa fa-caret-right"></i> KWH Plus Minus');?></li>
          </ul>
        </li>
		<li class="nav-parent"><a href="#"><i class="fa fa-suitcase"></i> <span>Rekap Data</span></a>
          <ul class="children">
            <li><?=anchor('rekap/jml_plg/new','<i class="fa fa-caret-right"></i> Jumlah Pelanggan');?></li>
            <!--<li><?=anchor('rekap/jml_kwh_pantau','<i class="fa fa-caret-right"></i> Jml KWH Gardu');?></li>-->
            <li><?=anchor('pelanggan/data_ts','<i class="fa fa-caret-right"></i> Rekap Tagihan Susulan');?></li>
          </ul>
        </li>
		<li class="nav-parent"><a href="#"><i class="glyphicon glyphicon-search"></i> <span>Pencarian Data</span></a>
          <ul class="children">
            <li><?=anchor('pelanggan/search_id','<i class="fa fa-caret-right"></i> Pelanggan AMR');?></li>
            <li><?=anchor('pelanggan/pelanggan_tt','<i class="fa fa-caret-right"></i> Pelanggan TT');?></li>
           <!-- <li><?=anchor('pelanggan/lpkurang','<i class="fa fa-caret-right"></i> LP Kurang');?></li>
            <li><?=anchor('pelanggan/interval_lp','<i class="fa fa-caret-right"></i> Interval LP');?></li> -->
          </ul>
        </li>        
      </ul>
		<? } else {?>
		<ul class="nav nav-pills nav-stacked nav-bracket">
        <li class="active"><?=anchor('rekap/jml_data/new','<i class="fa fa-home"></i> <span>Home</span>');?></li>          
		<li class="nav-parent"><a href="#"><i class="fa fa-suitcase"></i> <span>Rekap Data</span></a>
          <ul class="children">
            <li><?=anchor('rekap/jml_plg/new','<i class="fa fa-caret-right"></i> Jumlah Pelanggan');?></li>
          </ul>
        </li>      
      </ul>
		<? }?>
      <!-- infosummary -->
    </div>
    <!-- leftpanelinner -->
  </div><!-- leftpanel -->
   