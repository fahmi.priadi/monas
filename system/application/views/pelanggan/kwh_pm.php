
<?php
	if($tanda){	
	$i=0;
?>
<div align="center" class="style1">Periode Bulan <?php echo substr($periode,4,2)?> Tahun <?php echo substr($periode,0,4);?><br/>
Persentase Lebih dari <?php echo $persen;?>%<br/>
</div>
<br/>
<div class="table-responsive">
<table class="table" id="table2">
 <thead>
  <tr>
    <th><div align="center">BLTH</div></th>
    <th><div align="center">IDPEL</div></th>
    <th><div align="center">CATATAN</div></th>
    <th><div align="center">NAMA</div></th>
    <th><div align="center">TARIF</div></th>
    <th><div align="center">DAYA</div></th>
    <th><div align="center">KWH PLUS</div></th>
    <th><div align="center">KWH MINUS</div></th>
    <th><div align="center">PERSEN</div></th>
  </tr>
  </thead>
  <tbody>
	<?php foreach($result as $plg) { ?>
	<tr>
		<td><?php echo $plg['BLTH']?></td>
		<td><?php echo anchor(site_url('pelanggan/search_detail/'.trim($plg['IDPEL']).'/'.trim($plg['IDMETER']).'/'.$plg['KDJENISMETER']),$plg['IDPEL'])?></td>
		<?php
		if($plg['CATATAN'] == 0) {
			?>
			<td align="center">
			<?=anchor_popup('pelanggan/catatan/'.$plg['IDPEL'],'<img alt="BELUM DIPERIKSA" height="20px" width="20px" src="'.base_url().'public/images/view.png">',array())?></td>
			<?php
		} else { ?>
			<td align="center">
			<?=anchor_popup('pelanggan/catatan/'.$plg['IDPEL'],'<img alt="SUDAH DIPERIKSA" height="20px" width="20px" src="'.base_url().'public/images/view_2.png">',array())?></td>
			<?php
		}
		?>
		
		<td><?php echo $plg['NAMA']?></td>
		<td style="text-align:center;"><?php echo $plg['GOLONGAN']?></td>
		<td style="text-align:right;"><?php echo $plg['DAYATPS']?></td>
		<td style="text-align:center;"><?php echo $plg['KWH_PLUS']?></td>
		<td style="text-align:center;"><?php echo $plg['KWH_MINUS']?></td>
		<td style="text-align:center;"><?php echo $plg['PERSEN']?></td>
	</tr>
	<?php } ?>
  </tbody>
</table>
</br></br>
<div align="center">
<?=anchor('pelanggan/excel_an_kwh_pm/'.$periode.'/'.$persen.'/'.$current_area.'/','Export to Excel', array('type'=>'button', 'class'=>'btn btn-primary'))?>
</div>
</div>
<?php } ?>