
<?php
	if($tanda){	
	$i=0;
?>

<div align="center">Periode tgl <?=$tgl1?> s.d <?=$tgl2?><br/>
Batas Arus Minimum <?=$arus_min?> A dan Batas Arus Normal <?=$arus_normal?> A<br/>
Minimum Sejumlah <?=$jumlah?> kali <?php echo $nama_aream;?></div>
<br/>
<div class="table-responsive">
<table class="table" id="table2">
 <thead>
  <tr>
    <th><div align="center">UNITUP</div></th>
    <th><div align="center">IDPEL</div></th>
    <th><div align="center">CATATAN</div></th>
    <th><div align="center">NAMA</div></th>
    <th><div align="center">DAYATPS</div></th>
    <th><div align="center">JML </div></th>
  </tr>
  </thead>
  <tbody>
  <?php foreach($result as $plg){ ?>
  <tr >
    <td><?=strtoupper($plg['NAMA_UNIT'])?></td>
    <td align="center"><?=anchor('pelanggan/arus_idpel/'.$plg['IDPEL'].'/'.$tgl_awal.'/'.$tgl_akhir.'/'.$arus_min.'/'.$arus_normal.'/X/'.$plg['UNITUP'].'/'.trim($plg['IDMETER']).'/'.$plg['KDUNIT'],$plg['IDPEL'],  array('target' => '_blank'))?></td>
    
	<?php
		if($plg['CATATAN'] == 0) {
			?>
			<td align="center">
			<?=anchor_popup('pelanggan/catatan/'.$plg['IDPEL'],'<img alt="BELUM DIPERIKSA" height="20px" width="20px" src="'.base_url().'public/images/view.png">',array())?></td>
			<?php
		} else {
			?>
			<td align="center">
			<?=anchor_popup('pelanggan/catatan/'.$plg['IDPEL'],'<img alt="SUDAH DIPERIKSA" height="20px" width="20px" src="'.base_url().'public/images/view_2.png">',array())?></td>
			<?php
		}
	?>
	
    <td><?=$plg['NAMA']?></td>
    <td align="right"><?=number_format($plg['DAYATPS'],0,'','')?></td>
	<td align="right"><?=number_format($plg['JML'],0,'','')?></td>
  </tr>
  <?php } //end foreach ?> 
 </tbody>
</table>
</br></br>
<div align="center">
<?=anchor('pelanggan/excel_an_arus/'.$tgl_awal.'/'.$tgl_akhir.'/'.$jumlah.'/'.$arus_min.'/'.$arus_normal.'/'.$current_area.'/','Export to Excel', array('type'=>'button', 'class'=>'btn btn-primary'))?>
</div>
</div>
<?php	
	}							
?>
