<?php

class User extends Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model','model');
		$this->load->library('session');
		$this->load->helper('form');
	}
	
	function index()
	{
		redirect('/auth/login/');
	}
	
	
	function user_profile()
	{
		$data['title'] = "User Profile";
		$data['subtitle'] = "";
		$data['area']  = $this->model->get_data_area();
		$data['result'] = $this->model->get_user_profile($this->session->userdata('usn'));
		$data['isi'] = "user/user_profile";
		
		$this->load->view('template', $data);
			
	}
	
	function user_add()
	{
		$data['title'] = "Add Data User";
		$data['subtitle'] = "";
		$data['area']  = $this->model->get_data_area();
		$data['isi'] = "user/user_add";
		
		$this->load->view('template', $data);
	}
	
	function user_add_save()
	{
		if($this->model->cek_user($this->input->post('usn')))
		{
			if($this->model->insert_user())
				header("Location: ".base_url()."index.php/user/user_all");
			else echo "Error Saving User..!";
		}
	}
	
	function user_all()
	{
		if($this->session->userdata('level')!=1) redirect('rekap/jml_data/new');
		$data['title'] = "Data User";
		$data['subtitle'] = "";
		$data['result'] = $this->model->get_user_all();
		$data['isi'] = "user/user_all";
		
		$this->load->view('template', $data);
	}
	
	function user_edit()
	{
		$usn = $this->uri->segment(3);
		$data['title'] = "Edit Data User";
		$data['subtitle'] = "";
		$data['result'] = $this->model->get_user_profile($usn);
		$data['area']  = $this->model->get_data_area();
		$data['isi'] = "user/user_edit";
		
		$this->load->view('template', $data);
	
	}
	
	function user_edit_save()
	{
		if($this->model->update_user())
			header("Location: ".base_url()."index.php/user/user_all");
		else echo "Error Updating User..!";
	
	}
	
	function user_delete()
	{
		$usn = $this->uri->segment(3);
		
		if($this->model->delete_user($usn))
			header("Location: ".base_url()."index.php/user/user_all");
		else echo "Error Delete User..!";
	}
	
	function rekap_login()
	{
		$data['title'] = "Rekap Login User";
		$data['subtitle'] = "";
		
		$data['blth'] = date('Ym');
		if($_POST) $data['blth'] = $this->input->post('blth');
		$data['result'] = $this->model->rekap_login($data['blth']);
		$data['list_blth'] = $this->model->get_blth_login();
		$data['isi'] = "user/rekap_login";
		
		$this->load->view('template', $data);
	}
	
	function detil_rekap()
	{
		$kdjab = $this->uri->segment(3);
		$blth = $this->uri->segment(4);
		
		$data['title'] = "Rekap Login User";
		$data['subtitle'] = "";
		$data['result'] = $this->model->detil_rekap($kdjab, $blth);
		$data['isi'] = "user/detil_rekap_login";
		
		$this->load->view('template', $data);
	}
	
	function excel_rekap_login()
	{
		$this->load->plugin('to_excel');
		$blth = $this->uri->segment(3);
		to_excel($this->model->rekap_login($blth), 'MONAS_REKAP_LOGIN_'.$blth);
	
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */