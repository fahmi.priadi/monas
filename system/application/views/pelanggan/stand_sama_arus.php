<!-- Datepicker-->
<script src="<?=base_url()?>public/datepicker/jquery-1.4.2.js"></script>
<script src="<?=base_url()?>public/datepicker/ui/jquery.ui.core.js"></script>
<script src="<?=base_url()?>public/datepicker/ui/jquery.ui.widget.js"></script>
<script src="<?=base_url()?>public/datepicker/ui/jquery.ui.datepicker.js"></script>		
<link rel="stylesheet" href="<?=base_url()?>public/datepicker/themes/base/jquery.ui.all.css">

<!-- Datatables-->
		<style type="text/css" title="currentStyle">
			@import "http://10.3.0.212/test/amr_view/public/datatables/css/demo_table.css";
			@import "http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/css/TableTools.css";
		</style>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/js/ZeroClipboard.js"></script>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/js/TableTools.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				$('#example').dataTable( {
					"bProcessing": true,
					"bDeferRender": true,
					"iDisplayLength": 100,
					"sPaginationType": "full_numbers"
				} );
			} );
		</script>
		
		
<style type="text/css">
<!--
.style1 {font-size: 12px}
-->
</style>
<h1><a href="#"><?php echo $title; ?></a></h1><br/>
<table width="486" border="0" cellpadding="15" cellspacing="1" id="result">
  <tr>
  <td width="454" >
	<?=form_open('pelanggan/stand_sama_arus', array('name'=>'form1'))?>
	<table width="100%" border="0">
  <tr>
    <td width="45%">Unitup</td>
	<td><select name="unitup" >
	<?php
		foreach($area as $r){
			?>
				<option value="<?php echo $r['KDAREA'];?>" <?php if($current_area == $r['KDAREA']) echo "Selected";?>><?php echo $r['NAMA_AREA'];?></option>
			<?php
		}
	?>
	</select>
	</td>
  </tr>
	<tr>
	<td width="40%">Bulan / Tahun</td>
	<td width="60%"><select name="blth">
		<?php
		foreach($tahun as $r){
			?><option value="<?php echo $r['BLTH'];?>" <?php if($blth == $r['BLTH']) echo "Selected";?>><?php echo substr_replace($r['BLTH'],'/',4,0);?>&nbsp;&nbsp;</option>
			<?php
		}
		?>
	</select></td>
	</tr>
  <tr>
    <td>Batas Arus (A)</td>
    <td><select name="arus_min" >
      <option value="0.1" <?php if($arus_min == "0.1") echo "Selected";?>>0.1&nbsp;&nbsp;</option>
      <option value="0.2" <?php if($arus_min == "0.2") echo "Selected";?>>0.2</option>
      <option value="0.3" <?php if($arus_min == "0.3") echo "Selected";?>>0.3</option>
        </select>&nbsp;Ampere</td>
  </tr>
  
  <tr>
    <td></td>
    <td>
      <input type="checkbox" value="kvarh" name="kvarh"> KVARH&nbsp;&nbsp;
        </td>
  </tr>
  
	<tr>
	<td>&nbsp;</td>
	<td><input id="blok" type="submit" name="Submit" value="&nbsp;&nbsp;Cari&nbsp;&nbsp;" /></td>
	</tr>
</table>
</form>
</td></tr></table>
<br />
<hr>
<br />

<?php
	if($tanda){	
	$i=0;
?>
<div style="width:100%;overflow-x:scroll">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
 <thead>
  <tr>
    <th><div align="center">IDPEL</div></th>
    <th><div align="center">KDUNIT</div></th>
    <th><div align="center">NAMA AREA</div></th>
    <th><div align="center">KDJENISMETER</div></th>
    <th><div align="center">NAMA</div></th>
    <th><div align="center">DAYA</div></th>
    <th><div align="center">JUMLAH</div></th>
  </tr>
  </thead>
  <tbody>
	<?php foreach($result as $r) { ?>
	<tr>
		<td><?php echo anchor(site_url('pelanggan/byidpel/nopel/'.trim($r['NOPEL']).'/'.trim($r['IDMETER']).'/'.$r['KDJENISMETER']),$r['NOPEL'])?></td>
		<td><?php echo $r['KDUNIT']?></td>
		<td><?php echo $r['NAMA_AREA']?></td>
		<td><?php echo $r['KDJENISMETER']?></td>
		<td><?php echo $r['NAMA']?></td>
		<td><?php echo $r['DAYATPS']?></td>
		<td><?php echo $r['JML']?></td>
	</tr>
	<?php } ?>
  </tbody>
</table>
</div>
<?php } ?>