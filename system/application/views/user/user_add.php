<script type="text/javascript">
$(document).ready(function(){
	$('#_close').click(function(){
		$('._error_msg').slideToggle();
	});
});
function validasi(){
	
	var tanda = true;
	var pesan = 'Kolom berikut ini belum diisi:';
	if(document.getElementById('usn').value==""){
		pesan += '\n- Username ';
		tanda = false;
	}
	if(document.getElementById('pass1').value==""){
		pesan += '\n- Password ';
		tanda = false;
	}
	if(document.getElementById('pass2').value==""){
		pesan += '\n- Konfirmasi Password ';
		tanda = false;
	}
	if(document.getElementById('nama').value==""){
		pesan += '\n- Nama ';
		tanda = false;
	}if(document.getElementById('email').value==""){
		pesan += '\n- Email ';
		tanda = false;
	}if(document.getElementById('pass1').value!=document.getElementById('pass2').value){
		pesan += '\n- Password Tidak Cocok ';
		tanda = false;
	}
	
	if(tanda){
		return confirm("Are you sure want to save?");
	}else {
		alert(pesan);
		return false;
	}
	
}
</script>
<div class="contentpanel">  
    <div class="panel panel-default">
        <div class="panel-body panel-body-nopadding">
          
            <?=form_open('user/user_add_save', array('class'=>'form-horizontal form-bordered'))?>
            <div class="form-group">
              <label class="col-sm-3 control-label">Username</label>
              <div class="col-sm-6">
               <input type="text" class="form-control" name="usn" id="usn"/>
              </div>
            </div>
			<div class="form-group">
              <label class="col-sm-3 control-label">Nama</label>
              <div class="col-sm-6">
               <input type="text" class="form-control" name="nama" id="nama"/>
              </div>
            </div>
			<div class="form-group">
              <label class="col-sm-3 control-label">Email</label>
              <div class="col-sm-6">
               <input type="text" class="form-control" name="email" id="email"/>
              </div>
            </div>
			<div class="form-group">
              <label class="col-sm-3 control-label">Password</label>
              <div class="col-sm-6">
               <input type="password" class="form-control" name="pass1" id="pass1"/>
              </div>
            </div>
			<div class="form-group">
              <label class="col-sm-3 control-label">Confirm Password</label>
              <div class="col-sm-6">
               <input type="password" class="form-control" name="pass2" id="pass2"/>
              </div>
            </div>
            
			<div class="form-group">
              <label class="col-sm-3 control-label">Area</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="kdarea" >
                  <? foreach($area as $data) { ?>
                  <option value="<?=$data['KDAREA']?>"><?=$data['NAMA_AREA']?></option>
                   <? } //end foreach ?>
                </select>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-3 control-label">Jabatan</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="kdjab">
                  	<option value="1">MANAJER&nbsp;&nbsp;</option>
      				<option value="2">ASMAN</option>
      				<option value="3">SPV</option>
					<option value="4">COMMON USER</option>
        	   </select>
              </div>
            </div>
           
           <div class="form-group">
              <label class="col-sm-3 control-label">Level user</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="level_user">
                   <option value="0">Common User</option>
                  <option value="1" >Administrator</option>
                </select>
              </div>
            </div>
                       
           <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-6 col-sm-offset-3">
				  <button id="blok" class="btn btn-primary" type="submit" onclick="return validasi();">Submit</button>&nbsp;
				</div>
			 </div>
		  </div><!-- panel-footer -->
            
          </form>
          
        </div><!-- panel-body -->
        
      </div>
      </div>
</div>