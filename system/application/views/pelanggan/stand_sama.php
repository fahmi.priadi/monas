<!-- Datepicker-->
<script src="<?=base_url()?>public/datepicker/jquery-1.4.2.js"></script>
<script src="<?=base_url()?>public/datepicker/ui/jquery.ui.core.js"></script>
<script src="<?=base_url()?>public/datepicker/ui/jquery.ui.widget.js"></script>
<script src="<?=base_url()?>public/datepicker/ui/jquery.ui.datepicker.js"></script>		
<link rel="stylesheet" href="<?=base_url()?>public/datepicker/themes/base/jquery.ui.all.css">

<!-- Datatables-->
		<style type="text/css" title="currentStyle">
			@import "http://10.3.0.212/test/amr_view/public/datatables/css/demo_table.css";
			@import "http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/css/TableTools.css";
		</style>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/js/ZeroClipboard.js"></script>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/js/TableTools.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				$('#example').dataTable( {
					 "sPaginationType": "full_numbers",
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/swf/copy_cvs_xls_pdf.swf",
						"aButtons": [ "xls" ]
					}
				} );
			} );
		</script>
		
<style type="text/css">
<!--
.style1 {font-size: 12px}
-->
</style>
<h1><a href="#"><?php echo $title; ?></a></h1><br/>
<table width="486" border="0" cellpadding="15" cellspacing="1" id="result">
  <tr>
  <td width="454" >
	<?=form_open('pelanggan/stand_sama', array('name'=>'form1'))?>
	<table width="100%" border="0">
  <tr>
    <td width="45%">Unitup</td>
	<td><select name="unitup" >
	<?php
		foreach($area as $r){
			?>
				<option value="<?php echo $r['KDAREA'];?>" <?php if($current_area == $r['KDAREA']) echo "Selected";?>><?php echo $r['NAMA_AREA'];?></option>
			<?php
		}
	?>
	</select>
	</td>
  </tr>
  <tr>
    <td></td>
    <td>
      <input type="checkbox" value="kvarh" name="kvarh"> KVARH&nbsp;&nbsp;
        </td>
  </tr>
  
	<tr>
	<td>&nbsp;</td>
	<td><input id="blok" type="submit" name="Submit" value="&nbsp;&nbsp;Cari&nbsp;&nbsp;" /></td>
	</tr>
</table>
</form>
</td></tr></table>
<br />
<hr>
<br />

<?php
	if($tanda){	
	$i=0;
?>
<div style="width:100%;overflow-x:scroll">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
 <thead>
  <tr>
    <th><div align="center">IDPEL</div></th>
    <th><div align="center">IDMETER</div></th>
    <th><div align="center">KDJENISMETER</div></th>
    <th><div align="center">SLAWBP</div></th>
    <th><div align="center">SAHWBP</div></th>
    <th><div align="center">SLWLWBP</div></th>
    <th><div align="center">SAHLWBP</div></th>
    <th><div align="center">SLAKVARH</div></th>
    <th><div align="center">SAHKVARH</div></th>
    <th><div align="center">KWHWBP</div></th>
    <th><div align="center">KWHLWBP</div></th>
    <th><div align="center">BLOK3</div></th>
    <th><div align="center">PEMKWH</div></th>
    <th><div align="center">KWHKVARH</div></th>
  </tr>
  </thead>
  <tbody>
	<?php foreach($result as $r) { ?>
	<tr>
		<td><?php echo anchor(site_url('pelanggan/byidpel/nopel/'.trim($r['IDPEL']).'/'.trim($r['IDMETER']).'/'.$r['KDJENISMETER']),$r['IDPEL'])?></td>
		<td><?php echo $r['IDMETER']?></td>
		<td><?php echo $r['KDJENISMETER']?></td>
		<td><?php echo $r['SLAWBP']?></td>
		<td><?php echo $r['SAHWBP']?></td>
		<td><?php echo $r['SLWLWBP']?></td>
		<td><?php echo $r['SAHLWBP']?></td>
		<td><?php echo $r['SLAKVARH']?></td>
		<td><?php echo $r['SAHKVARH']?></td>
		<td><?php echo $r['KWHWBP']?></td>
		<td><?php echo $r['KWHLWBP']?></td>
		<td><?php echo $r['BLOK3']?></td>
		<td><?php echo $r['PEMKWH']?></td>
		<td><?php echo $r['KWHKVARH']?></td>
	</tr>
	<?php } ?>
  </tbody>
</table>
</div>
<?php } ?>