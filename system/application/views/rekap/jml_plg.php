<script src="<?php echo base_url();?>public/lib/Charts/FusionCharts.js"></script>

  
<p>
<?=form_open('rekap/jml_plg', array('name'=>'form1'))?>
	
              <div class="col-sm-5" style="width:150px">
    <select name="blth" class="form-control chosen-select" data-placeholder="Pilih Tahun Bulan...">
                  <? foreach($list_blth as $row){?>
	  <option value="<?=$row['BLTH']?>" <? if($row['BLTH']==$blth) echo "selected";?>><?=$row['BLTH']?>&nbsp;</option>
	  <? }?>
                </select></div>
	&nbsp;<input id="blok" type="submit" name="submit" class="btn btn-primary" value="&nbsp;&nbsp;Cari&nbsp;&nbsp;" />
</form>
 
<br />
<?php //if($blth != '201404')  {
	//echo "<h2><center>Data Sedang Proses Recovery..!</center></h2>";
//} else { ?>
<table width="100%" >
  <tr>
  	<td ><? echo $graph; ?><br /></td>   
  </tr>
  <tr>
  	<td ><div align="center"><strong>REKAP PER PENGELOLA</strong></div></td>   
  </tr>
  	<tr><td align="center"><table width="100%" class="table table-bordered mb30">
  <tr>
    <th><div align="center">BLTH</div></th>
    <th><div align="center">UNITUP</div></th>	
    <th><div align="center">NAMA AREA</div></th>
    <th><div align="center">JML PLG</div></th>
	<th><div align="center">JML METER</div></th>
	<th colspan="2"><div align="center">BERHASIL DOWNLOAD</div></th>
	<th><div align="center">BLM BERHASIL</div></th>
	</tr>
  
  <?php 
  $i=0;
  $tot_plg = 0;
  foreach($result as $plg){
  $tot_plg += $plg['JML_PLG'];
  if($i%2==0)
	$row = "row-a";
  else $row = "row-b";
  ?>
  <tr class="<?=$row?>">
    <td align="center"><?=$plg['BLTH']?></td>
    <td align="center"><?=$plg['UNITUP']?></td>
	<td align="left"><?=$plg['NAMA_AREAM']?></td>
	<td align="center"><?=$plg['JML_PLG_ALL_ID']?></td>
	<td align="center"><?=$plg['JML_PLG_ALL']?></td>
	<td align="center"><?=anchor('rekap/rinci_area/new/'.$plg['BLTH'].'/'.$plg['UNITUP'].'/'.$plg['JML_PLG'], number_format($plg['JML_PLG'],0,'','.'))?></td>
    <td align="center"><?=number_format(($plg['JML_PLG']/$plg['JML_PLG_ALL'])*100,2,',','.')?>&nbsp;%</td>
	<td align="center"><?=anchor('rekap/rinci_area_no/new/'.$plg['BLTH'].'/'.$plg['UNITUP'].'/'.$plg['SELISIH'], number_format($plg['SELISIH'],0,'','.'))?></td>
	</tr>
  
  <?php $i++;} //end foreach ?> 
  <!--<tr bgcolor="#999999" >
    <td colspan="2" align="center"><strong>Total Data </strong></td>
    <td align="right">&nbsp;</td>
    <td align="right"><strong>
      <?=number_format($tot_plg,0,'','.')?>
    </strong></td>
    </tr> -->
</table></td>
     </tr>
	 <tr align="center"><td><?=anchor('rekap/excel_rekap_jml_data/'.$blth,'<img src="'.base_url().'public/images/Excel-icon.png" width="12" height="9"/>&nbsp;Export to Excel')?>
	 </td></tr>
	 <td><tr>
	 
<table width="100%" >
  <tr>
  	<td><div align="center"><strong><br />
  	  <br />
  	  REKAP PER RAYON </strong><br />
	  </div></td>   
  </tr>
  	<tr><td align="center"><table width="100%" class="table table-bordered mb30">
  <tr>
    <th><div align="center">BLTH</div></th>
    <th><div align="center">UNITUP</div></th>	
    <th><div align="center">NAMA RAYON </div></th>
    <th><div align="center">JML PLG</div></th>
	<th><div align="center">JML METER</div></th>
	<th colspan="2"><div align="center">BERHASIL DOWNLOAD</div></th>
	<th><div align="center">BLM BERHASIL</div></th>
	</tr>	 
	
  <?php 
  $i=0;
  $tot_plg = 0;
  foreach($result2 as $plg){
  $tot_plg += $plg['JML_PLG'];
  if($i%2==0)
	$row = "row-a";
  else $row = "row-b";
  ?>
  <tr class="<?=$row?>">
    <td align="center"><?=$plg['BLTH']?></td>
    <td align="center"><?=$plg['KDUNIT']?></td>
	<td align="left"><?=$plg['NAMA_AREA']?></td>
	<td align="center"><?=$plg['JML_PLG_ALL_ID']?></td>
	<td align="center"><?=$plg['JML_PLG_ALL']?></td>
	<td align="center">
		<?php if($plg['JML_PLG'] != 0) {?>
		<a href="<?php echo site_url('rekap/area_detail_download/new/'.$plg['BLTH'].'-'.$plg['KDUNIT'].'-'.$plg['JML_PLG'].'-1/');?>"><?=number_format($plg['JML_PLG'],0,'','.')?></a>
		<?php } else echo "0"; ?>	</td>
	<td align="center"><?=number_format(($plg['JML_PLG']/$plg['JML_PLG_ALL'])*100,2,',','.')?>&nbsp;%</td>
	<td align="center">
		<?php if($plg['SELISIH'] != 0) {?>
		<a href="<?php echo site_url('rekap/area_detail_download/new/'.$plg['BLTH'].'-'.$plg['KDUNIT'].'-'.$plg['SELISIH'].'-2/');?>"><?=$plg['SELISIH']?></a>
		<?php } else echo "0"; ?>	</td>
	</tr>
	
	<?php $i++;} //end foreach ?> 
</table>
	
	 </td></tr>
	 <tr align="center">
		<td><?=anchor('rekap/excel_rekap_jml_data2/'.$blth,'<img src="'.base_url().'public/images/Excel-icon.png" width="12" height="9"/>&nbsp;Export to Excel')?></td>
	 
	 </tr>
</table>


</p>

