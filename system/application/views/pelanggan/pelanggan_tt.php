<div class="table-responsive">
          <table class="table" id="table2">
	<thead>
		<tr>
			<th><div align="center">IDPEL</div></th>
			<th><div align="center">IDMETER</div></th>
			<th><div align="center">KDJENISMETER</div></th>
			<th><div align="center">NAMA</div></th>
			<th><div align="center">TARIF</div></th>
			<th><div align="center">DAYA</div></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($result as $r) { ?>
		<tr>
			<td align="center"><a href="<?php echo site_url('pelanggan/search_detail').'/'.trim($r['IDPEL']).'/'.trim($r['IDMETER']).'/'.$r['KDJENISMETER'];?>"><?php echo $r['IDPEL'];?></a></td>
			<td align="right"><?php echo $r['IDMETER'];?></td>
			<td align="center"><?php echo $r['KDJENISMETER'];?></td>
			<td><?php echo $r['NAMA'];?></td>
			<td align="center"><?php echo $r['GOLONGAN'];?></td>
			<td align="right"><?php echo $r['DAYA'];?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
</div><br/><br/><br/>
<div align="right"><?=anchor('pelanggan/excel_pelanggan_tt/','<img src="'.base_url().'public/images/excel2.png" width="36" height="36"/>');?> </div>
<p>&nbsp;</p>
<script>
  jQuery(document).ready(function() {     
    
    jQuery('#table2').dataTable({
      "sPaginationType": "full_numbers"
    });
    
    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });
  
  
  });
</script>