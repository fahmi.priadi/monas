
<script type="text/javascript" charset="utf-8">
	$(document).ready( function () {
		$('#example').dataTable( {
			"bProcessing": true,
			"bDeferRender": true,
			"sPaginationType": "full_numbers"
		} );
	} );
	
	function popupWindow(link){
		var lebar = 1024;
		var left = (screen.width/2)-(lebar/2);
		newwindow=window.open(link,'name','scrollbars=1,height=700,width='+lebar+',top=25,left='+left);
		if (window.focus) {newwindow.focus()}
		return false;
	}
	
	function deleteConfirm() {
		var cek = confirm('Anda yakin akan menghapus data Tagihan ini?');
		if (cek==true) return true;
		else return false;
	}
</script>
<div class="table-responsive">
          <table class="table" id="table2">
<thead>
	<tr>
		<th>IDPEL</th>
		<th>Nama</th>
		<th>Tarif</th>
		<th>Daya</th>
		<th>Kategori</th>
		<th>Jenis</th>
		<th>Besar TS</th>
		<th>Action</th>
	</tr>
</thead>
<tbody>
<?php
if($result) {
	foreach($result as $r){
?>
	<tr>
		<td><a href="#" onclick="return popupWindow('<?php echo site_url('pelanggan/ts_catatans/' . $r['ID_DATA_TS']);?>')"><?php echo $r['IDPEL'];?></a></td>
		<td><?php echo $r['NAMA'];?></td>
		<td><?php echo $r['TARIF'];?></td>
		<td><?php echo $r['DAYA'];?></td>
		<td><?php echo $r['KATEGORI'];?></td>
		<td><?php echo $r['JENIS'];?></td>
		<td><?php echo $r['TS_KWH'];?></td>
		<td><a href="<?php echo site_url('pelanggan/data_ts_edit/'.$r['ID_DATA_TS']);?>">Edit</a> | <a href="<?php echo site_url('pelanggan/data_ts_delete/'.$r['ID_DATA_TS']);?>" onclick="return deleteConfirm();">Delete</a> | <a href="<?php echo site_url('pelanggan/data_ts_input_sph/'.$r['ID_DATA_TS']);?>">Input SPH</a></td>
	</tr>
<?php
	}
}
?>
</tbody>
</table>
</div>
<br/><br/>
<div align="right"><?=anchor('pelanggan/excel_data_ts/','<img src="'.base_url().'public/images/excel2.png" width="24" height="24"/>');?> </div>
<script>
  jQuery(document).ready(function() {     
    
    jQuery('#table2').dataTable({
      "sPaginationType": "full_numbers"
    });
    
    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });
  
  
  });
</script>