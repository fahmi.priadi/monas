
<script type="text/javascript" charset="utf-8">
	$(document).ready( function () {
		$('#example').dataTable( {
			"bProcessing": true,
			"bDeferRender": true,
			"sPaginationType": "full_numbers"
		} );
	} );
	
	function popupWindow(link){
		var lebar = 1024;
		var left = (screen.width/2)-(lebar/2);
		newwindow=window.open(link,'name','scrollbars=1,height=700,width='+lebar+',top=25,left='+left);
		if (window.focus) {newwindow.focus()}
		return false;
	}
	
	function deleteConfirm() {
		var cek = confirm('Anda yakin akan menghapus data user ini?');
		if (cek==true) return true;
		else return false;
	}
</script>
<div class="table-responsive">
          <table class="table" id="table2">
<thead>
	<tr>
		<th>Username</th>
		<th>Nama</th>
		<th>Email</th>
		<th>Jabatan</th>
		<th>Area</th>
		<th>Action</th>
	</tr>
</thead>
<tbody>
<?php
if($result) {
	foreach($result as $r){
?>
	<tr>
		<td><?php echo $r['USN'];?></td>
		<td><?php echo $r['NAMA'];?></td>
		<td><?php echo $r['EMAIL'];?></td>
		<td><?php switch($r['KDJAB'])
			{
				case '1':
					echo "MANAJER";
					break;
				case '2':
					echo "ASMAN";
					break;
				case '3':
					echo "SPV";
					break;
				case '4':
					echo "COMMON USER";
					break;
			}?></td>
		<td><?php echo $r['NAMA_AREA'];?></td>
		<td><a href="<?php echo site_url('user/user_edit/'.$r['USN']);?>"><i class="glyphicon glyphicon-edit"></i></a> &nbsp; <a href="<?php echo site_url('user/user_delete/'.$r['USN']);?>" onclick="return deleteConfirm();"><i class="glyphicon glyphicon-remove"></i></a> </td>
	</tr>
<?php
	}
}
?>
</tbody>
</table>
</div>
<br/><br/>
<div align="right"><?=anchor('user/excel_data_user/','<img src="'.base_url().'public/images/excel2.png" width="24" height="24"/>');?> </div>
<br />
<div align="center">
<?=anchor(site_url().'/user/user_add/','ADD USER', array('type'=>'button', 'class'=>'btn btn-primary'));?>
</div>
<script>
  jQuery(document).ready(function() {     
    
    jQuery('#table2').dataTable({
      "sPaginationType": "full_numbers"
    });
    
    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });
  
  
  });
</script>

