<!-- Datepicker-->
<script src="<?=base_url()?>public/datepicker/jquery-1.4.2.js"></script>
<script src="<?=base_url()?>public/js/jquery.chained.js"></script>
<script src="<?=base_url()?>public/datepicker/ui/jquery.ui.core.js"></script>
<script src="<?=base_url()?>public/datepicker/ui/jquery.ui.widget.js"></script>
<script src="<?=base_url()?>public/datepicker/ui/jquery.ui.datepicker.js"></script>		
<link rel="stylesheet" href="<?=base_url()?>public/datepicker/themes/base/jquery.ui.all.css">

<!-- Datatables-->
		<style type="text/css" title="currentStyle">
			@import "http://10.3.0.212/test/amr_view/public/datatables/css/demo_table.css";
			@import "http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/css/TableTools.css";
		</style>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/js/ZeroClipboard.js"></script>
		<script type="text/javascript" charset="utf-8" src="http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/js/TableTools.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				$("#interval").chained("#unitup");
				$('#example').dataTable( {
					 "sPaginationType": "full_numbers",
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "http://10.3.0.212/test/amr_view/public/datatables/TableTools/media/swf/copy_cvs_xls_pdf.swf",
						"aButtons": [ "xls" ]
					}
				} );
			} );
		</script>

<script>
	
	
	$(function() {
	
		$( "#datepicker" ).datepicker();
		
		$( "#datepicker2" ).datepicker();
		
	});
</script>

<style type="text/css">
<!--
.style1 {font-size: 12px}
-->
</style>
<h1><a href="#"><?php echo $title; ?></a></h1>
<table width="486" border="0" cellpadding="15" cellspacing="1" id="result">
  <tr>
  <td width="454" >
		  <?=form_open('pelanggan/interval_lp', array('name'=>'form1'))?>
        <table width="100%" border="0">
  <tr>
    <td width="45%">Unitup</td>
	<td><select name="unitup" id="unitup">
	<?php
		foreach($area as $r){
			?>
				<option value="<?php echo $r['KDAREA'];?>" <?php if($current_area == $r['KDAREA']) echo "Selected";?>><?php echo $r['NAMA_AREA'];?></option>
			<?php
		}
	?>
	</select>
	</td>
  </tr>

  <tr>
    <td width="40%">Interval</td>
	<td width="60%">
		<select name="interval" id="interval">
			<?php
				foreach($interval as $r){
				?>
					<option class="<?php echo $r['KDUNIT'];?>" value="<?php echo $r['LP_INTERVAL'];?>" <?php if(($current_area == $r['KDUNIT']) && ($current_interval == $r['LP_INTERVAL'])) echo "Selected";?> ><?php echo $r['LP_INTERVAL'] . " - " . $r['JML_PLG'] . " Plg";?>&nbsp;&nbsp;</option>
				<?php
				}
			?>
		</select>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input id="blok" type="submit" name="Submit" value="&nbsp;&nbsp;Cari&nbsp;&nbsp;" /></td>
  </tr>
</table>

      </form>
</td></tr></table>
<br />
<hr>
<br />

<?php
	
	if($tanda){	
		?>
		
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
	<thead>
		<tr>
			<th><div align="center">IDPEL</div></th>
			<th><div align="center">KDUNIT</div></th>
			<th><div align="center">IDMETER</div></th>
			<th><div align="center">NAMA</div></th>
			<th><div align="center">LP INTERVAL</div></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($result as $r) { ?>
		<tr>
			<td align="center"><a href="<?php echo site_url('pelanggan/byidpel/nopel').'/'.trim($r['NOPEL']).'/'.trim($r['IDMETER']).'/'.$r['KDJENISMETER'];?>"><?php echo $r['NOPEL'];?></a></td>
			<td align="right"><?php echo $r['KDUNIT'];?></td>
			<td align="right"><?php echo $r['IDMETER'];?></td>
			<td align="center"><?php echo $r['NAMA'];?></td>
			<td align="center"><?php echo $r['LP_INTERVAL'];?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
		<?php
	}
?>


