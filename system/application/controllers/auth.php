<?php

class Auth extends Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('auth_model','model');
		$this->load->library('session');
		$this->load->helper('form');
	}
	
	function index()
	{
		redirect('/auth/login/');
	}
	
	function login($errorMsg = NULL)
	{
		$data['title']  = "Login";
		$data['err']	= $errorMsg;
		$this->load->view('signin',$data);
	}
	
	function set_sess()
	{
		//get post data
		$usn = $_POST['var_username'];
		$pswd  = $_POST['var_password'];
		
		//cek isian usn dan password
		if($usn=="" OR $pswd=="")
		{	
			$this->login('Empty Username OR Password!');
		}
		else if($this->model->cek_user($usn))
		{
			error_reporting(0);
		
			/* ### Active Directory Address and usn/pwd ### */
			$ad_server = $this->config->item('ad_server');
			$ad_dn	= $this->config->item('ad_dn');			
			$ad_usn_postfix	= $this->config->item('ad_postfix');

			//Connect to Active Directory
			$ad = ldap_connect($ad_server);
			ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
			ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
	
			$adusn = $usn.$ad_usn_postfix; $adpwd = $pswd;
	
			$bind = ldap_bind($ad, $adusn, $adpwd);
			$ldapErr = ldap_errno($ad);
			
			if ($ldapErr==0) // cek domain controller success
			{  
				//Ambil data nama dari active directory
				$ldap_search_param = "(&"."(sAMAccountName=$usn)".")";
				$ldap_search_return = array('displayname','employeenumber','mail','company','department','title');
				
				$search = ldap_search($ad, $ad_dn, $ldap_search_param, $ldap_search_return);
				$entries = ldap_get_entries($ad, $search);
				
				$AD_displayName = substr(str_replace("'","\'",$entries[0]['displayname'][0]),0,150);
				$AD_employeeNumber = substr(str_replace("'","\'",$entries[0]['employeenumber'][0]),0,14);
				$AD_mail = substr(str_replace("'","\'",$entries[0]['mail'][0]),0,150);
				$AD_company = substr(str_replace("'","\'",$entries[0]['company'][0]),0,150);
				$AD_department = substr(str_replace("'","\'",$entries[0]['department'][0]),0,150);
				$AD_title = substr(str_replace("'","\'",$entries[0]['title'][0]),0,150);
				
				setcookie("usn", $usn);
				
				//UPDATE DATA USER
				$data_user = array ('usn' => $usn,
									'pswd' => md5($pswd),
									'nip' => $AD_employeeNumber,
									'nama' => $AD_displayName,
									'ad_mail' => $AD_mail);
									
				$this->model->update_user($data_user);			
								
				//ambil data dari database & set session
				$data = $this->model->get_user($usn);
				//print_r($data);
				$this->session->set_userdata('email_amr', $data->EMAIL);
				$this->session->set_userdata('tm', $data->TM);
				$this->session->set_userdata('tr', $data->TR);
				$this->session->set_userdata('kdarea', $data->KDAREA);
				$this->session->set_userdata('usn', $data->USN);
				$this->session->set_userdata('level', $data->LEVEL_USER);
				$this->session->set_userdata('nama', $data->NAMA);
				
				//WRITE TO LOG
				$this->WriteToLog("LOGIN", "LOGIN DOMAIN SUCCESS");
				
				redirect('/rekap/jml_data/new');
			}
			else if($this->model->cek_userdb($usn, md5($pswd)))
			{
				//ambil data dari database & set session
				$data = $this->model->get_user($usn);
				//print_r($data);
				$this->session->set_userdata('email_amr', $data->EMAIL);
				$this->session->set_userdata('tm', $data->TM);
				$this->session->set_userdata('tr', $data->TR);
				$this->session->set_userdata('kdarea', $data->KDAREA);
				$this->session->set_userdata('usn', $data->USN);
				$this->session->set_userdata('level', $data->LEVEL_USER);
				$this->session->set_userdata('nama', $data->NAMA);
				
				//WRITE TO LOG
				$this->WriteToLog("LOGIN", "LOGIN DB SUCCESS");
				
				redirect('/rekap/jml_data/new');
			
			}
			else $this->login('Wrong Username OR Password!'); 
		}
		else
		{
			$this->login('Sorry, you have no account!');
		}
		
	}
	
	function logout()
	{
		//WRITE TO LOG
		$this->WriteToLog("LOGOUT", "");
				
		$this->session->unset_userdata('nip');
		$this->session->unset_userdata('email_amr');
		$this->session->unset_userdata('tm');
		$this->session->unset_userdata('tr');
		$this->session->unset_userdata('kdarea');
		$this->session->unset_userdata('usn');
		$this->session->unset_userdata('level');
		$this->session->unset_userdata('nama');
		header("location: ".base_url()."index.php/auth/login");
	}
	
	function WriteToLog($page, $var)
	{
		//ASSIGN VARIABLES TO USER INFO
		if (getenv("HTTP_CLIENT_IP"))
			$ip = getenv("HTTP_CLIENT_IP");
		else if(getenv("HTTP_X_FORWARDED_FOR"))
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		else if(getenv("REMOTE_ADDR"))
			$ip = getenv("REMOTE_ADDR");
		else
			$ip = "UNKNOWN";
		
		
		//CALL OUR LOG FUNCTION
		$this->db->insert('AMR_LOGS', array(
											'IP_KOM' => $ip,
											'PAGE' => $page,
											'LOG_TEXT' => $var,
											'USN' => $this->session->userdata('usn') ));	
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */