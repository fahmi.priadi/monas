<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>public/css/table_default.css" />

<p>
<?=form_open('user/rekap_login')?>
 <div class="col-sm-5" style="width:150px">

<select name="blth"  class="form-control chosen-select" data-placeholder="Pilih Bulan Tahun...">

 <? foreach($list_blth as $data_blth) { ?>
	  	  <option value="<?=$data_blth['BLTH']?>" <? if($data_blth['BLTH']==$blth) echo 'selected';?>><?=$data_blth['BLTH']?>&nbsp;</option>
<? } ?>
</select>
</div>
&nbsp;<input type="submit" name="submit" class="btn btn-primary" value="&nbsp;&nbsp;Cari&nbsp;&nbsp;" />
</form>
</br>
<table width="100%"  class="table table-bordered mb30">
 
  <tr>
    <th><div align="center">BLTH</div></th>
    <th><div align="center">KDAREA</div></th>
    <th><div align="center">NAMA AREA</div></th>
    <th><div align="center">MANAJER</div></th>
    <th><div align="center">ASMAN</div></th>
    <th><div align="center">SPV</div></th>
    <th><div align="center">COMMON USER</div></th>
  </tr>
  
  <?php 
  $i=0;
  foreach($result as $plg){ if($i%2==0)
	$row="row-a";
  else $row="row-b";
  ?>
  <tr class="<?=$row?>">
    <td align="center"><?=$blth?></td>
    <td align="center"><?=$plg['KDAREAM']//anchor('pelanggan/detil_pelanggan/'.$plg['IDPEL'],$plg['IDPEL'])?></td>
    <td><? if($plg['KDAREAM']==$this->config->item('kdareaall')) echo "KANTOR DISTRIBUSI/WILAYAH"; else echo $plg['NAMA_AREAM']?></td>
    <td  align="center"><? if($plg['MANAJER']>0) echo anchor('user/detil_rekap/1/'.$blth,$plg['MANAJER']); else echo '0';?></td>
    <td align="center"><? if($plg['ASMAN']>0) echo anchor('user/detil_rekap/2/'.$blth,$plg['ASMAN']); else echo '0';?></td>
	<td align="center"><? if($plg['SPV']>0) echo anchor('user/detil_rekap/3/'.$blth,$plg['SPV']); else echo '0';?></td>
    <td align="center"><? if($plg['COMMON_USER']>0) echo anchor('user/detil_rekap/4/'.$blth,$plg['COMMON_USER']); else echo '0';?></td>
  </tr>
  <?php $i++;} //end foreach ?> 

  <tr>
    <td colspan="7" align="center"><? echo anchor('user/excel_rekap_login/'.$blth,'<img src="'.base_url().'public/images/Excel-icon.png" width="12" height="9"/>&nbsp;Export to Excel')?></td>
  </tr>
</table>
</p>

