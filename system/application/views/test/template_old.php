<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themepixels.com/demo/webpage/bracket/index.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 06:31:23 GMT -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?=base_url()?>public/images/favicon.png" type="image/png">

  <title>Monas - Monitoring dan Analisa Sistem AMR</title>

  <link href="<?=base_url()?>public/css/style.default.css" rel="stylesheet">
  <link href="<?=base_url()?>public/css/jquery.datatables.css" rel="stylesheet">
  

</head>

<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <? $this->load->view('left_menu');?>
  
  <div class="mainpanel">    
    <div class="headerbar">      
      <a class="menutoggle"><i class="fa fa-bars"></i></a>         
      <div class="header-right">
        <ul class="headermenu">                 
          <? $this->load->view('user_menu'); ?>
        </ul>
      </div><!-- header-right -->      
    </div><!-- headerbar -->
    
    <div class="pageheader">
      <h2><i class="fa fa-table"></i> <?=$title?> </h2>
    </div>
    <div class="contentpanel">        
    <div class="panel panel-default">
        <div class="panel-heading">          
          <h3 class="panel-title"><?=$subtitle?></h3>
          <p><?php $this->load->view($isi);?></p>
        </div>
        
      </div><!-- panel panel-default -->
       </div><!-- contentpanel -->  
        
  </div><!-- mainpanel -->
  <!-- tab-content -->
  </div>
  <!-- rightpanel -->
</section>

<script src="<?=base_url()?>public/js/jquery-1.10.2.min.js"></script>
<script src="<?=base_url()?>public/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.sparkline.min.js"></script>
<script src="<?=base_url()?>public/js/toggles.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.cookies.js"></script>
<script src="<?=base_url()?>public/js/custom.js"></script>
<script src="<?=base_url()?>public/js/jquery.datatables.min.js"></script>
<script src="<?=base_url()?>public/js/chosen.jquery.min.js"></script>



</body>

<!-- Mirrored from themepixels.com/demo/webpage/bracket/index.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 06:32:31 GMT -->
</html>
