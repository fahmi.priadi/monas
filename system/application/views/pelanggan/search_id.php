<div class="panel-body panel-body-nopadding">
          
          <form class="form-horizontal form-bordered" method="POST" action="<?php echo site_url('pelanggan/search_result');?>">
            
            <div class="form-group">
              <label class="col-sm-1 control-label">IDPEL</label>
              <div class="col-sm-4">
                <input type="text" placeholder="IDPEL" name="idpelg" class="form-control" />
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-1 control-label">IDMETER</label>
              <div class="col-sm-4">
                <input type="text" placeholder="IDMETER" name="idmeter" class="form-control" />
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-1 control-label">NAMA</label>
              <div class="col-sm-4">
                <input type="text" placeholder="Nama Pelanggan" name="nama" class="form-control" />
              </div>
            </div>   
			
             <div class="form-group">
              <label class="col-sm-1 control-label">&nbsp;</label>
              <div class="col-sm-4">
                <button class="btn btn-primary">Submit</button>&nbsp;
				  <button class="btn btn-default">Cancel</button>
              </div>
            </div> 

</form>
</div>
<hr>

