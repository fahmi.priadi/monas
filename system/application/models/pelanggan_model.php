<?php
class Pelanggan_model extends Model
{
	function Pelanggan_model()
	{
		parent::Model();
		$this->load->library('session');
	}
	
	function get_data_plg_idpel($idpel, $jenis_meter, $kdunit)
	{
		$sql = "select t.idmeter, t.nopel, t.nama, t.alamat, t.dayatps, t.golongan as tarif, t.kdjenismeter, a.nama_area
				from meter t left join area a on t.kdunit = a.kdarea
				where t.nopel='".$idpel."' and t.kdunit='".$kdunit."'" ;
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->row();
	}	
	
	function get_data_amr_idpel_all($idpel, $tgl_awal, $tgl_akhir, $unitup, $idmeter, $kdjenismeter)
	{
	    
		$sql = "SELECT TRIM(T.NOPEL) AS IDPEL, T.NAMA, T.DAYATPS,
					   T.IDMETER,V.TGLJAM,
				       ROUND(V.VA, 2) VA, ROUND(V.VB, 2) VB, ROUND(V.VC, 2) VC, 
					   ROUND(V.IA, 3) IA, ROUND(V.IB, 3) IB, ROUND(V.IC, 3) IC
				FROM	   
				(SELECT V.TGLJAM,V.KDUNIT,V.IDMETER,V.KDJENISMETER,
				       ROUND(V.VA, 2) VA, ROUND(V.VB, 2) VB, ROUND(V.VC, 2) VC, 
					   ROUND(V.IA, 3) IA, ROUND(V.IB, 3) IB, ROUND(V.IC, 3) IC
				  FROM V_LOADPROFILE PARTITION(AMR".$unitup.") V
				 WHERE V.UNITUP='".$unitup."' AND TRIM(V.IDMETER) = '".$idmeter."'				  
				   AND SUBSTR(V.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
				 ORDER BY TGLJAM) V, METER T
                 WHERE V.KDUNIT=T.KDUNIT AND TRIM(V.IDMETER)=TRIM(T.IDMETER) ";
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_data_amr_idpel_all_excel($idpel, $tgl_awal, $tgl_akhir, $unitup, $idmeter, $kdjenismeter)
	{
	    
		$sql = "SELECT TRIM(T.NOPEL) AS IDPEL, T.NAMA, T.DAYATPS,
					   T.IDMETER,T.KDJENISMETER,V.TGLJAM,
				       ROUND(V.VA, 2) VR, ROUND(V.VB, 2) VS, ROUND(V.VC, 2) VT, 
					   ROUND(V.IA, 3) IR, ROUND(V.IB, 3) \"IS\", ROUND(V.IC, 3) IT
				FROM	   
				(SELECT V.TGLJAM,V.KDUNIT,V.IDMETER,
				       ROUND(V.VA, 2) VA, ROUND(V.VB, 2) VB, ROUND(V.VC, 2) VC, 
					   ROUND(V.IA, 3) IA, ROUND(V.IB, 3) IB, ROUND(V.IC, 3) IC
				  FROM V_LOADPROFILE PARTITION(AMR".$unitup.") V
				 WHERE V.UNITUP='".$unitup."' AND TRIM(V.IDMETER) = '".trim($idmeter)."'
				   AND SUBSTR(V.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
				 ORDER BY TGLJAM) V, METER T
                 WHERE V.KDUNIT=T.KDUNIT AND TRIM(V.IDMETER)=TRIM(T.IDMETER) ";
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	
	
	function get_data_arus_amr($tgl_awal, $tgl_akhir, $jml_data, $arus_min, $arus_normal, $unitup, $list_kdarea)
	{
		$partisi="" ;
		$whereup="" ;
		$where_plg = "";
		
		if ($unitup!= $this->config->item('kdareaall')) 
		{
			$partisi=" partition(AMR".$unitup.")" ;
			$whereup=" UNITUP='".$unitup."' AND " ;
			
			$sql = "SELECT T.KDUNIT, T.UNITUP,
					   T.NAMA_AREA NAMA_UNIT,
					   TRIM(M.NOPEL) AS IDPEL,
				       M.NAMA,
				       M.DAYATPS,
				       'X' AS KDJENISMETER, 
					   T.IDMETER,
					   T.JML, 
					   (SELECT COUNT(1) FROM AMR_NOTE WHERE IDPEL = TRIM(M.NOPEL)) CATATAN
				 FROM	   
				 (SELECT KDUNIT,UNITUP,NAMA_AREA,IDMETER,COUNT(KDUNIT||IDMETER) AS JML FROM V_LOADPROFILE ".$partisi." T
				 LEFT JOIN AREA A ON T.KDUNIT = A.KDAREA
				 WHERE ".$whereup." SUBSTR(T.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
				   AND ((T.IA < ".$arus_min." AND T.IB > ".$arus_normal." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB < ".$arus_min." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB > ".$arus_normal." AND T.IC < ".$arus_min.") OR
					   (T.IA < ".$arus_min." AND T.IB < ".$arus_min." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB < ".$arus_min." AND T.IC < ".$arus_min.") OR
				       (T.IA < ".$arus_min." AND T.IB > ".$arus_normal." AND T.IC < ".$arus_min."))
				 GROUP BY KDUNIT,UNITUP,NAMA_AREA,IDMETER
				HAVING COUNT(KDUNIT||IDMETER) > $jml_data) T, METER M
				WHERE T.KDUNIT = M.KDUNIT 
				AND TRIM(T.IDMETER) = TRIM(M.IDMETER)" ;
		} else {
		
			$sql = '';
			foreach($list_kdarea as $r){
			
			if($r['KDAREA'] != $this->config->item('kdareaall')){
				if($r['KDAREA'] != "54888"){
				$partisi = " partition(AMR".$r['KDAREA'].")" ;

				$sql .= "SELECT T.KDUNIT, T.UNITUP,
							   T.NAMA_AREA NAMA_UNIT,
							   TRIM(M.NOPEL) AS IDPEL,
							   M.NAMA,
							   M.DAYATPS,
							   '' AS KDJENISMETER, 
							   T.IDMETER,
							   T.JML,
							   (SELECT COUNT(1) FROM AMR_NOTE WHERE IDPEL = TRIM(M.NOPEL)) CATATAN
						 FROM
						 (SELECT KDUNIT,UNITUP,NAMA_AREA,IDMETER,COUNT(KDUNIT||IDMETER||KDJENISMETER) AS JML FROM V_LOADPROFILE ".$partisi." T
						 LEFT JOIN AREA A ON T.KDUNIT = A.KDAREA
						 WHERE UNITUP = '".$r['KDAREA']."' AND SUBSTR(T.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
						   AND ((T.IA < ".$arus_min." AND T.IB > ".$arus_normal." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB < ".$arus_min." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB > ".$arus_normal." AND T.IC < ".$arus_min.") OR
					   (T.IA < ".$arus_min." AND T.IB < ".$arus_min." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB < ".$arus_min." AND T.IC < ".$arus_min.") OR
				       (T.IA < ".$arus_min." AND T.IB > ".$arus_normal." AND T.IC < ".$arus_min."))
						GROUP BY KDUNIT,UNITUP,NAMA_AREA,IDMETER
						HAVING COUNT(KDUNIT||IDMETER) > $jml_data) T, METER M
						WHERE T.KDUNIT = M.KDUNIT 
						AND TRIM(T.IDMETER) = TRIM(M.IDMETER)
						UNION ";
					}
				}
			}
			$sql = substr($sql,0,-6);
		}
		
		
		//echo $sql;
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}

	
	function get_data_arus_amr_idpel($idpel, $tgl_awal, $tgl_akhir, $arus_min, $arus_normal, $unitup, $idmeter, $kdjenismeter)
	{
		$sql = "SELECT T.KDUNIT UNITUP, TRIM(M.NOPEL) AS IDPEL, M.NAMA, M.DAYATPS,M.IDMETER,
		        T.TGLJAM, ROUND(T.VA, 2) VA, ROUND(T.VB, 2) VB, ROUND(T.VC, 2) VC, 
					      ROUND(T.IA, 3) IA, ROUND(T.IB, 3) IB, ROUND(T.IC, 3) IC
				FROM		  
		        (SELECT T.KDUNIT,T.IDMETER,T.TGLJAM, 
						ROUND(T.VA, 2) VA, ROUND(T.VB, 2) VB, ROUND(T.VC, 2) VC, 
					    ROUND(T.IA, 3) IA, ROUND(T.IB, 3) IB, ROUND(T.IC, 3) IC
				  FROM V_LOADPROFILE PARTITION(AMR".$unitup.") T
				 WHERE T.UNITUP='".$unitup."' AND T.IDMETER = '".$idmeter."'
				   AND SUBSTR(T.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
				   AND ((T.IA < ".$arus_min." AND T.IB > ".$arus_normal." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB < ".$arus_min." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB > ".$arus_normal." AND T.IC < ".$arus_min.") OR
					   (T.IA < ".$arus_min." AND T.IB < ".$arus_min." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB < ".$arus_min." AND T.IC < ".$arus_min.") OR
				       (T.IA < ".$arus_min." AND T.IB > ".$arus_normal." AND T.IC < ".$arus_min."))
				 ORDER BY TGLJAM) T, METER M
                 WHERE t.kdunit=m.kdunit and t.idmeter=m.idmeter ";
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_data_arus_amr_idpel_excel($idpel, $tgl_awal, $tgl_akhir, $arus_min, $arus_normal, $unitup, $idmeter, $kdjenismeter)
	{
		$sql = "SELECT T.KDUNIT UNITUP, TRIM(M.NOPEL) AS IDPEL, M.NAMA, M.DAYATPS,M.IDMETER,
		        T.TGLJAM, ROUND(T.VA, 2) VR, ROUND(T.VB, 2) VS, ROUND(T.VC, 2) VT, 
					      ROUND(T.IA, 3) IR, ROUND(T.IB, 3) \"IS \", ROUND(T.IC, 3) IT
				FROM		  
		        (SELECT T.KDUNIT,T.IDMETER,T.TGLJAM, 
						ROUND(T.VA, 2) VA, ROUND(T.VB, 2) VB, ROUND(T.VC, 2) VC, 
					    ROUND(T.IA, 3) IA, ROUND(T.IB, 3) IB, ROUND(T.IC, 3) IC
				  FROM V_LOADPROFILE PARTITION(AMR".$unitup.") T
				 WHERE T.UNITUP='".$unitup."' AND T.IDMETER = '".$idmeter."'
				   AND SUBSTR(T.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
				   AND ((T.IA < ".$arus_min." AND T.IB > ".$arus_normal." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB < ".$arus_min." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB > ".$arus_normal." AND T.IC < ".$arus_min.") OR
					   (T.IA < ".$arus_min." AND T.IB < ".$arus_min." AND T.IC > ".$arus_normal.") OR
				       (T.IA > ".$arus_normal." AND T.IB < ".$arus_min." AND T.IC < ".$arus_min.") OR
				       (T.IA < ".$arus_min." AND T.IB > ".$arus_normal." AND T.IC < ".$arus_min."))
				 ORDER BY TGLJAM) T, METER M
                 WHERE T.KDUNIT=M.KDUNIT AND TRIM(T.IDMETER)=TRIM(M.IDMETER) ";
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	
	function get_data_tegangan_amr($tgl_awal, $tgl_akhir, $jml_data, $teg_min, $teg_normal, $unitup, $list_kdarea)
	{
		$partisi="" ;
		$whereup="" ;
		$where_plg = "";
		
		
		if ($unitup!= $this->config->item('kdareaall')) 
		{
			$partisi=" partition(AMR".$unitup.")" ;
			$whereup=" UNITUP='".$unitup."' AND " ;
		
			$sql = "SELECT M.KDUNIT, M.UNITUP,
					   T.NAMA_AREA NAMA_UNIT,
					   TRIM(M.NOPEL) AS IDPEL,
				       M.NAMA,
				       M.DAYATPS,
					   TRIM(T.IDMETER) AS IDMETER,
					   T.JML,
					   (SELECT COUNT(1) FROM AMR_NOTE WHERE IDPEL = TRIM(M.NOPEL)) CATATAN
				 FROM	   
				 (SELECT KDUNIT,NAMA_AREA,IDMETER,COUNT(KDUNIT||IDMETER) AS JML FROM V_LOADPROFILE ".$partisi." T
				 LEFT JOIN AREA A ON T.KDUNIT = A.KDAREA
				 WHERE ".$whereup." SUBSTR(T.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
				   AND ((T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min.") OR
					   (T.VA < ".$teg_min." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC < ".$teg_min.") OR
				       (T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min."))
				 GROUP BY KDUNIT,NAMA_AREA,IDMETER
				HAVING COUNT(KDUNIT||IDMETER) > ".$jml_data.") T, METER M
				WHERE T.KDUNIT = M.KDUNIT
				AND TRIM(T.IDMETER) = TRIM(M.IDMETER)";
				
		} else {
		
			$sql = '';
			foreach($list_kdarea as $r){
			
				if($r['KDAREA'] != $this->config->item('kdareaall')){
					if($r['KDAREA'] != "54888"){
				$partisi = " partition(AMR".$r['KDAREA'].")" ;

				$sql .= "SELECT M.KDUNIT, M.UNITUP,
							   T.NAMA_AREA NAMA_UNIT,
							   TRIM(M.NOPEL) AS IDPEL,
							   M.NAMA,
							   M.DAYATPS, 
							   TRIM(T.IDMETER) AS IDMETER,
							   T.JML,
							   (SELECT COUNT(1) FROM AMR_NOTE WHERE IDPEL = TRIM(M.NOPEL)) CATATAN
						 FROM
						 (SELECT KDUNIT,NAMA_AREA,IDMETER,COUNT(KDUNIT||IDMETER) AS JML FROM V_LOADPROFILE ".$partisi." T
						 LEFT JOIN AREA A ON T.KDUNIT = A.KDAREA
						 WHERE UNITUP = '".$r['KDAREA']."' AND SUBSTR(T.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
						   AND ((T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC > ".$teg_normal.") OR
							   (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
							   (T.VA > ".$teg_normal." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min.") OR
							   (T.VA < ".$teg_min." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
							   (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC < ".$teg_min.") OR
							   (T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min."))
						 GROUP BY KDUNIT,NAMA_AREA,IDMETER
						HAVING COUNT(KDUNIT||IDMETER) > ".$jml_data.") T, METER M
						WHERE T.KDUNIT = M.KDUNIT 
						AND TRIM(T.IDMETER) = TRIM(M.IDMETER)
						UNION ";
					}
				}
			}
			$sql = substr($sql,0,-6);
		}
				
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_data_tegangan_amr_tm($tgl_awal, $tgl_akhir, $jml_data, $teg_min, $teg_normal, $unitup, $list_kdarea)
	{
		$partisi="" ;
		$whereup="" ;
		$where_plg = "";
		
		
		if ($unitup!= $this->config->item('kdareaall')) 
		{
			$partisi=" partition(AMR".$unitup.")" ;
			$whereup=" UNITUP='".$unitup."' AND " ;
		
			$sql = "SELECT M.KDUNIT, M.UNITUP,
					   T.NAMA_AREA NAMA_UNIT,
					   TRIM(M.NOPEL) AS IDPEL,
				       M.NAMA,
				       M.DAYATPS,
					   TRIM(T.IDMETER) AS IDMETER,
					   T.JML,
					   (SELECT COUNT(1) FROM AMR_NOTE WHERE IDPEL = TRIM(M.NOPEL)) CATATAN
				 FROM	   
				 (SELECT KDUNIT,NAMA_AREA,IDMETER,COUNT(KDUNIT||IDMETER) AS JML FROM V_LOADPROFILE ".$partisi." T
				 LEFT JOIN AREA A ON T.KDUNIT = A.KDAREA
				 WHERE ".$whereup." SUBSTR(T.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
				   AND ((T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min.") OR
					   (T.VA < ".$teg_min." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC < ".$teg_min.") OR
				       (T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min."))
				 GROUP BY KDUNIT,NAMA_AREA,IDMETER
				HAVING COUNT(KDUNIT||IDMETER) > ".$jml_data.") T, METER M
				WHERE T.KDUNIT = M.KDUNIT AND M.DAYATPS >= 200000
				AND TRIM(T.IDMETER) = TRIM(M.IDMETER)";
				
		} else {
		
			$sql = '';
			foreach($list_kdarea as $r){
			
				if($r['KDAREA'] != $this->config->item('kdareaall')){
					if($r['KDAREA'] != "54888"){
				$partisi = " partition(AMR".$r['KDAREA'].")" ;

				$sql .= "SELECT M.KDUNIT, M.UNITUP,
							   T.NAMA_AREA NAMA_UNIT,
							   TRIM(M.NOPEL) AS IDPEL,
							   M.NAMA,
							   M.DAYATPS, 
							   TRIM(T.IDMETER) AS IDMETER,
							   T.JML,
							   (SELECT COUNT(1) FROM AMR_NOTE WHERE IDPEL = TRIM(M.NOPEL)) CATATAN
						 FROM
						 (SELECT KDUNIT,NAMA_AREA,IDMETER,COUNT(KDUNIT||IDMETER) AS JML FROM V_LOADPROFILE ".$partisi." T
						 LEFT JOIN AREA A ON T.KDUNIT = A.KDAREA
						 WHERE UNITUP = '".$r['KDAREA']."' AND SUBSTR(T.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
						   AND ((T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC > ".$teg_normal.") OR
							   (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
							   (T.VA > ".$teg_normal." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min.") OR
							   (T.VA < ".$teg_min." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
							   (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC < ".$teg_min.") OR
							   (T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min."))
						 GROUP BY KDUNIT,NAMA_AREA,IDMETER
						HAVING COUNT(KDUNIT||IDMETER) > ".$jml_data.") T, METER M
						WHERE T.KDUNIT = M.KDUNIT AND M.DAYATPS >= 200000
						AND TRIM(T.IDMETER) = TRIM(M.IDMETER)
						UNION ";
					}
				}
			}
			$sql = substr($sql,0,-6);
		}
				
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_data_tegangan_amr_idpel($idpel, $tgl_awal, $tgl_akhir, $teg_min, $teg_normal, $unitup, $idmeter, $kdjenismeter)
	{
		$sql = "SELECT T.KDUNIT, T.UNITUP, TRIM(M.NOPEL) AS IDPEL, M.NAMA, M.DAYATPS,M.IDMETER,
		        T.TGLJAM, ROUND(T.VA, 2) VA, ROUND(T.VB, 2) VB, ROUND(T.VC, 2) VC, 
					      ROUND(T.IA, 3) IA, ROUND(T.IB, 3) IB, ROUND(T.IC, 3) IC
				FROM		  
		        (SELECT T.KDUNIT,T.UNITUP,TRIM(T.IDMETER) AS IDMETER,T.KDJENISMETER,T.TGLJAM, 
						ROUND(T.VA, 2) VA, ROUND(T.VB, 2) VB, ROUND(T.VC, 2) VC, 
					    ROUND(T.IA, 3) IA, ROUND(T.IB, 3) IB, ROUND(T.IC, 3) IC
				  FROM V_LOADPROFILE PARTITION(AMR".$unitup.") T
				 WHERE T.UNITUP='".$unitup."' AND T.IDMETER = '".$idmeter."'
				   AND SUBSTR(T.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
				   AND ((T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min.") OR
					   (T.VA < ".$teg_min." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC < ".$teg_min.") OR
				       (T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min."))
				 ORDER BY TGLJAM) T, METER M
                 WHERE T.KDUNIT=M.KDUNIT AND TRIM(T.IDMETER)=TRIM(M.IDMETER) ";
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}

	function get_data_tegangan_amr_idpel_excel($idpel, $tgl_awal, $tgl_akhir, $teg_min, $teg_normal, $unitup, $idmeter, $kdjenismeter)
	{
		$sql = "SELECT T.KDUNIT, T.UNITUP, TRIM(M.NOPEL) AS IDPEL, M.NAMA, M.DAYATPS,M.KDJENISMETER,
		        T.TGLJAM, ROUND(T.VA, 2) VR, ROUND(T.VB, 2) VS, ROUND(T.VC, 2) VT, 
					      ROUND(T.IA, 3) IR, ROUND(T.IB, 3) \"IS \", ROUND(T.IC, 3) IT
				FROM		  
		        (SELECT T.KDUNIT, T.UNITUP,T.IDMETER,T.KDJENISMETER,T.TGLJAM, 
						ROUND(T.VA, 2) VA, ROUND(T.VB, 2) VB, ROUND(T.VC, 2) VC, 
					    ROUND(T.IA, 3) IA, ROUND(T.IB, 3) IB, ROUND(T.IC, 3) IC
				  FROM V_LOADPROFILE PARTITION(AMR".$unitup.") T
				 WHERE T.UNITUP='".$unitup."' AND T.IDMETER = '".$idmeter."'
				   AND SUBSTR(T.TGLJAM, 0, 8) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."'
				   AND ((T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min.") OR
					   (T.VA < ".$teg_min." AND T.VB < ".$teg_min." AND T.VC > ".$teg_normal.") OR
				       (T.VA > ".$teg_normal." AND T.VB < ".$teg_min." AND T.VC < ".$teg_min.") OR
				       (T.VA < ".$teg_min." AND T.VB > ".$teg_normal." AND T.VC < ".$teg_min."))
				 ORDER BY TGLJAM) T, METER M
                 WHERE T.KDUNIT=M.KDUNIT AND TRIM(T.IDMETER)=TRIM(M.IDMETER)";
		//echo $sql;		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_jml_idmeter($id, $idm, $nama){
		if($id!='' and $idm!='' and $nama!='') //111
			$where = " where trim(nopel)='$id' and trim(idmeter) = '$idm' and nama like '%$nama%'";
		if($id!='' and $idm!='' and $nama=='') //110
			$where = " where trim(nopel)='$id' and trim(idmeter) = '$idm' ";
		if($id!='' and $idm=='' and $nama=='') //100
			$where = " where trim(nopel)='$id' ";
		if($id=='' and $idm=='' and $nama!='') //001
			$where = " where nama like '%$nama%'";
		if($id=='' and $idm!='' and $nama!='') //011
			$where = " where trim(idmeter) = '$idm' and nama like '%$nama%'";
		if($id!='' and $idm=='' and $nama!='') //101
			$where = " where trim(nopel)='$id' and nama like '%$nama%'";
		if($id=='' and $idm!='' and $nama=='') //010
			$where = " where trim(idmeter) = '$idm' ";
		if($id=='' and $idm=='' and $nama=='') //000
			$where = " ";
		
		$sql = "SELECT COUNT(1) AS JML FROM meter ".$where;
		$query	= $this->db->query($sql);
		return $query->row()->JML;
	}
	
	function get_pelanggan_idm($id, $idm)
	{
		$sql = "SELECT * FROM meter LEFT JOIN AREA ON KDUNIT = KDAREA where trim(nopel)='$id' and trim(idmeter) = '$idm'";
		$query	= $this->db->query($sql); 
		return $query->row();
	}
	
	function get_data_identitas($id,$idm,$nama,$ganda){
		if($id!='' and $idm!='' and $nama!='') //111
			$where = " where trim(nopel)='$id' and trim(idmeter) = '$idm' and nama like '%$nama%'";
		if($id!='' and $idm!='' and $nama=='') //110
			$where = " where trim(nopel)='$id' and trim(idmeter) = '$idm' ";
		if($id!='' and $idm=='' and $nama=='') //100
			$where = " where trim(nopel)='$id' ";
		if($id=='' and $idm=='' and $nama!='') //001
			$where = " where nama like '%$nama%'";
		if($id=='' and $idm!='' and $nama!='') //011
			$where = " where trim(idmeter) = '$idm' and nama like '%$nama%'";
		if($id!='' and $idm=='' and $nama!='') //101
			$where = " where trim(nopel)='$id' and nama like '%$nama%'";
		if($id=='' and $idm!='' and $nama=='') //010
			$where = " where trim(idmeter) = '$idm' ";
		if($id=='' and $idm=='' and $nama=='') //000
			$where = " ";
		$sql = "SELECT * FROM meter LEFT JOIN AREA ON KDUNIT = KDAREA ".$where;
		//echo $sql;
		$query	= $this->db->query($sql);
		if($ganda) return $query->result_array();
		else return $query->row();
	}
	
	function get_data_plg_by_idpel($idpel,$unitup,$idmeter,$kdjenis)
	{		
		$idmeter = trim($idmeter);
		$sql = "SELECT '".trim($idpel)."' AS IDPEL, '".trim($unitup)."' AS UNITUP,t.* 
				FROM v_loadprofile t 
				WHERE IDMETER = '".$idmeter."'
					and to_date(blth, 'YYYYMM') between add_months(sysdate, -4) and sysdate
				ORDER BY TGLJAM";
		//echo $sql;
		$query	= $this->db->query($sql);
		return $query->result_array();
	}

	function get_data_plg_by_idpel_excel($idpel,$unitup,$idmeter,$kdjenis,$nama){
		$idmeter = trim($idmeter);
		$sql = "SELECT '".$idpel."' AS IDPEL, '".$unitup."' AS UNITUP,
				t.IDMETER,
				t.KDJENISMETER,
				TGLJAM,
				'".$nama."' AS NAMA,
				VA AS VR,
				VB AS VS,
				VC AS VT,
				IA AS IR,
				IB AS \"IS\",
				IC AS IT,
				BLTH,
				KWH_PLUS,
				KWH_MINUS,
				PERSEN,
				KDUNIT
				FROM v_loadprofile t WHERE IDMETER = '".$idmeter."'
				ORDER BY TGLJAM";
		//echo $sql;
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_pelanggan_tt(){
		$sql = "SELECT NOPEL AS IDPEL,IDMETER,KDJENISMETER,NAMA,GOLONGAN,DAYATPS AS DAYA FROM meter 
				WHERE GOLONGAN='I4' ORDER BY nopel";
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_periode_kwh_meter(){
		$sql = "SELECT DISTINCT blth from rkp_persen_kwh order by blth DESC";
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_data_kwh_meter($blth,$persen,$jenismeter,$unitup){
		$where = '';
		if($unitup != $this->config->item('kdareaall')) $where .= "B.UNITUP = '".$unitup."' AND ";
		$sql = "SELECT A.BLTH,
						A.IDPEL,
						B.IDMETER,
						B.KDJENISMETER,
						B.NAMA,
						B.GOLONGAN,
						B.DAYATPS,
						A.KWH_PLUS,
						A.KWH_MINUS ,
						ROUND(A.PERSEN) PERSEN,
						(SELECT COUNT(1) FROM AMR_NOTE WHERE IDPEL = TRIM(A.IDPEL)) CATATAN 
				FROM RKP_PERSEN_KWH A, METER B 
				WHERE ".$where." A.BLTH='".$blth."' AND ROUND(A.PERSEN) >= ".$persen." 
				AND A.IDPEL=B.NOPEL ORDER BY A.PERSEN ASC";
		//echo $sql;
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_data_area()
	{
		if($this->session->userdata('kdarea') != $this->config->item('kdareaall'))
			$where = " WHERE KDAREAM = '".$this->session->userdata('kdarea')."'";
		else $where = "";
		
		$sql = "SELECT DISTINCT KDAREAM AS KDAREA, NAMA_AREAM AS NAMA_AREA 
				FROM AREA $where
				order by KDAREAM";
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_catatan_pelanggan($idpel){
		$sql = "SELECT IDPEL,CATATAN,ID_USER,TO_CHAR(TGL_KEJADIAN, 'DD/MM/YYYY HH24:MI:SS') AS TGL FROM AMR_NOTE WHERE IDPEL = '".$idpel."' ORDER BY TGL_KEJADIAN ASC";
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_pelanggan($idpel){
		$sql = "SELECT t.nama,t.alamat,t.dayatps,t.golongan FROM METER t WHERE nopel = '".$idpel."'";
		//echo $sql;
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_data_lp_kurang($blth, $unitup, $operasi, $table=null){
		$where1 = null;
		$where2 = null;
		$operand = null;
		
		$where1 = "where blth = " . $blth;
		if ($unitup!=$this->config->item('kdareaall')) $where2 = "and b.kdunit = '".$unitup."'";
		
		if ($operasi == "LebihBesar") $operand = "> 3000 ";
		elseif ($operasi == "LebihKecil") $operand = "< 650 ";
			
		$sql = "select b.kdunit, a.blth, b.nopel, a.idmeter, a.jml_penarikan, b.kdjenismeter
				from (select * from rekap_penarikan ".$where1.") a,
				meter".$table." b 
				where a.idmeter = b.idmeter and b.kdunit<>'54888' and a.jml_penarikan ".$operand . $where2;
		//echo $sql;
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_interval() {
		/*
		$sql = "select kdunit,lp_interval,count(*) jml_plg from (select kdunit,
				case interval_lp when '1' then '5'
				when '2' then '15'
				when '3' then '30'
				when '4' then '60'
				when '5' then '120' end as lp_interval
				from meter) group by kdunit,lp_interval order by to_number(kdunit),to_number(lp_interval) asc";
		*/
		$sql = "select * from (
					select kdunit,lp_interval,count(*) jml_plg from (select kdunit,
					case interval_lp when '1' then '5'
					when '2' then '15'
					when '3' then '30'
					when '4' then '60'
					when '5' then '120' end as lp_interval
					from meter0314 where kdunit<>'54888') group by kdunit,lp_interval order by to_number(kdunit),to_number(lp_interval) asc
				)
				UNION ALL
					select * from (
					select '$this->config->item('kdareaall')' as unit, lp_interval,count(*) jml_plg from (select kdunit,
					case interval_lp when '1' then '5'
					when '2' then '15'
					when '3' then '30'
					when '4' then '60'
					when '5' then '120' end as lp_interval
					from meter0314 where kdunit<>'54888') group by lp_interval order by to_number(lp_interval) asc
					)";
		
		//echo $sql;
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_interval_detail($kdunit, $lp_interval) {
		$where = null;
		if ($kdunit != $this->config->item('kdareaall') ) $where = " and kdunit = '".$kdunit."'";
		
		$sql = "select * from (
					select kdunit, nopel, idmeter, nama, kdjenismeter,
					case interval_lp when '1' then '5'
					when '2' then '15'
					when '3' then '30'
					when '4' then '60'
					when '5' then '120' end as lp_interval
					from meter0314 where kdunit<>'54888' 
				) where lp_interval = ".$lp_interval . $where;
					
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_data_ts() {
		$sql = "SELECT * FROM data_ts t";
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_data_ts_by_Id($id) {
		$sql = "SELECT T.*,TO_CHAR(SPH_TANGGAL,'dd-mm-yyyy') AS TGL FROM DATA_TS T WHERE ID_DATA_TS = ".$id;
		
		$query	= $this->db->query($sql);
		return $query->row();
	}
	
	function get_ts_catatan($id) {
		$sql = "SELECT t.*,a.CATATAN,to_char(SPH_TANGGAL,'dd-mm-yyyy') AS TGL FROM data_ts t 
				LEFT JOIN AMR_NOTE a ON t.idpel = a.idpel
				WHERE id_data_ts = ".$id;
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function save_catatan($idpel, $waktu) {
		$sql = "INSERT INTO AMR_NOTE VALUES ('".$idpel."', '".str_replace("'","''",$this->input->post('catatan'))."', to_date('".$waktu."','dd/mm/yyyy HH24:MI:SS'), SYSDATE, '".$this->session->userdata('usn')."')";
		$this->db->query($sql);
	}
	
	function update_catatan($idpel, $waktu) {
		$sql = "UPDATE AMR_NOTE SET CATATAN = '".str_replace("'","''",$this->input->post('catatan'))."'
				WHERE IDPEL = '".$idpel."' AND TGL_KEJADIAN = to_date('".$waktu."','dd/mm/yyyy HH24:MI:SS')";
		$this->db->query($sql);
	}
	
	function delete_catatan($idpel, $tanggal){
		$sql = "DELETE FROM AMR_NOTE WHERE IDPEL = '".$idpel."' AND TGL_KEJADIAN = to_date('".$tanggal."','dd/mm/yyyy HH24:MI:SS')";
		$this->db->query($sql);
	}
	
	function data_ts_save() {
		$sql = "INSERT INTO DATA_TS (	IDPEL, 
										NAMA, 
										ALAMAT, 
										TARIF, 
										DAYA,
										KATEGORI,
										JENIS,
										DESKRIPSI,
										TS_DAYA,
										TS_KWH,
										TS_RUPIAH,
										SPH_TANGGAL,
										SPH_NOMOR,
										SPH_RUPIAH,
										TGL_EDIT_SPH,
										ID_USER_TS,
										ID_USER_SPH)
				VALUES ('".$this->input->post('current_idpel')."',
						'".$this->input->post('current_nama')."',
						'".$this->input->post('current_alamat')."',
						'".$this->input->post('current_tarif')."',
						'".$this->input->post('current_daya')."',
						'".$this->input->post('kategori')."',
						'".$this->input->post('jenis')."',
						'".$this->input->post('deskripsi')."',
						'".$this->input->post('ts_daya')."',
						'".$this->input->post('ts_kwh')."',
						'".$this->input->post('ts_rupiah')."',
						to_date('".$this->input->post('sph_tanggal')."', 'dd-mm-yyyy'),
						'".$this->input->post('sph_nomor')."',
						'".$this->input->post('sph_rupiah')."',
						to_date('".$this->input->post('sph_tanggal')."', 'dd-mm-yyyy'),
						'".$this->session->userdata('email_amr')."',
						'".$this->session->userdata('email_amr')."')";
		//echo $sql;
		$this->db->query($sql);
	}
	
	function data_ts_edit() {
		$sql = "UPDATE DATA_TS SET 		KATEGORI = '".$this->input->post('kategori')."',
										JENIS = '".$this->input->post('jenis')."',
										DESKRIPSI = '".$this->input->post('deskripsi')."',
										TS_DAYA = '".$this->input->post('ts_daya')."',
										TS_KWH = '".$this->input->post('ts_kwh')."',
										TS_RUPIAH = '".$this->input->post('ts_rupiah')."',
										SPH_TANGGAL = to_date('".$this->input->post('sph_tanggal')."', 'dd-mm-yyyy'),
										SPH_NOMOR = '".$this->input->post('sph_nomor')."',
										SPH_RUPIAH = '".$this->input->post('sph_rupiah')."',
										TGL_EDIT_SPH = to_date('".$this->input->post('sph_tanggal')."', 'dd-mm-yyyy'),
										ID_USER_TS = '".$this->input->post('usn')."',
										ID_USER_SPH  = '".$this->input->post('usn')."'
				WHERE ID_DATA_TS  = '".$this->input->post('current_idpel')."'";
		//echo $sql;		
		$this->db->query($sql);
	}
	
	function data_ts_input_sph() {
		$sql = "UPDATE DATA_TS SET SPH_TANGGAL = to_date('".$this->input->post('sph_tanggal')."', 'dd-mm-yyyy'),
									SPH_NOMOR = '".$this->input->post('sph_nomor')."',
									SPH_RUPIAH = '".$this->input->post('sph_rupiah')."',
									TGL_EDIT_SPH = to_date('".$this->input->post('sph_tanggal')."', 'dd-mm-yyyy'),
									ID_USER_TS = '".$this->input->post('usn')."',
									ID_USER_SPH  = '".$this->input->post('usn')."'
				WHERE ID_DATA_TS = '".$this->input->post('current_id')."'";
		//echo $sql;
		$this->db->query($sql);
	}
	
	function data_ts_delete($id) {
		$sql = "DELETE FROM DATA_TS WHERE ID_DATA_TS = '".$id."'";
		$this->db->query($sql);
	}
	
	function get_stand_by_idpel($idpel){
		$sql = "SELECT b.* FROM meter a, phpdil.sor03141 b 
				WHERE trim(a.nopel) = b.idpel
				AND ((b.slawbp=b.sahwbp and b.slawbp>0) OR (b.slwlwbp=b.sahlwbp and b.slwlwbp>0) OR (b.slakvarh=b.sahkvarh and b.slakvarh>0))
				AND rtrim(b.idpel) = '".$idpel."'";
		//echo $sql;
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_stand_sama($area,$cekkvarh) {
		$where = null;
		$kvarh = null;
		if ($cekkvarh=='kvarh') $kvarh='OR (b.slakvarh=b.sahkvarh and b.slakvarh>0)' ;
		else $kvarh=' ' ;
		if ($area != $this->config->item('kdareaall') ) $where = " AND kdunit = '".$area."'";
		else $where = " AND a.kdunit<>'54888'";
		
		$sql = "SELECT a.*,b.* FROM meter a, phpdil.sor03141 b 
				WHERE trim(a.nopel) = b.idpel ".$where."
				AND ((b.slawbp=b.sahwbp and b.slawbp>0) OR (b.slwlwbp=b.sahlwbp and b.slwlwbp>0)".$kvarh." )";
				
		$query	= $this->db->query($sql);
		//echo $sql ;
		return $query->result_array();
	}
	
	function get_stand_sama_arus($unitup, $blth, $arus, $list_kdarea,$cekkvarh) {
		$kvarh = null;
		if ($cekkvarh=='kvarh') $kvarh='OR (b.slakvarh=b.sahkvarh and b.slakvarh>0)' ;
		else $kvarh=' ' ;
		if ($unitup != $this->config->item('kdareaall') ) {
			$where = " AND a.kdunit = '".$unitup."'";
			$partisi=" partition(AMR".$unitup.")" ;
			
			$sql = "select a.KDUNIT,c.NAMA_AREA,a.nama,a.nopel,a.IDMETER,a.KDJENISMETER,a.dayatps,count(*) jml from (SELECT a.*, b.*
					FROM meter a, phpdil.sor03141 b
					WHERE trim(a.nopel) = b.idpel
					AND ((b.slawbp = b.sahwbp and b.slawbp > 0) OR
					(b.slwlwbp = b.sahlwbp and b.slwlwbp > 0) ".$kvarh." 
					)) a, v_loadprofile ".$partisi." b, area c
					where a.idmeter=b.idmeter and b.blth='".$blth."' and (b.ia > ".$arus." or b.ib > ".$arus." or b.ic > ".$arus.") ".$where." and a.kdunit=c.kdarea
					GROUP BY a.KDUNIT,c.NAMA_AREA,a.nama,a.nopel,a.KDJENISMETER,a.dayatps,a.IDMETER";
				
		} else {
			$sql = null;
			foreach($list_kdarea as $r){
				
				if($r['KDAREA'] != $this->config->item('kdareaall')) {
					$where = " AND a.kdunit = '".$r['KDAREA']."'";
					$partisi=" partition(AMR".$r['KDAREA'].")" ;
					
					$sql .= "select a.KDUNIT,c.NAMA_AREA,a.nama,a.nopel,a.IDMETER,a.KDJENISMETER,a.dayatps,count(*) jml from (SELECT a.*, b.*
							FROM meter a, phpdil.sor03141 b
							WHERE trim(a.nopel) = b.idpel
							AND ((b.slawbp = b.sahwbp and b.slawbp > 0) OR
							(b.slwlwbp = b.sahlwbp and b.slwlwbp > 0) ".$kvarh." 
							)) a, v_loadprofile ".$partisi." b, area c
							where a.idmeter=b.idmeter and b.blth='".$blth."' and (b.ia > ".$arus." or b.ib > ".$arus." or b.ic > ".$arus.") ".$where." and a.kdunit=c.kdarea
							GROUP BY a.KDUNIT,c.NAMA_AREA,a.nama,a.nopel,a.KDJENISMETER,a.dayatps,a.IDMETER
							UNION ";
				}
			}
			
			$sql = substr($sql,0,-6);
			//echo $sql;
		}
		
		$query	= $this->db->query($sql);
		return $query->result_array();
	}
	
	function get_nama_aream($kdaream)
	{
		if($kdaream == $this->config->item('kdareall')) return "SEMUA AREA";
		else 
		{
			$sql = "SELECT distinct NAMA_AREAM FROM AREA WHERE KDAREAM='".$kdaream."'";
			
			$query	= $this->db->query($sql);
			return "AREA ".$query->row()->NAMA_AREAM;
		}
	}
	
}
?>