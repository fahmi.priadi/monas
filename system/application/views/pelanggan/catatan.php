<style>
textarea { font:12px normal Arial; }
</style>


<script src="<?=base_url()?>public/lib/js/jquery.simplemodal.js"></script>
<script src="<?=base_url()?>public/lib/js/confirm.js"></script>
<link rel="stylesheet" href="<?=base_url()?>public/css/confirm.css">
<script type="text/javascript">
	
	$(document).ready(function(){
	    $('.confirm').click(function(){
	        var answer = confirm("Anda yakin ingin menghapus catatan ini?");
	        if (answer){
	            return true;
	        } else {
	            return false;
	        }
	    });
		
		$('.updates').click(function(){
			alert(this.id);
			var catatanId = 'cat_'+this.id;
			var saveId = 'save_'+this.id;
			document.getElementById(catatanId).setAttribute("style", "width:100%; padding:4px 2px; border: 1px solid #CCCCCC; background:#FFFFFF;");
			document.getElementById(catatanId).readOnly=false;
			document.getElementById(this.id).setAttribute("style", "display:none;");
			document.getElementById(saveId).setAttribute("style", "display:inline;");
		});
		
		$('.save').click(function(){
			var idpel = <?php echo $idpel;?>;
			var tgl = this.title;
			var id = tgl;
			var konten = document.getElementById('cat_'+tgl).value;
			$.post("<?php echo site_url('pelanggan/save_catatan_async/')?>/"+idpel+"/"+tgl, {catatan: konten});
			document.getElementById(id).setAttribute("style", "display:inline;");
			document.getElementById('save_'+id).setAttribute("style", "display:none;");
			document.getElementById('cat_'+id).setAttribute("style", "width:100%; padding:4px 2px; border: none; background:none;");
			document.getElementById('cat_'+id).readOnly=true;
		});
		
	});
</script>
<p>
<form method="POST" action="<?php echo site_url('pelanggan/save_catatan/'.$idpel);?>">
<?php
	foreach($plg as $r){
		$nama = strtoupper($r['NAMA']);
		$alamat = $r['ALAMAT'];
	}
?>
<div class="contentpanel">  
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title"><?=$subtitle?></h4>
<table width="100%">
<tr><td width="125" >IDPEL</td>
<td width="1005">: <?php echo $idpel;?></td>
</tr>
<tr><td>NAMA</td><td>: <?php echo $nama;?></td>
</tr>
<tr><td>ALAMAT</td><td>: <?php echo $alamat;?></td>
</tr>
</table>

<br/>
<table class="table table-bordered mb30">
<input type="hidden" name="pelanggan" value="">
<tr>
	<th width="20%"><div align="center">TANGGAL</div></th>
	<th><div align="center">CATATAN</div></th>
	<th width="10%"><div align="center">USER INPUT </div></th>
	<th width="10%"><div align="center"></div></th>
</tr>
<?php
$i=0;
foreach($result as $r) {
	if($i%2==0) $row="row-a";
	else $row="row-b";
?>
<tr class="row-y">
	<?php
		$cari = array(' ', '/');
		$ganti   = array('-', '_');
		$tanggal = str_replace($cari,$ganti,substr($r['TGL'],0,19));
	?>
	<td valign="top"><div align="center" style="margin-top:8px;"><?php echo substr($r['TGL'],0,19);?></div></td>
	<td>
		<div align="center">
			<textarea id="cat_<?php echo $tanggal;?>" name="catatan1"  maxlength='500' class="form-control" Readonly ><?php echo $r['CATATAN'];?></textarea>
		</div>	</td>
	<td valign="top"><?=$r['ID_USER']?></td>
	<td valign="top"><div align="center" style="margin-top:8px;"><span>
			<a class="save" href="#" title="<?php echo $tanggal;?>" id="save_<?php echo $tanggal;?>" style="display:none;"><img src="<?php echo base_url() . "public/images/save.png";?>"></a>
			<!--<a class="updates" href="#" id="<?php echo $tanggal;?>" ><i class="glyphicon glyphicon-edit"></i></a>-->
			<a class="confirm" href="<?php echo site_url('pelanggan/delete_catatan/'.$r['IDPEL'].'/'.$tanggal);?>"><i class="glyphicon glyphicon-remove"></i></a></span></div></td>
</tr>
<?php
	$i++;
}
?>
<tr class="row-x">
	<td valign="middle"><div class="input-group" align="center">
                <input type="text" name="tgl" class="form-control" placeholder="dd/mm/yyyy" id="datepicker3" >
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div></td>
	<td>
		<div align="center">
			<textarea name="catatan" maxlength='500' class="form-control"></textarea>
		</div>	</td>
	<td>&nbsp;</td>
	<td><div align="center"><input type="submit" class="btn btn-primary" value="&nbsp;SAVE&nbsp;"></div></td>
</tr>
</table>
</form>
<div align="center">
<?=anchor(site_url().'/pelanggan/data_ts_input/'.trim($idpel),'INPUT DATA TS', array('type'=>'button', 'class'=>'btn btn-primary'));?>
</div>
</p>


</div><!-- panel-body -->
        
      </div>
      </div>