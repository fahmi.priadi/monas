<div class="contentpanel">  
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title"><?=$subtitle?></h4>
<p>
<script src="<?=base_url()?>public/js/jquery.chained.js"></script>
<script type="text/javascript">
	$(function() {
		$(".sph_tanggal").datepicker({
			maxDate: "+0D"
		});
	});
	function numberOnly(angka){
		var charCode = (angka.which) ? angka.which : event.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
		else return true;
	}
	$(document).ready(function(){
		$("#jenis").chained("#kategori");
	});
</script>


<form method="POST" action="<?php echo site_url('pelanggan/data_ts_input_sph');?>">

<input type="hidden" name="current_id" id="current_id" value="<?=$result->ID_DATA_TS;?>">

<table cellpadding="0" cellspacing="0" >
<tr>
	<td width="100">IDPEL</td>
	<td width="249">: <?=$result->IDPEL;?></td>
	<td width="40"></td>
	<td width="77">TARIF</td>
	<td width="446">: <?=$result->TARIF;?></td>
</tr>
<tr>
	<td>NAMA</td><td>: <?=$result->NAMA;?></td>
	<td></td>
	<td>DAYA</td><td>: <?=$result->DAYA;?></td>
</tr>
<tr>
	<td>ALAMAT</td><td>: <?=$result->ALAMAT;?></td>
	<td colspan="3"></td>
</tr>
</table>

<hr class="style_ts" />
<div class="panel-body panel-body-nopadding">

            <div class="form-group">
              <label class="col-sm-3 control-label">Kategori Temuan</label>
              <div class="col-sm-6">
                <select name="kategori" id="kategori" class="form-control chosen-select" disabled="disabled">
					<option value="KELAINAN" <? if($result->KATEGORI=='KELAINAN') echo 'selected="selected"';?> >&nbsp;Kelainan</option>
					<option value="PELANGGARAN" <? if($result->KATEGORI=='PELANGGARAN') echo 'selected="selected"';?>>&nbsp;Pelanggaran</option>
				</select>
              </div>
            </div>
            
            <div class="form-group">
				  <label class="col-sm-3 control-label" for="disabledinput">Jenis Kelainan / Pelanggaran</label>
				  <div class="col-sm-6">
                <select name="jenis" id="jenis" class="form-control chosen-select"  disabled="disabled">
                    <option class="KELAINAN" value="K1" <? if($result->JENIS=='K1') echo 'selected="selected"';?>>&nbsp;K1&nbsp;&nbsp;</option>
                    <option class="KELAINAN" value="K2" <? if($result->JENIS=='K2') echo 'selected="selected"';?>>&nbsp;K2&nbsp;&nbsp;</option>
                    <option class="KELAINAN" value="K3" <? if($result->JENIS=='K3') echo 'selected="selected"';?>>&nbsp;K3&nbsp;&nbsp;</option>
                    <option class="PELANGGARAN" value="P1" <? if($result->JENIS=='P1') echo 'selected="selected"';?>>&nbsp;P1&nbsp;&nbsp;</option>
                    <option class="PELANGGARAN" value="P2" <? if($result->JENIS=='P2') echo 'selected="selected"';?>>&nbsp;P2&nbsp;&nbsp;</option>
                    <option class="PELANGGARAN" value="P3" <? if($result->JENIS=='P3') echo 'selected="selected"';?>>&nbsp;P3&nbsp;&nbsp;</option>
                </select>
				  </div>
		</div>
            
            <div class="form-group">
				  <label class="col-sm-3 control-label" for="readonlyinput">Deskripsi</label>
				  <div class="col-sm-6">
					 <textarea maxlength="500" class="form-control" name="deskripsi" readonly="readonly"><?=$result->DESKRIPSI?></textarea>
				  </div>
		</div>
            
            <div class="form-group">
              <label class="control-label col-sm-3"><strong>Data Tagihan Susulan</strong></label>
              <div class="col-sm-6">&nbsp;
              </div>
            </div>
            
        <div class="form-group">
              <label class="col-sm-3 control-label">Jumlah kWh</label>
          <div class="col-sm-6" style=" width:200px">
               <input name="ts_kwh" type="text" class="form-control" onkeypress="return numberOnly(event);" value="<?=$result->TS_KWH?>" readonly="readonly">
          </div>
        </div>
        <div class="form-group">
              <label class="col-sm-3 control-label">Jumlah Daya</label>
              <div class="col-sm-6" style=" width:200px">
               <input name="ts_daya" type="text" class="form-control" onkeypress="return numberOnly(event);" value="<?=$result->TS_DAYA?>" readonly="readonly">
              </div>
        </div>
        <div class="form-group">
              <label class="col-sm-3 control-label">Jumlah Rupiah</label>
              <div class="col-sm-6" style=" width:200px">
               <input name="ts_rupiah" type="text" class="form-control" onkeypress="return numberOnly(event);" value="<?=$result->TS_RUPIAH?>" readonly="readonly">
              </div>
        </div>
            
            <div class="form-group">
              <label class="control-label col-sm-3"><strong>Data Surat Pengakuan Hutang (SPH)</strong></label>
              <div class="col-sm-6">&nbsp;
              </div>
            </div>
            
           <div class="form-group">
              <label class="col-sm-3 control-label">Tanggal</label>
              <div class="col-sm-6">
              <div class="input-group" style=" width:180px">
                <input type="text" name="sph_tanggal" class="form-control" id="datepicker3" value="<?=$result->TGL?>"">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div>
              </div>
        </div>
        <div class="form-group">
              <label class="col-sm-3 control-label">Nomor SPH</label>
              <div class="col-sm-6" style=" width:200px">
               <input name="sph_nomor" type="text" class="form-control" value="<?=$result->SPH_NOMOR?>">
              </div>
        </div>
        <div class="form-group">
              <label class="col-sm-3 control-label">Rupiah SPH</label>
              <div class="col-sm-6" style=" width:200px">
               <input name="sph_rupiah" type="text" class="form-control" onkeypress="return numberOnly(event);" value="<?=$result->SPH_RUPIAH?>">
              </div>
        </div>
            
           <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-6 col-sm-offset-3">
				  <button id="blok" class="btn btn-primary" type="submit">Submit</button>&nbsp;
				  <button class="btn btn-default">Cancel</button>
				</div>
			 </div>
		  </div><!-- panel-footer -->
            
          </form>

          
        </div><!-- panel-body -->
 
    </div>
    <!-- panel-body -->
        
      </div>
      </div>
</div>