
<p>&nbsp;</p>

<div class="contentpanel">  
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title"><?=$subtitle?></h4>
          <p>Fitur analisa arus digunakan untuk mencari pelanggan yang arusnya tidak sesuai dengan standar TMP. Fitur ini akan mencari data load profile pelanggan yang salah satu/dua arus phasa-nya kurang dari Batas Arus Minimum dan dua/satu arus phasa lainnya lebih dari Batas Arus Normal dalam rentang waktu sesuai periode yang dipilih.</p>
        </div>
        <div class="panel-body panel-body-nopadding">
          
            <?=form_open('#', array('class'=>'form-horizontal form-bordered'))?>
            <div class="form-group">
              <label class="col-sm-3 control-label">UNITUP</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="unitup" >
                  <? foreach($area as $data) { ?>
                  <option value="<?=$data['KDAREA']?>"><?=$data['NAMA_AREA']?></option>
                   <? } //end foreach ?>
                </select>
              </div>
            </div>
            
            <div class="form-group">
				  <label class="col-sm-3 control-label" for="disabledinput">Periode</label>
				  <div class="col-sm-6">
					 <div class="input-group">
                <input type="text" name="tgl1" class="form-control" id="datepicker"  >
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div>
				  </div>
				</div>
            
            <div class="form-group">
				  <label class="col-sm-3 control-label" for="readonlyinput">s.d</label>
				  <div class="col-sm-6">
					 <div class="input-group">
                <input type="text" name="tgl2" class="form-control" placeholder="mm/dd/yyyy" id="datepicker2" >
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div>
				  </div>
				</div>
            
            <div class="form-group">
              <label class="col-sm-3 control-label">Batas Arus Minimum (A)</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="arus_min">
                  	<option value="0.1">0.1&nbsp;&nbsp;</option>
      				<option value="0.2" selected="selected">0.2</option>
      				<option value="0.3">0.3</option>
        	   </select>
                <span class="help-block">Batas Bawah arus dalam loadprofile yang dicari</span>
              </div>
            </div>
            
           <div class="form-group">
              <label class="col-sm-3 control-label">Batas Arus Normal (A)</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="arus_normal">
                   <option value="0.75">0.75</option>
                  <option value="1" selected="selected">1</option>
                  <option value="1.5">1.5</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option value="40">40</option>
                  <option value="50">50</option>
                  <option value="75">70</option>
                  <option value="100">100</option>
                </select>
                <span class="help-block">Batas atas arus dalam loadprofile yang dicari</span>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-3 control-label">Minimum Sejumlah</label>
              <div class="col-sm-6">
               <select class="form-control chosen-select" name="jumlah">
                  <option value="300">300 x</option>
                  <option value="500" selected="selected">500 x</option>
                  <option value="1000">1000 x</option>
                  <option value="2000">2000 x</option>
                </select>
                <span class="help-block">Jumlah kejadian minimum</span>
              </div>
            </div>

           <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-6 col-sm-offset-3">
				  <input name="" type="button" id="blok" value="submit">
				</div>
			 </div>
		  </div><!-- panel-footer -->
            
          </form>
        </div><!-- panel-body -->
        
      </div>
      </div>
</div>


