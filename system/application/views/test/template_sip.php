<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themepixels.com/demo/webpage/bracket/index.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 06:31:23 GMT -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?=base_url()?>public/images/favicon.png" type="image/png">

  <title>Bracket Responsive Bootstrap3 Admin</title>

  <link href="<?=base_url()?>public/css/style.default.css" rel="stylesheet">


</head>

<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <div class="leftpanel">
    
    <div class="logopanel">
        <h1><span>[</span> Monas <span>]</span></h1>
    </div><!-- logopanel -->
        
    <div class="leftpanelinner">    
        
        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">   
            <div class="media userlogged">
                <img alt="" src="images/photos/loggeduser.png" class="media-object">
                <div class="media-body">
                    <h4>John Doe</h4>
                    <span>"Life is so..."</span>
                </div>
            </div>
          
            <h5 class="sidebartitle actitle">Account</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
              <li><a href="profile.html"><i class="fa fa-user"></i> <span>Profile</span></a></li>
              <li><a href="#"><i class="fa fa-cog"></i> <span>Account Settings</span></a></li>
              <li><a href="#"><i class="fa fa-question-circle"></i> <span>Help</span></a></li>
              <li><a href="signout.html"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>
      
      <h5 class="sidebartitle">Navigation</h5>
      <ul class="nav nav-pills nav-stacked nav-bracket">
        <li class="active"><a href="index2.html"><i class="fa fa-home"></i> <span>Home</span></a></li>        
        <li class="nav-parent"><a href="#"><i class="fa fa-edit"></i> <span>Analisa Data AMR</span></a>
          <ul class="children">
            <li><a href="general-forms.html"><i class="fa fa-caret-right"></i> Arus</a></li>
            <li><a href="form-layouts.html"><i class="fa fa-caret-right"></i> Arus-Ukur Langsung</a></li>
            <li><a href="form-validation.html"><i class="fa fa-caret-right"></i> Tegangan</a></li>
            <li><a href="form-wizards.html"><i class="fa fa-caret-right"></i> Tegangan-Ukur TR</a></li>
            <li><a href="wysiwyg.html"><i class="fa fa-caret-right"></i> KWH Plus Minus</a></li>
          </ul>
        </li>
		<li class="nav-parent"><a href="#"><i class="fa fa-edit"></i> <span>Rekap Data</span></a>
          <ul class="children">
            <li><a href="general-forms.html"><i class="fa fa-caret-right"></i> Jumlah Pelanggan</a></li>
            <li><a href="form-layouts.html"><i class="fa fa-caret-right"></i> Jml KWH Gardu</a></li>
            <li><a href="form-validation.html"><i class="fa fa-caret-right"></i> Rekap Tagihan Susulan</a></li>
          </ul>
        </li>
		<li class="nav-parent"><a href="#"><i class="fa fa-edit"></i> <span>Pencarian Data</span></a>
          <ul class="children">
            <li><a href="general-forms.html"><i class="fa fa-caret-right"></i> IDPEL</a></li>
            <li><a href="form-layouts.html"><i class="fa fa-caret-right"></i> Pelanggan TT</a></li>
            <li><a href="form-validation.html"><i class="fa fa-caret-right"></i> LP Kurang</a></li>
            <li><a href="form-wizards.html"><i class="fa fa-caret-right"></i> Interval LP</a></li>
          </ul>
        </li>
        <li><a href="tables.html"><i class="fa fa-th-list"></i> <span>Menu Biasa</span></a></li>
        
      </ul>
      
      <!-- infosummary -->
    </div>
    <!-- leftpanelinner -->
  </div><!-- leftpanel -->
  
  <div class="mainpanel">    
    <div class="headerbar">      
      <a class="menutoggle"><i class="fa fa-bars"></i></a>         
      <div class="header-right">
        <ul class="headermenu">                 
          <? if($this->session->userdata('usn')!='') {?>
		  <li>
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <img src="<?=base_url()?>public/images/photos/loggeduser.png" alt="" />
                <?=$this->session->userdata('usn')?>
              <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                <li><a href="profile.html"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> Manual Book</a></li>
                 <li><?=anchor('auth/logout','<i class="glyphicon glyphicon-log-out"></i> Log Out')?></li>
              </ul>
            </div>
          </li> <? }else { ?>
			<li>
            <div >
              <button type="button" class="btn btn-default dropdown-toggle" >
                <a href="<?=base_url()?>index.php/auth/login"><span class="glyphicon glyphicon-log-in"></span> <strong>LOGIN</strong></a>
               
              </button>              
            </div>
          </li> <? } ?>
        </ul>
      </div><!-- header-right -->      
    </div><!-- headerbar -->
    
    <div class="pageheader">
      <h2><i class="fa fa-table"></i> <?=$title?> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Bracket</a></li>
          <li class="active">Tables</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">        
    <div class="panel panel-default">
        <div class="panel-heading">          
          <h3 class="panel-title"><?=$title?></h3>
          <p>DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add advanced interaction controls to any HTML table.</p>
        </div>
        <div class="panel-body">
          <h5 class="subtitle mb5">Basic Table</h5>
          <p class="m20">DataTables has most features enabled by default, so all you need to do to use it with one of your own tables is to call the construction function.</p>
          <br />
          </div><!--panel-body -->
           </div><!--panel-heading -->
      </div><!-- panel panel-default -->
       </div><!-- contentpanel -->  
        
  </div><!-- mainpanel -->
  <!-- tab-content -->
  </div>
  <!-- rightpanel -->
</section>

  
<script src="<?=base_url()?>public/js/jquery-1.10.2.min.js"></script>
<script src="<?=base_url()?>public/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.sparkline.min.js"></script>
<script src="<?=base_url()?>public/js/toggles.min.js"></script>
<script src="<?=base_url()?>public/js/jquery.cookies.js"></script>
<script src="<?=base_url()?>public/js/custom.js"></script>

</body>

<!-- Mirrored from themepixels.com/demo/webpage/bracket/index.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 06:32:31 GMT -->
</html>
