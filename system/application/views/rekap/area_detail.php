<link rel="stylesheet" type="text/css" media="screen" href="<?=base_url()?>public/css/table_default.css" />

<table width="100%" class="table table-bordered mb30">

<tr>
<th><div align="center">BLTH</div></th>
<th><div align="center">IDPEL</div></th>
<th><div align="center">NAMA</div></th>
<th><div align="center">ALAMAT</div></th>
<th><div align="center">TARIF</div></th>
<th><div align="center">DAYA (VA)</div></th>
<th><div align="center">JUMLAH LP</div></th>
</tr>

<?php 
	$i=0;
	foreach($result as $r){
	if($i%2==0) $row="row-a";
	else $row="row-b";
?>
<tr class="<?=$row?>">
	<td  align="center"><?php echo $r['BLTH'];?></td>
	<td  align="center"><?php echo $r['NOPEL'];?></td>
	<td><?php echo $r['NAMA'];?></td>
	<td><?php echo $r['ALAMAT'];?></td>
	<td><?php echo $r['GOLONGAN'];?></td>
	<td align="right"><?php echo number_format($r['DAYATPS'],0,'','.');?></td>
	<td align="right"><?php echo $r['JML_LP'];?></td>
</tr>
<?php } ?>
<tr>
	<td colspan="7" align="center">Jumlah data: <strong><?=number_format($jml_data,0,'','.')?></strong> record</td>
</tr>
<tr>
	<td colspan="7" align="center"><?php echo $paging?></td>
</tr>
  <tr>
    <td colspan="7" align="center"><? if($jml_data>65000) echo "Jumlah Data Melebihi Kapasitas Ms.Excel<br>Silakan Persempit Kriteria Pencarian"; else echo anchor('rekap/excel_area_detail_download/'.$blth.'-'.$unitup.'-'.$jml_data.'-'.$opsi,'<img src="'.base_url().'public/images/Excel-icon.png" width="12" height="9"/>&nbsp;Export to Excel')?></td>
  </tr>
</table>